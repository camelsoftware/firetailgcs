/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HSI_H_INCLUDED
#define HSI_H_INCLUDED


#include <gtkmm.h>
#include <sstream>
#include <fstream>
#include "osmmaps/osm-gps-map.h"
#include "coms.h"


class HSI : public Gtk::Box
{
    public:
        HSI(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
        virtual ~HSI();

        void redraw(bool lock_on_aircraft);
        void reload();

        void set_map_type(OsmGpsMapSource_t src);

        bool got_click_point();
        void reset_click_point();
        coord get_click_point();

    protected:
        OsmGpsMapLayer* osd;
        OsmGpsMapTrack* track;

        double old_lat, old_lng;

        bool not_available;

        GdkPixbuf* plane_image;
        OsmGpsMapImage* last_image;
        OsmGpsMapPoint* plane_point;

        GdkPixbuf* gdk_home_image;
        OsmGpsMapImage* map_home_image;
        OsmGpsMapPoint* home_point;

        OsmGpsMapTrack* flight_plan;
        GdkPixbuf* gdk_point_image;
        OsmGpsMapImage* map_point_image;

        OsmGpsMapTrack* home_track;
};


#endif // HSI_H_INCLUDED
