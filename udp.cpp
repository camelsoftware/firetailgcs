/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "udp.h"

#include <iostream>
using namespace std;

UDPSocket::UDPSocket()
{
	cout << "opening socket" << endl;
#ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(0x0202,&wsaData);
#endif // _WIN32
    sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    int y = 1;

#ifdef _WIN32
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&y, sizeof(int));
#else
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &y, sizeof(int));
#endif // _WIN32
	cout << "done" << endl;
}

bool UDPSocket::set_timeout(int us)
{
#ifdef _WIN32
	us /= 1000;
    if(setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,(char*)&(us),sizeof(int)) < 0)
#else
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = us;
    if(setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0)
#endif // _WIN32
		return false;
    return true;
}

bool UDPSocket::listen(int port)
{
	if (sock < 0)
		return false;
    memset(&listen_addr, 0, sizeof(listen_addr));
	listen_addr.sin_family=AF_INET;
	listen_addr.sin_addr.s_addr=INADDR_ANY;
	listen_addr.sin_port=htons(port);
	if (bind(sock,(struct sockaddr *)&listen_addr, sizeof(listen_addr))<0)
       return false;
	return true;
}

bool UDPSocket::set_write_address(const char* hostname, uint16_t port)
{
    struct hostent *hp;

	if((hp = gethostbyname(hostname)) == NULL)
        return false;

	memset(&write_addr, 0, sizeof(write_addr));
	memcpy(&write_addr.sin_addr, hp->h_addr_list[0],
             hp->h_length);
    write_addr.sin_port = htons(port);
	write_addr.sin_family=AF_INET;
    return true;
}

int UDPSocket::read(void* buf, int maxlen)
{
#ifdef _WIN32
    struct sockaddr_in in;
    int len = sizeof(struct sockaddr);
	int ret = recvfrom(sock, (char*)buf, maxlen,
		0,(struct sockaddr*)&in, &len);

	if(ret == SOCKET_ERROR)
	{
		cout << "read socket error: " << WSAGetLastError() << endl;
	}
	return ret;
#else
    struct sockaddr_in in;
    socklen_t len = sizeof(sockaddr_in);
	return recvfrom(sock, buf, maxlen,
		0,(struct sockaddr*)&in, &len);
#endif
}

int UDPSocket::write(void* buf, int len)
{
#ifdef _WIN32
	int ret = sendto(sock, (const char*)buf, len,
			0,(struct sockaddr *)&write_addr, sizeof(struct sockaddr_in));
	if(ret == SOCKET_ERROR)
	{
		cout << "write socket error: " << WSAGetLastError() << endl;
	}
	return ret;
#else
	return sendto(sock, (const char*)buf, len,
			0,(struct sockaddr *)&write_addr, sizeof(struct sockaddr_in));
#endif
}

