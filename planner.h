/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PLANNER_H_INCLUDED
#define PLANNER_H_INCLUDED

#include <gtkmm.h>
#include <sstream>
#include <fstream>
#include "plan_file.h"
#include "coms.h"
#include "vector.h"
#include "osmmaps/osm-gps-map.h"



class Planner : public Gtk::Window
{
public:
    Planner(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~Planner();

    void show();
    void timeout();

    void on_waypoint_selection_changed();
    void update_path_distance();
    void speed_spin_changed();

    void on_geofence_create();
    void on_geofence_delete();

    void on_save();
    void on_save_as();
    void on_new();
    void on_open();

    void update_fence_warning();

    void on_zoom_changed();
    void on_map_type_combo_changed();


    void on_cache_clicked();

    std::string file_name;

    std::vector<PlanFile::wp> points;
    std::vector<PlanFile::wp> geopoints;
    std::vector<PlanFile::wp> runways;

    void on_upload_plan();
    void threadfunc();
    void upload_thread();
    std::thread thr;
    std::mutex mute;

    std::string progress_msg;
    double upload_progress; // percentage complete
    bool thr_running;

    Glib::RefPtr<Gtk::Builder> refBuilder;

    class ModelColumns : public Gtk::TreeModelColumnRecord
    {
    public:

        ModelColumns()
        {
            add(m_col_number);
            add(m_col_latitude);
            add(m_col_longitude);
            add(m_col_altitude);
        }

        Gtk::TreeModelColumn<float> m_col_latitude;
        Gtk::TreeModelColumn<float> m_col_longitude;
        Gtk::TreeModelColumn<int> m_col_number;
        Gtk::TreeModelColumn<int> m_col_altitude;
    };

    Gtk::TreeView* waypoint_list;
    Glib::RefPtr<Gtk::TreeSelection> waypoint_selection;
    ModelColumns m_Columns;
    Glib::RefPtr<Gtk::ListStore> refwpListStore;

    Gtk::TreeView* geopoint_list;
    Glib::RefPtr<Gtk::ListStore> refgpListStore;

    Gtk::SpinButton* speed_spin;
    Gtk::Label* path_distance_lbl;
    Gtk::Label* time_estimate_lbl;
    Gtk::ProgressBar* sync_progress;

    Gtk::Label* progress_lbl;

    Gtk::Button* upload_button;

    bool first_show;

    Gtk::Button* file_open;
    Gtk::Button* file_save;
    Gtk::Button* file_saveas;
    Gtk::Button* file_new;

    bool changed;

    Gtk::SpinButton* map_zoom_spin;
    Gtk::ComboBoxText* map_type_combo;

    Gtk::Button* create_geofence_button;
    Gtk::Button* delete_geofence_button;

    Gtk::Label* fence_warning;

    double map_center_lat, map_center_lng;

    Gtk::Button* cache_button;
    Gtk::Label* tiles_label;
    GdkPixbuf* gdk_cursor_image;
};



#endif


