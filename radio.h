#ifndef RADIO_H_INCLUDED
#define RADIO_H_INCLUDED

#include <gtkmm.h>
#include "coms.h"

class RadioWindow : public Gtk::Window
{
public:
    RadioWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~RadioWindow();

    void timeout();

private:

    void write_settings();
    void read_settings();
    void write_default();

    bool open_port();
    bool enter_at_mode();

    Glib::RefPtr<Gtk::Builder> refBuilder;

    Gtk::Entry* port_entry;
    Gtk::Entry* version_entry;

    Gtk::ComboBoxText* baud_combo;
    Gtk::ComboBoxText* air_speed_combo;
    Gtk::SpinButton* net_id_spinbutton;
    Gtk::SpinButton* tx_pwr_spinbutton;
    Gtk::ComboBoxText* node_id_combo;
    Gtk::ComboBoxText* node_dest_combo;
    Gtk::ComboBoxText* min_freq_combo;
    Gtk::ComboBoxText* max_freq_combo;
    Gtk::ComboBoxText* num_chans_combo;
    Gtk::ComboBoxText* duty_cycle_combo;
    Gtk::ComboBoxText* node_count_combo;

    Gtk::CheckButton* sync_any_check;
    Gtk::CheckButton* ecc_check;
    Gtk::CheckButton* opp_resend_check;
    Gtk::CheckButton* mavlink_check;

    Gtk::Button* read_button;
    Gtk::Button* write_button;
    Gtk::Button* write_default_button;

    serial::Serial sp;

    int rd_settings[18];
};

#endif // RADIO_H_INCLUDED
