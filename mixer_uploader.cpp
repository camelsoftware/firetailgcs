#include "mixer_uploader.h"

MixerUploader::MixerUploader(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("close_cancel_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &MixerUploader::on_close));

    refBuilder->get_widget("upload_mixer_progress", pbar);
}



void MixerUploader::show()
{
    Gtk::FileChooserDialog fcd(*this, "Open mixer file", Gtk::FILE_CHOOSER_ACTION_OPEN, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::fstream inf;
    inf.open(fcd.get_filename());
    if(!inf.is_open())
    {
        Gtk::MessageDialog msg(*this, "Error opening file!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be opened.");
        msg.run();
        return;
    }

    std::stringstream ss;
    ss << inf.rdbuf();
    mix_file = ss.str();
    inf.close();

    part = 0;
    n_parts = 1 + (mix_file.length()/22.0);

    Gtk::Window::show();
}

void MixerUploader::on_close()
{
    Gtk::Window::close();
}

void MixerUploader::timeout()
{
    if(mix_file.length() >= ((n_parts-1)*22))
    {
        if(part > n_parts)
        {
            btn->set_label("Close");
            return; //we're done sending, don't even bother
        }
        btn->set_label("Cancel");

        shared_ptr<Coms> coms = Coms::get_active_instance();
        coms->lock();
        bool wait_this_turn = coms->on_queue(Firelink::SM_SET_MIXER_FILE_PART);
        coms->unlock();
        if(wait_this_turn)
            return;

        if(part == 0)
        {
            coms->lock();
            coms->set_file_upload(Firelink::FILE_TYPE_MIXER, Firelink::FILE_STATUS_STARTING);
            coms->unlock();
        }

        part++;
        coms->lock();
        coms->set_mixer_file_part((uint8_t*)&mix_file[(part*22)-22], part-1);
        coms->unlock();

        if(part > n_parts)
        {
            coms->lock();
            coms->set_file_upload(Firelink::FILE_TYPE_MIXER, Firelink::FILE_STATUS_COMPLETE);
            coms->unlock();
        }

        pbar->set_fraction(float(part)/float(n_parts));
    }
}

