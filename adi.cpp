/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "adi.h"

ADI::ADI(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::DrawingArea(cobject)
{

}

ADI::~ADI()
{

}


void ADI::redraw()
{
    Glib::RefPtr<Gdk::Window> win = get_window();
    if(win)
    {
        win->invalidate(false);
    }
}



bool ADI::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    coms->lock();

    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->set_source_rgba(1.0, 1.0, 1.0, 1.0);
    cr->rectangle(0, 0, width, height);
    cr->fill();

    float pitch, heading, roll = 0;
    bool invert = false;
    int hori_pos = 0;
    int dia = 0;

    if(width > height)
        dia = height;
    else
        dia = width;

    heading = coms->get_heading();
    pitch = coms->get_pitch();
    roll = coms->get_roll();

    if((pitch > 270) || (pitch < 90))
    {
        hori_pos = (dia/2)*sin(to_radians(pitch));
        invert = true;
    }
    else
        hori_pos = (dia/2)*sin(to_radians(pitch+180));


    float rad_rol = to_radians(-roll);
    cr->translate(width/2, height/2);
    cr->rotate(rad_rol);
    cr->translate(-width/2, -height/2);
    if(invert)
    {
        Cairo::RefPtr< Cairo::LinearGradient > background_gradient_ptr = Cairo::LinearGradient::create (
                    0, -400, 0, (height/2)+hori_pos);

        background_gradient_ptr->add_color_stop_rgb (1,
                0.2, 0.2, 1.0);
        background_gradient_ptr->add_color_stop_rgb (0,
                0.0, 0.0, 0.1);
        cr->set_source (background_gradient_ptr);

        cr->rectangle(-400, -400, width+800, (height/2)+hori_pos+400);
        cr->fill();

        background_gradient_ptr = Cairo::LinearGradient::create (
                    0, (height/2)+hori_pos, 0,  height+400);
        background_gradient_ptr->add_color_stop_rgb (0,
                0.05, 0.8, 0.1);
        background_gradient_ptr->add_color_stop_rgb (1,
                0.01, 0.1, 0.0);
        cr->set_source (background_gradient_ptr);

        cr->rectangle(-400, (height/2)+hori_pos, width+800, height+400);
        cr->fill();
    }
    else
    {
        Cairo::RefPtr< Cairo::LinearGradient > background_gradient_ptr = Cairo::LinearGradient::create (
                    0, -400, 0, (height/2)+hori_pos);

        background_gradient_ptr->add_color_stop_rgb (1,
                0.05, 0.8, 0.1);
        background_gradient_ptr->add_color_stop_rgb (0,
                0.01, 0.1, 0.0);
        cr->set_source (background_gradient_ptr);
        cr->rectangle(-400, -400, width+800, (height/2)+hori_pos+400);
        cr->fill();

        background_gradient_ptr = Cairo::LinearGradient::create (
                    0, (height/2)+hori_pos, 0,  height+400);

        background_gradient_ptr->add_color_stop_rgb (0,
                0.2, 0.2, 1.0);
        background_gradient_ptr->add_color_stop_rgb (1,
                0.0, 0.0, 0.1);
        cr->set_source (background_gradient_ptr);
        cr->rectangle(-400, (height/2)+hori_pos, width+800, height+400);
        cr->fill();
    }

    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->move_to(-200, (height/2)+hori_pos);
    cr->line_to(width+400, (height/2)+hori_pos);
    cr->stroke();

    cr->move_to((width/2)-60, (height/2)+(dia/2)*0.173);
    cr->line_to((width/2)-15, (height/2)+(dia/2)*0.173);
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)+(dia/2)*0.173);
    cr->line_to((width/2)+15, (height/2)+(dia/2)*0.173);
    cr->stroke();

    cr->move_to((width/2)-6, (height/2)+((dia/2)*0.173)+4);
    cr->show_text("-10");
    cr->stroke();

    cr->move_to((width/2)-60, (height/2)+(dia/2)*0.342);
    cr->line_to((width/2)-15, (height/2)+(dia/2)*0.342);
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)+(dia/2)*0.342);
    cr->line_to((width/2)+15, (height/2)+(dia/2)*0.342);
    cr->stroke();

    cr->move_to((width/2)-6, (height/2)+((dia/2)*0.342)+4);
    cr->show_text("-20");
    cr->stroke();

    cr->move_to((width/2)-60, (height/2)+(dia/2)*0.5);
    cr->line_to((width/2)-15, (height/2)+(dia/2)*0.5);
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)+(dia/2)*0.5);
    cr->line_to((width/2)+15, (height/2)+(dia/2)*0.5);
    cr->stroke();

    cr->move_to((width/2)-6, (height/2)+((dia/2)*0.5)+4);
    cr->show_text("-30");
    cr->stroke();

    cr->move_to((width/2)-60, (height/2)-(dia/2)*0.173);
    cr->line_to((width/2)-15, (height/2)-(dia/2)*0.173);
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)-(dia/2)*0.173);
    cr->line_to((width/2)+15, (height/2)-(dia/2)*0.173);
    cr->stroke();

    cr->move_to((width/2)-6, (height/2)-((dia/2)*0.173)+4);
    cr->show_text("10");
    cr->stroke();

    cr->move_to((width/2)-60, (height/2)-(dia/2)*0.342);
    cr->line_to((width/2)-15, (height/2)-(dia/2)*0.342);
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)-(dia/2)*0.342);
    cr->line_to((width/2)+15, (height/2)-(dia/2)*0.342);
    cr->stroke();

    cr->move_to((width/2)-6, (height/2)-((dia/2)*0.342)+4);
    cr->show_text("20");
    cr->stroke();

    cr->move_to((width/2)-60, (height/2)-(dia/2)*0.5);
    cr->line_to((width/2)-15, (height/2)-(dia/2)*0.5);
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)-(dia/2)*0.5);
    cr->line_to((width/2)+15, (height/2)-(dia/2)*0.5);
    cr->stroke();

    cr->move_to((width/2)-6, (height/2)-((dia/2)*0.5)+4);
    cr->show_text("30");
    cr->stroke();

    cr->set_source_rgb(0.2, 0.0, 0.4);
    float apitchrad = to_radians(coms->get_ap_pitch());
    cr->move_to((width/2)-60, (height/2)+(dia/2)*sin(apitchrad));
    cr->line_to((width/2)-15, (height/2)+(dia/2)*sin(apitchrad));
    cr->stroke();

    cr->move_to((width/2)+60, (height/2)+(dia/2)*sin(apitchrad));
    cr->line_to((width/2)+15, (height/2)+(dia/2)*sin(apitchrad));
    cr->stroke();

    cr->translate(width/2, height/2);
    cr->rotate(-rad_rol);
    cr->translate(-width/2, -height/2);

    //draw the heading lines
    cr->move_to(width/2-8, 40);
    char buf[120];
    sprintf(buf, "%0.2f", heading);
    cr->show_text(buf);
    cr->stroke();

    float hheight = height/2;
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->arc(width/2, hheight+70, dia/2, 4.18, 5.236);
    cr->stroke();

    int angle = 30;
    while(angle < 370)
    {
        float adj = cos(((angle-90)-heading)*(M_PI/180.0))*(dia/2);
        float opp = sin((M_PI/180.0)*((angle-90)-heading))*(dia/2);

        float adj2 = cos((M_PI/180)*((angle-90)-heading))*((dia/2)+10);
        float opp2 = sin((M_PI/180)*((angle-90)-heading))*((dia/2)+10);

        float adj3 = cos((M_PI/180)*((angle-90)-heading))*((dia/2)+20);
        float opp3 = sin((M_PI/180)*((angle-90)-heading))*((dia/2)+20);

        if(hheight+70+opp < (height/2.0))
        {
            cr->move_to(width/2+adj, hheight+70+opp);
            cr->line_to(width/2+adj2, hheight+70+opp2);
            cr->stroke();

            cr->move_to(width/2+adj3-9, hheight+70+opp3+5);
            sprintf(buf, "%i", angle);
            cr->show_text(buf);
            cr->stroke();
        }
        angle += 30;
    }

    //draw the autopilot heading mark
    float adj, opp, adj2, opp2;
    float ap_heading = coms->get_ap_heading();
    adj = cos(to_radians(((ap_heading-90)-heading)))*(dia/2);
    opp = sin(to_radians(((ap_heading-90)-heading)))*(dia/2);

    adj2 = cos(to_radians(((ap_heading-90)-heading)))*((dia/2)+10);
    opp2 = sin(to_radians(((ap_heading-90)-heading)))*((dia/2)+10);

    cr->set_source_rgb(0.2, 0.0, 0.4);
    cr->move_to(width/2+adj, hheight+70+opp);
    cr->line_to(width/2+adj2, hheight+70+opp2);
    cr->stroke();

    //draw the altimeter
    cr->set_source_rgba(0.0, 0.0, 0.0, 0.25);
    cr->rectangle(0, 0, 45, height);
    cr->fill();

    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->move_to(20, hheight - 150);
    cr->line_to(20, hheight + 150);

    cr->stroke();

    cr->set_source_rgb(1.0, 1.0, 0.0);
    cr->set_font_size(14);
    cr->move_to(55, hheight+2);
    char cstr_alt[20];
    sprintf(cstr_alt, "%0.0f ft", coms->get_alt());
    cr->show_text(cstr_alt);
    cr->stroke();
    cr->move_to(55, hheight+22);
    int vs = coms->get_vert_speed()/100.0f*3.2808399f*60.0f; //centimeters per second to feet per minute
    sprintf(cstr_alt, "%i ft/min", vs);
    cr->show_text(cstr_alt);
    cr->stroke();

    cr->set_font_size(8);

    float altscale = 0.75;
    float startval = (float(coms->get_alt()/50.0)*50)+150;
    float startpos = (float(coms->get_alt())-startval)*altscale+hheight;

    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->move_to(22, startpos);

    sprintf(cstr_alt, "%1.0f", startval);
    cr->show_text(cstr_alt);
    cr->stroke();

    int i;
    for(i = 0; i <= 5; i++)
    {
        char bb[20];
        startval -= 50;
        startpos += 50*altscale;
        cr->move_to(22, startpos);

        sprintf(bb, "%1.0f", startval);
        cr->show_text(bb);
        cr->stroke();
    }

    cr->set_source_rgb(0.2, 0.0, 0.4);
    cr->move_to(22, (int(coms->get_alt())-coms->get_ap_alt())*altscale+hheight);
    cr->line_to(30, (int(coms->get_alt())-coms->get_ap_alt())*altscale+hheight);
    cr->stroke();

    //if selected alt is off scale too low
    if((coms->get_alt()-200) > coms->get_ap_alt())
    {
        cr->move_to(22, hheight+150);
        cr->line_to(26, hheight+155);
        cr->line_to(30, hheight+150);
        cr->stroke();
    }
    if((coms->get_alt()+200) < coms->get_ap_alt())
    {
        cr->move_to(22, hheight-150);
        cr->line_to(26, hheight-155);
        cr->line_to(30, hheight-150);
        cr->stroke();
    }

    //draw vertical speed
    if(vs > 0)
        cr->set_source_rgb(0.0, 1.0, 0.3);
    else
        cr->set_source_rgb(1.0, 0.0, 0.0);

    cr->move_to(0, height/2);
    cr->line_to(19, height/2);
    cr->line_to(19, (vs/600.0)*-hheight + hheight);
    cr->line_to(0, (vs/600.0)*-hheight + hheight);
    cr->line_to(0, height/2);
    cr->fill();

    //draw home altitude
    cr->set_source_rgb(0.38, 0.20, 0.09);
    cr->move_to(00, (int(coms->get_alt())-coms->home_alt)*altscale+hheight);
    cr->line_to(19, (int(coms->get_alt())-coms->home_alt)*altscale+hheight);
    cr->line_to(19, height);
    cr->line_to(0, height);
    cr->line_to(0, (int(coms->get_alt())-coms->home_alt)*altscale+hheight);
    cr->fill();


    //draw airspeed
    cr->set_source_rgba(0.0, 0.0, 0.0, 0.25);
    cr->rectangle(width-45, 0, 45, height);
    cr->fill();

    cr->set_source_rgb(1.0, 1.0, 1.0);
    float airspeed_scale = 0.25;

    for(int i = 0; i < 150; i += 50)
    {
        float as = coms->get_air_speed()+(i*airspeed_scale);
        if(as > 0)
        {
            cr->move_to(width-40, hheight-i);
            char asbuf[20];
            sprintf(asbuf, "%0.1f", as);
            cr->show_text(asbuf);
            cr->fill();
        }
    }
    for(int i = 50; i < 150; i += 50)
    {
        float as = coms->get_air_speed()+(-i*airspeed_scale);
        if(as > 0)
        {
            cr->move_to(width-40, hheight+i);
            char asbuf[20];
            sprintf(asbuf, "%0.1f", as);
            cr->show_text(asbuf);
            cr->fill();
        }
    }
    //draw the autopilot mode text
    cr->move_to(50, 15);
    cr->set_source_rgb(1.0, 1.0, 0.0);
    cr->set_font_size(12);

    switch(coms->get_ap_mode())
    {
    case Firelink::AP_MODE_MANUAL:
    {
        cr->show_text("MANUAL");
    }
    break;
    case Firelink::AP_MODE_PNR:
    {
        cr->show_text("PNR");
    }
    break;
    case Firelink::AP_MODE_HNA:
    {
        cr->show_text("HNA");
    }
    break;
    case Firelink::AP_MODE_TRAINER:
    {
        cr->show_text("TRAINER");
    }
    break;
    case Firelink::AP_MODE_LOITER:
    {
        cr->show_text("LOITER");
    }
    break;
    case Firelink::AP_MODE_RTB:
    {
        cr->show_text("RTB");
    }
    break;
    case Firelink::AP_MODE_WAYPOINTS:
    {
        cr->show_text("WAYPOINTS");
    }
    break;
    default:
        cr->show_text("UNKNOWN MODE");
    }
    cr->fill();

    int offpos = 0;
    if(coms->get_autothrottle_on())
    {
        cr->move_to(width-160, 40);
        cr->show_text("AUTOTHROTTLE");
        cr->fill();
        offpos += 20;
    }
    if(coms->get_autolaunch_on())
    {
        cr->move_to(width-140, offpos+40);
        cr->show_text("LAUNCH");
        cr->fill();
    }

    /*
        cr->move_to(20, height-20);
        char asd[200];
        sprintf(asd, "Heading: %1.0f Pitch: %1.0f Roll: %1.0f", heading, pitch, roll);
        cr->show_text(asd);
        cr->fill();
    */
    if(coms->connection_strength() < 10)
    {
        cr->set_source_rgba(1.0, 0.0, 0.0, 1.0);
        cr->move_to(0, 0);
        cr->line_to(width, height);
        cr->stroke();

        cr->move_to(width, 0);
        cr->line_to(0, height);
        cr->stroke();
    }
    coms->unlock();
    return true;
}

