/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "logger.h"

#include "coms.h"

Logger::Logger()
{
    running = false;
}

bool Logger::begin(std::string path)
{
    if(running)
        return false;

    of.open(path);
    if(!of.is_open())
        return false;

    std::stringstream now;

    auto tp = std::chrono::system_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>( tp.time_since_epoch() );
    size_t modulo = ms.count() % 1000;

    time_t seconds = std::chrono::duration_cast<std::chrono::seconds>( ms ).count();

    char buffer[250];

    if(strftime(buffer, 250, "%Y-%m-%d %H:%M:%S.", localtime(&seconds)))
        now << buffer;

    now.fill('0');
    now.width(3);
    now << modulo;

    of << "<FlightLog>\n";
    of << "\t<DateTime>" << now.str() << "</DateTime>\n";
    of.flush();

    thr = std::thread(&Logger::logger_thread, this);
    return true;
}

void Logger::end()
{
    if(running)
    {
        running = false;
        thr.join();
        of << "</FlightLog>\n";
        of.flush();
        of.close();
    }
}


/*
 */
void Logger::log_message(std::string msg)
{
    if(running)
    {
        mute.lock();
        msg_vec.push_back(msg);
        mute.unlock();
    }
}

void Logger::logger_thread()
{
    msg_vec.clear();
    running = true;
    while(running)
    {
        mute.lock();
        bool ofopen = of.is_open();
        mute.unlock();

        if(!ofopen)
        {
            running = false;
            cout << "logger thread exiting" << endl;
            return;
        }

        std::stringstream ss;
        ss.precision(2);

        mute.lock();
        of << "\t<Entry>\n";

        for(int i = 0; i < msg_vec.size(); i++)
        {
            of << "\t\t<Pkt>";

            std::string msg;
            for(int j = 0; j < msg_vec[i].size(); j++)
            {
                std::stringstream hss;
                hss << std::hex << std::setfill('0');
                hss << std::setw(2) << uint32_t(msg_vec[i][j]);
                msg.append(hss.str(), hss.str().length()-2, 2);
                msg.append(" ");
            }

            of << msg << "</Pkt>\n";
        }
        msg_vec.clear();

        of << "\t</Entry>\n";
        of.flush();
        mute.unlock();
        sleep_ms(100);
    }
}
