#ifndef CONTROL_SETUP_H
#define CONTROL_SETUP_H

#include <gtkmm.h>
#include "coms.h"

#include <iostream>
using namespace std;


class ControlSetup : public Gtk::Window
{
public:
    ControlSetup(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    virtual ~ControlSetup()
    {
        return;
    }

    void show();
    bool on_delete_event(GdkEventAny* ent);
    void timeout();

protected:

    void on_next_clicked();
    void on_back_clicked();
    void on_close_clicked();
    void on_ok_clicked();

    Glib::RefPtr<Gtk::Builder> refBuilder;

    Gtk::Button* close_btn;
    Gtk::Button* ok_btn;
    Gtk::Button* next_btn;
    Gtk::Button* back_btn;

    Gtk::Notebook* notebook;

    Gtk::Label* channel_lbl[6];

    Gtk::Label* max_ele_down_lbl;

    int neutrals[6];

    int ele_up;
    int ele_down;
    int ail_left;
    int ail_right;
    int rud_left;
    int rud_right;
    int throttle_up;
    int throttle_down;

    bool ele_ok;
    bool ail_ok;
    bool rud_ok;
    bool thr_ok;
};




#endif // CONTROL_SETUP_H
