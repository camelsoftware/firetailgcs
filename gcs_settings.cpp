#include "gcs_settings.h"
#include <iostream>
#include <fstream>

using namespace std;

gcs_settings gcs_set;

void gcs_settings_init()
{
    #ifdef _WIN32
    sprintf(gcs_set.coms_port, "COM1");
    sprintf(gcs_set.terminal_port, "COM1");
    sprintf(gcs_set.radio_port, "COM1");
    #else
    sprintf(gcs_set.coms_port, "/dev/ttyUSB0");
    sprintf(gcs_set.terminal_port, "/dev/ttyACM0");
    sprintf(gcs_set.radio_port, "/dev/ttyUSB0");
    #endif

    sprintf(gcs_set.coms_addr, "localhost");

    gcs_set.coms_baud = 57600;
    gcs_set.terminal_baud = 115200;
    gcs_set.coms_udp_port = 5684;
    gcs_set.snd_mute = false;
    gcs_set.snd_disconnect_vol = 100;
    gcs_set.snd_alarm_vol = 100;
    gcs_set.snd_buzzer_vol = 100;

    std::string fname = Glib::get_home_dir();
    fname += "/.fls_rc";

    std::ifstream fs;
    fs.open(fname);
    if(!fs.is_open())
    {
        cout << "Couldn't open settings file! Using default settings instead." << endl;
        return;
    }
    fs.read((char*)&gcs_set, sizeof(gcs_set));
    fs.close();
}

void gcs_settings_write()
{
    std::string fname = Glib::get_home_dir();
    fname += "/.fls_rc";


    std::ofstream fs;
    fs.open(fname);
    if(!fs.is_open())
    {
        cout << "Error saving settings to " << fname << endl;
        return;
    }

    fs.write((char*)&gcs_set, sizeof(gcs_set));
    fs.close();
}

