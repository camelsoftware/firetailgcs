/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "connect.h"
#include "gcs_settings.h"

connectDlg::connectDlg()
{
    //create all of the widgets, starting with the notebook
    nb = Gtk::manage(new Gtk::Notebook());

    //create the coms page
    Gtk::VBox* box = Gtk::manage(new Gtk::VBox());

    //create the port label & entry
    Gtk::HBox* subbox = Gtk::manage(new Gtk::HBox());
    Gtk::Label* lbl = Gtk::manage(new Gtk::Label("Port"));
    coms_port_entry = Gtk::manage(new Gtk::Entry());
#ifndef _WIN32
    coms_port_entry->set_text(gcs_set.coms_port);
#else
    coms_port_entry->set_text(gcs_set.coms_port);
#endif

    //add to box
    subbox->pack_start(*lbl, true, true, 0);
    subbox->pack_start(*coms_port_entry, true, true, 0);
    box->pack_start(*subbox, true, true, 0);

    //create the baud label & entry
    subbox = Gtk::manage(new Gtk::HBox());
    lbl = Gtk::manage(new Gtk::Label("Baud"));
    coms_baud_entry = Gtk::manage(new Gtk::Entry());

    char bd[120];
    snprintf(bd, 120, "%i", gcs_set.coms_baud);
    coms_baud_entry->set_text(bd);

    //pack them into the box
    subbox->pack_start(*lbl, true, true, 0);
    subbox->pack_start(*coms_baud_entry, true, true, 0);
    box->pack_start(*subbox, true, true, 0);

    //add the coms box as a new page to the notebook
    nb->append_page(*box, "Coms");


	//UDP page
	Gtk::VBox* udpbox = Gtk::manage(new Gtk::VBox());

    //create the port label & entry
    subbox = Gtk::manage(new Gtk::HBox());
    lbl = Gtk::manage(new Gtk::Label("Hostname/IP"));
    udp_address_entry = Gtk::manage(new Gtk::Entry());
    udp_address_entry->set_text(gcs_set.coms_addr);

    //add to box
    subbox->pack_start(*lbl, true, true, 0);
    subbox->pack_start(*udp_address_entry, true, true, 0);
    udpbox->pack_start(*subbox, true, true, 0);

    //create the baud label & entry
    subbox = Gtk::manage(new Gtk::HBox());
    lbl = Gtk::manage(new Gtk::Label("Port"));
    udp_port_entry = Gtk::manage(new Gtk::Entry());

    char prt[120];
    snprintf(prt, 120, "%i", gcs_set.coms_udp_port);
    udp_port_entry->set_text(prt);

    //pack them into the box
    subbox->pack_start(*lbl, true, true, 0);
    subbox->pack_start(*udp_port_entry, true, true, 0);
    udpbox->pack_start(*subbox, true, true, 0);

    //add the coms box as a new page to the notebook
    nb->append_page(*udpbox, "UDP");

    nb->set_size_request(400, 150);
    //add the notebook to the dialog
    this->get_vbox()->pack_start(*nb, true, true, 0);
    this->show_all();

    //add the cancel & ok buttons
    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);

    Gtk::Button* ok_button = (Gtk::Button*)get_widget_for_response(Gtk::ResponseType::RESPONSE_OK);
    ok_button->signal_clicked().connect(sigc::mem_fun(*this, &connectDlg::ok_button_clicked));

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    show_all();
}

connectDlg::~connectDlg()
{

}


void connectDlg::ok_button_clicked()
{
    baud = atoi(coms_baud_entry->get_text().c_str());
    coms_port = coms_port_entry->get_text();

    udp_port = atoi(udp_port_entry->get_text().c_str());
    udp_addr = udp_address_entry->get_text();
    page = nb->get_current_page();

    gcs_set.coms_baud = baud;
    snprintf(gcs_set.coms_port, 120, "%s", coms_port.c_str());
    gcs_set.coms_udp_port = udp_port;
    snprintf(gcs_set.coms_addr, 120, "%s", udp_addr.c_str());
    gcs_settings_write();

    response(Gtk::RESPONSE_OK);
    hide();
}

void connectDlg::cancel_button_clicked()
{
    response(Gtk::RESPONSE_CANCEL);
    hide();
}

