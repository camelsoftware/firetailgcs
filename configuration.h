/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include <fstream>
#include <gtkmm.h>
#include "coms.h"
#include "ahrs_cal_window.h"
#include "control_setup.h"
#include "mixer_uploader.h"
#include "imumaths.h"


class SettingDialog : public Gtk::Dialog
{
public:
    SettingDialog(std::string setting);
    Gtk::Entry* val_entry;
};


class Config : public Gtk::Window
{
public:
    Config(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    virtual ~Config();


    void timeout();

    void show();

protected:
    void on_treeview_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* col);

    int title_to_val(std::string st);
    void set_title_val(std::string st, int val);
    void upload_setting_from_iter(Gtk::TreeModel::iterator iter);

    AhrsCal* ahrs_cal;
    ControlSetup* control_setup;
    MixerUploader* mixer_uploader;

    bool check_datalink();

    bool dispatch_message(uint16_t id);
    bool wait_for_message(uint16_t id);

    void on_offset_changed();
    void on_recal_ahrs_offset();
    void on_download_ahrs_offset();
    void on_upload_ahrs_offset();


    Glib::RefPtr<Gtk::Builder> refBuilder;

    //AHRS offset widgets
    Gtk::Entry* accel_bias_x;
    Gtk::Entry* accel_bias_y;
    Gtk::Entry* accel_bias_z;

    Gtk::Entry* mag_bias_x;
    Gtk::Entry* mag_bias_y;
    Gtk::Entry* mag_bias_z;

    Gtk::Entry* mag_gain_x;
    Gtk::Entry* mag_gain_y;
    Gtk::Entry* mag_gain_z;

    Gtk::Button* begin_cal_button;
    Gtk::Button* download_ahrs_cal_btn;
    Gtk::Button* upload_ahrs_cal_btn;

    void on_begin_ahrs_cal_clicked();
    void on_download_ahrs_cal_clicked();
    void on_upload_ahrs_cal_clicked();


    //Servo config widgets
    Gtk::SpinButton* rc_signal_scale;
    Gtk::Scale* aileron_min_scale;
    Gtk::Scale* aileron_max_scale;
    Gtk::Scale* elevator_min_scale;
    Gtk::Scale* elevator_max_scale;
    Gtk::Scale* rudder_min_scale;
    Gtk::Scale* rudder_max_scale;
    Gtk::Scale* throttle_min_scale;
    Gtk::Scale* throttle_max_scale;

    Gtk::CheckButton* ail_pwm_rev;
    Gtk::CheckButton* ail_ap_rev;
    Gtk::CheckButton* ail_servo_rev;
    Gtk::CheckButton* ele_pwm_rev;
    Gtk::CheckButton* ele_ap_rev;
    Gtk::CheckButton* ele_servo_rev;
    Gtk::CheckButton* rud_pwm_rev;
    Gtk::CheckButton* rud_ap_rev;
    Gtk::CheckButton* rud_servo_rev;
    Gtk::CheckButton* thr_pwm_rev;
    Gtk::CheckButton* thr_ap_rev;
    Gtk::CheckButton* thr_servo_rev;


    Gtk::Button* config_servo_button;
    Gtk::Button* upload_servo_config_button;
    Gtk::Button* download_servo_config_button;

    void on_setup_servo_clicked();
    void on_download_servo_config();
    void on_upload_servo_config();

    Gtk::ComboBoxText* mixer_config_combotext;
    Gtk::SpinButton* mixer_ratio_spinbutton;
    Gtk::Button* mixer_upload_button;
    Gtk::Button* mixer_config_download_button;
    Gtk::Button* mixer_config_upload_button;

    void on_upload_mixer_file();
    void on_download_mixer_config();
    void on_upload_mixer_config();

    Gtk::ComboBoxText* mode_select_channel;
    Gtk::ComboBoxText* mode_select_mode;
    Gtk::Button* mode_select_download_button;
    Gtk::Button* mode_select_upload_button;

    void on_download_mode_select();
    void on_upload_mode_select();

    //autopilot settings
    class ApModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:

        ApModelColumns()
        {
            add(col_column);
            add(col_setting);
            add(col_description);
            add(col_value);
        }

        Gtk::TreeModelColumn<Glib::ustring> col_column;
        Gtk::TreeModelColumn<Glib::ustring> col_setting;
        Gtk::TreeModelColumn<Glib::ustring> col_description;
        Gtk::TreeModelColumn<int> col_value;
    };

    ApModelColumns ap_columns;
    Gtk::TreeView* ap_treeview;
    Glib::RefPtr<Gtk::TreeStore> m_refTreeModel;

    //alarms settings
    Gtk::ComboBoxText* geofence_alarm_combo;
    Gtk::ComboBoxText* low_bat_alarm_combo;
    Gtk::ComboBoxText* no_gps_alarm_combo;
    Gtk::ComboBoxText* no_ctrl_alarm_combo;
    Gtk::ComboBoxText* no_datalink_alarm_combo;
    Gtk::Button* alarm_download_btn;
    Gtk::Button* alarm_upload_btn;

    Gtk::Button* save_config_btn;
    Gtk::Button* load_config_btn;
    Gtk::Button* edit_setting_btn;
    Gtk::Button* refresh_settings_btn;
    Gtk::Button* upload_all_settings_btn;

    void load_config();
    void save_config();
    void on_edit_setting();
    void on_refresh_settings();
    void on_upload_all_settings();


    void upload_pitch_roll_rates(int pp, int pi, int pd, int rp, int ri, int rd);
    void upload_max_angles(int pitch_up, int pitch_down, int roll, int pitch_rate, int roll_rate);
    void upload_pnr(int pp, int pi, int pd, int rp, int ri, int rd);
    void upload_hna(int hp, int hi, int hd, int vsp, int vsi, int vsd, int altp);
    void upload_max_vs(int climb, int descend);
    void upload_path_pid(int pathp, int radius, int loiter_gain);
    void upload_at_settings(int p, int i, int d, int vsff, int cruise_power, int cruise_speed, int min_thr);
    void upload_yd_tc_settings(int p, int i, int d, int yg, int pg);
    void upload_takeoff_settings(int launch_type, int max_roll, int runwaywidth, int takeoff_throttle, int rotate_speed, int climb_vs, int disconnect_alt, int bungee_accel, int time_delay);
    void upload_steering_gains(int outer, int inner);

    bool download_pitch_roll_rates();
    bool download_max_angles();
    bool download_pnr();
    bool download_hna();
    bool download_max_vs();
    bool download_path_pid();
    bool download_at_settings();
    bool download_yd_tc_settings();
    bool download_takeoff_settings();
    bool download_steering_gains();

    void on_alarm_upload();
    void on_alarm_download();

};



#endif // CONFIG_H_INCLUDED
