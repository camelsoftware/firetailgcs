#ifndef PLAN_FILE_H
#define PLAN_FILE_H

#include <gtkmm.h>
#include <vector>
#include <sstream>
#include <string>
/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <fstream>
#include <iostream>
#include "tinyxml2.h"

static const char* PLAN_FILE_VERSION = "0.1";


class PlanFile
{
public:
    PlanFile();

    struct wp
    {
        float lat, lng, alt;
    };

    struct runway
    {
        float lat1, lng1, alt1;
        float lat2, lng2, alt2;
        float width;
    };

    bool save(std::string fname);

    //returns 1 if file can't be opened
    // 2 if there is an XML error
    // 3 if version is bad
    // 0 on success
    bool load(std::string fname, Gtk::Window* w);

    int map_type;
    int zoom_level;
    double origin_lat;
    double origin_lng;
    std::vector<wp> waypoints;
    std::vector<wp> geopoints;
    std::vector<runway> runways;

    std::string version;
};

#endif // PLAN_FILE_H
