/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "coms.h"
#include <gtkmm.h>
#include <giomm/file.h>


shared_ptr<Coms> default_coms = nullptr;
std::vector< shared_ptr<Coms> > coms_vec;


Coms coms_default;
static int active_connection = 0;

static int coms_id = 0;

shared_ptr<Coms> Coms::add_new_connection()
{
    shared_ptr<Coms> nc(new Coms);
    cout << "created new coms: " << nc->id << endl;
    coms_vec.push_back(nc);
    return nc;
}

void Coms::remove_connection(int n)
{
    std::vector< shared_ptr<Coms> >::iterator it = coms_vec.begin()+n;
    if((it >= coms_vec.begin()) && (it < coms_vec.end()))
    {
        coms_vec[n]->stop();
        coms_vec.erase(it);
    }
    else
    {
        cout << "remove connection failed" << endl;
    }
}

shared_ptr<Coms> Coms::get_instance(int n)
{
    if(n >= coms_vec.size())
    {
        if(default_coms == nullptr)
        {
            shared_ptr<Coms> tmp(new Coms);
            cout << "created new coms: " << tmp->id << endl;
            default_coms = tmp;
        }
        return default_coms;
    }
    return coms_vec[n];
}

shared_ptr<Coms> Coms::get_active_instance()
{
    if(active_connection >= coms_vec.size())
    {
        if(default_coms == nullptr)
        {
            shared_ptr<Coms> tmp(new Coms);
            cout << "created new coms: " << tmp->id << endl;
            default_coms = tmp;
        }
        return default_coms;
    }
    return coms_vec[active_connection];
}

int Coms::get_n_connections()
{
    return coms_vec.size();
}

void Coms::set_active_connection(int n)
{
    active_connection = n;
}

int Coms::get_active_connection()
{
    return active_connection;
}

Coms::Coms()
{
    cout << "created" << endl;
    id = coms_id++;
    keep_going = false;
    enable_stream(false);
    sock = 0;
    home_lat = home_lng = home_alt = 0;
    set_v_stream_ptr(Firelink::VS_INST, (uint8_t*)&this->inst);
    set_v_stream_ptr(Firelink::VS_SYSTEMS, (uint8_t*)&this->systems);
    set_v_stream_ptr(Firelink::VS_AP_STATUS, (uint8_t*)&this->ap_status);
    set_v_stream_ptr(Firelink::VS_RAW_MARG, (uint8_t*)&this->marg);
    set_v_stream_ptr(Firelink::VS_RAW_PWM, (uint8_t*)&this->pwm);
    set_v_stream_ptr(Firelink::VS_SYSTEMS_HEALTH, (uint8_t*)&this->sys_health);
    memset((void*)&inst, 0, sizeof(inst));
    memset((void*)&systems, 0, sizeof(systems));
    memset((void*)&ap_status, 0, sizeof(ap_status));
    memset((void*)&sys_health, 0, sizeof(sys_health));
    pack_count = last_pack_count = 0;
    last_plan_point = 0;
    new_inst_settings = false;
    cout << "done creating" << endl;
}

Coms::~Coms()
{
}

bool Coms::get_autothrottle_on()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_AUTOTHROTTLE+1);
}

bool Coms::get_autolaunch_on()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_AUTOLAUNCH+1);
}

bool Coms::get_motor_armed()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_MOTOR+1);
}

bool Coms::get_has_rc_signal()
{
    return !read_bit(systems.switches, Firelink::SYSTEM_SWITCH_NO_SIGNAL+1);
}

bool Coms::get_is_inside_geofence()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_HIT_BOUNDARY+1);
}

void Coms::lock()
{
    mute.lock();
}

void Coms::unlock()
{
    mute.unlock();
}

int Coms::connection_strength()
{
    if(m == COMS_LOG_PLAYBACK)
        return 15;
    if(!keep_going)
        return 0;
    return last_pack_count;
}

bool Coms::running()
{
    return keep_going;
}


bool Coms::start()
{
    //don't try to start thread if it is already running
    if(keep_going)
        return false;
    clear_in_queue();
    clear_queue();
    if(m == COMS_SERIAL)
    {
        sp.setPort(address);
        sp.setBaudrate(baudport);
        serial::Timeout st = serial::Timeout::simpleTimeout(25);
        st.write_timeout_constant = 10;
        sp.setTimeout(st);
        try
        {
            sp.open();
        }
        catch(...)
        {
            cout << "exception caught" << endl;
            cout << "errno: " << errno << endl;
            return false;
        }
        if(!sp.isOpen())
            return false;
    }
    if(m == COMS_UDP)
    {
        if(sock != 0)
        {
            cout << "socket != 0" << endl;
            return false;
        }
        sock = shared_ptr<UDPSocket>(new UDPSocket);
        if(sock->listen(5665) == -1)
        {
            cout << "socket listen failed." << endl;
            return false;
        }
        if(!sock->set_timeout(30000))
            return false;
        if(!sock->set_write_address(address.c_str(), baudport))
            return false;
    }
    keep_going = false;
    thr = std::thread(&Coms::ComsThread, this);
    for(int i = 0; i < 100; i++)
    {
        if(keep_going)
            return true;
        sleep_ms(10);
    }
    return false;
}

void Coms::stop()
{
    if(keep_going)
    {
        keep_going = false;
        thr.join();
    }
    m = COMS_IDLE;
}

bool Coms::wait_for_dispatch(uint16_t pid)
{
    lock();
    clear_in_queue(); //clear in queue so that reply is 'fresh'
    unlock();
    int tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        lock();
        if(!on_queue(pid))
        {
            unlock();
            return true;
        }
        unlock();
        tries++;
    }
    clear_queue();
    return false;
}

bool Coms::wait_for_message(uint16_t pid)
{
    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        lock();
        oq = on_in_queue(pid);
        unlock();
        if(oq)
            break;
        tries++;
    }
    return oq;
}


void Coms::push_log_pack(std::string pk)
{
    lock();
    read_pack((unsigned char*)pk.c_str());
    unlock();
}

void Coms::ComsThread()
{
    if(m != COMS_LOG_PLAYBACK)
    {
        try
        {
            std::string fname = Glib::get_home_dir();
            fname += "/.firetailgcs/logs";
            try
            {
                Glib::RefPtr<Gio::File> pfile = Gio::File::create_for_path(fname);
                if(!pfile->query_exists())
                    pfile->make_directory_with_parents();
            }
            catch(...)
            {
            }
            fname += "/";
            fname += currentDateTime();
            fname += ".log";
            logger.begin(fname);
        }
        catch(...)
        {
            cout << "Exception throw while starting logger" << endl;
        }
    }
    keep_going = true;
    lock();
    time_t pps_time = time(NULL);
    while(keep_going)
    {
        if(difftime(time(NULL), pps_time) >= 1)
        {
            last_pack_count = pack_count;
            pack_count = 0;
            pps_time = time(NULL);
        }
        if((queue_size() == 0) && (!stream_enabled()) && (m != COMS_LOG_PLAYBACK))
        {
            if(difftime(time(NULL), keep_alive_time) >= 1)
            {
                set_keep_alive(keep_alive_seconds);
                keep_alive_time = time(NULL);
            }
        }
        if(m == COMS_UDP)
        {
            udp();
            unlock();
            sleep_ms(10);
            lock();
        }
        if(m == COMS_SERIAL)
        {
            serial();
            unlock();
            sleep_ms(18);
            lock();
        }
        remove_pack(Firelink::SM_SET_KEEP_ALIVE);
    }
    if(m == COMS_SERIAL)
        sp.close();
    unlock();
    if(m != COMS_LOG_PLAYBACK)
        logger.end();
}


bool Coms::serial()
{
    try
    {
        unsigned char buf[60];
        int pn = next_pack(buf);
        if(pn >= 0)
        {
            sp.write(buf, Firelink::PACKET_SIZE);
        }
        bool ret = false;
        int i = 0;
        std::string inmsg;
        while(sp.available())
        {
            unsigned char inc = 0;
            try
            {
                inc = sp.read(1)[0];
            }
            catch(...)
            {
                cout << "exception caught reading from serial port" << endl;
            }
            //cout << (char)inc;
            i++;
            inmsg += inc;
            int eret = encode(inc);
            if(eret != -1)
            {
                std::string msg = std::string((const char*)&inmsg[i-Firelink::PACKET_SIZE+1], Firelink::PACKET_SIZE);
                logger.log_message(msg);
                ret = true;
                pack_count++;
            }
        }
        if(ret)
            return true;
    }
    catch(...)
    {
        cout << "Exception during coms iteration. Radio unplugged?" << endl;
    }
    return false;
}

bool Coms::udp()
{
    unsigned char buf[Firelink::PACKET_SIZE+1];
    int np = next_pack(buf);
    if(np >= 0)
    {
        if(sock->write((char*)buf, Firelink::PACKET_SIZE) == -1)
        {
            cout << "bad socket write" << endl;
            return false;
        }
    }
    memset(buf, '\0', Firelink::PACKET_SIZE);
    if(sock->read(buf, Firelink::PACKET_SIZE) < 35)
    {
        return false;
    }
    if(read_pack(buf))
    {
        std::string msg = std::string((const char*)buf, Firelink::PACKET_SIZE);
        logger.log_message(msg);
        pack_count++;
        return true;
    }
    return false;
}

bool Coms::on_recv_satellites(uint8_t* data)
{
    Firelink::m_satellites* ms = (Firelink::m_satellites*)data;
    if(ms->sat_num > 11)
        return true;
    sat_reports[ms->sat_num].prn = ms->prn;
    sat_reports[ms->sat_num].ss = ms->ss;
    sat_reports[ms->sat_num].azimuth = ms->azimuth;
    sat_reports[ms->sat_num].elevation = ms->elevation;
    return true;
}

bool Coms::on_recv_home_coordinates(uint8_t* data)
{
    Firelink::m_home_coordinates* hc = (Firelink::m_home_coordinates*)data;
    home_lat = hc->lat/1000000.0;
    home_lng = hc->lng/1000000.0;
    home_alt = hc->alt;
    return true;
}

bool Coms::on_recv_alarms(uint8_t* data)
{
    Firelink::m_alarms* ma = (Firelink::m_alarms*)data;
    switch(ma->alarm)
    {
    case Firelink::ALARM_LOW_BAT:
    {
        std::string msg_txt = "Low battery!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_BOUNDARY:
    {
        std::string msg_txt = "Geofence has been hit!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_NO_GPS:
    {
        std::string msg_txt = "No GPS position fix!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_NO_SETTINGS:
    {
        std::string msg_txt = "Could not load settings!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_NO_RC:
    {
        alarm_msgs.push_back("Remote control disconnected.");
    }
    break;
    case Firelink::ALARM_NO_DATALINK:
    {
        alarm_msgs.push_back("Datalink lost.");
    }
    break;
    case Firelink::ALARM_SCRIPT_FAULT:
    {
        alarm_msgs.push_back("Script terminated.");
    }
    break;
    }
    return true;
}

bool Coms::on_recv_sensing_status(uint8_t* data)
{
    Firelink::m_sensing_status* ms = (Firelink::m_sensing_status*)data;
    sense_status = *ms;
    n_sensors = 0;
    if(sense_status.main_gyro_ok != Firelink::SENSOR_STATUS_NOT_AVAILABLE)
        n_sensors++;
    if(sense_status.main_accel_ok != Firelink::SENSOR_STATUS_NOT_AVAILABLE)
        n_sensors++;
    if(sense_status.backup_gyro_ok != Firelink::SENSOR_STATUS_NOT_AVAILABLE)
        n_sensors++;
    if(sense_status.backup_accel_ok != Firelink::SENSOR_STATUS_NOT_AVAILABLE)
        n_sensors++;
    if(sense_status.magnetometer_ok != Firelink::SENSOR_STATUS_NOT_AVAILABLE)
        n_sensors++;
    if(sense_status.baro_press_sense_ok != Firelink::SENSOR_STATUS_NOT_AVAILABLE)
        n_sensors++;
    n_sensors += (int)sense_status.n_db485_slaves;
    return true;
}

int Coms::get_n_sensors()
{
    return n_sensors;
}

int Coms::get_n_alarms()
{
    return alarm_msgs.size();
}

void Coms::send_script_message(char* pmsg)
{
    Firelink::m_script_msg msg;
    sprintf(msg.msg, pmsg);
    Firelink::flp pack;
    pack.id = Firelink::SM_SET_SCRIPT_MSG;
    memcpy(pack.data, &msg, sizeof(msg));
    push_pack(pack);
}

std::string Coms::get_next_alarm()
{
    std::string ret;
    if(get_n_alarms())
    {
        ret = alarm_msgs[0];
        alarm_msgs.erase(alarm_msgs.begin());
    }
    return ret;
}

bool Coms::on_recv_n_points(uint8_t* data)
{
    Firelink::m_n_points* np = (Firelink::m_n_points*)data;
    plan_points.resize(np->wp);
    fence_points.resize(np->gp);
    return false;
}

bool Coms::on_recv_point(uint8_t* data)
{
    Firelink::m_point* mp = (Firelink::m_point*)data;
    if(mp->type == Firelink::POINT_TYPE_WAYPOINT)
    {
        if(mp->n >= plan_points.size())
            return true;
        waypoint wp;
        last_plan_point = mp->n;
        wp.alt = mp->alt;
        wp.pos.lat = mp->lat/1000000.0;
        wp.pos.lng = mp->lng/1000000.0;
        plan_points[mp->n] = wp;
    }
    if(mp->type == Firelink::POINT_TYPE_GEOPOINT)
    {
        if(mp->n >= fence_points.size())
            return true;
        waypoint wp;
        last_fence_point = mp->n;
        wp.alt = mp->alt;
        wp.pos.lat = mp->lat/1000000.0;
        wp.pos.lng = mp->lng/1000000.0;
        fence_points[mp->n] = wp;
    }
    return true;
}

bool Coms::on_recv_vehicle_name(uint8_t* data)
{
    Firelink::m_vehicle_name* mv = (Firelink::m_vehicle_name*)data;
    vehicle_name = mv->title;
    return true;
}

bool Coms::on_recv_instrument_settings(uint8_t* data)
{
    Firelink::m_instrument_settings* is = (Firelink::m_instrument_settings*)data;
    inst_setting_alt_setting = roundf(is->alt_pascals/100.0f);
    inst_setting_gnd_ref = is->ground_alt;
    inst_setting_mag_var = roundf(is->mag_var/100.0f);
    new_inst_settings = true;
    return true;
}

bool Coms::on_recv_debug_msg(uint8_t* data)
{
    char* msg = (char*)data;
    cout << msg << flush;
    return true;
}

bool Coms::on_recv_script_msg(uint8_t* data)
{
    char* msg = (char*)data;
    script_msgs.push_back(msg);
    cout << msg << endl;
    return true;
}

int Coms::get_pwm_inputs(int c)
{
    return pwm.channels[c];
}

float Coms::get_heading()
{
    return inst.heading/100.0;
}
float Coms::get_pitch()
{
    return inst.pitch/100.0;
}
float Coms::get_roll()
{
    return inst.roll/100.0;
}
float Coms::get_ap_pitch()
{
    return ap_status.pitch/100.0;
}
float Coms::get_ap_roll()
{
    return ap_status.roll/100.0;
}
float Coms::get_ap_heading()
{
    return ap_status.heading/100.0;
}
float Coms::get_alt()
{
    return inst.baro_alt;
}
float Coms::get_vert_speed()
{
    return inst.vert_speed;
}
float Coms::get_ap_alt()
{
    return ap_status.alt;
}
float Coms::get_air_speed()
{
    return inst.air_speed/100.0;
}
float Coms::get_latitude()
{
    return inst.latitude/1000000.0;
}
float Coms::get_longitude()
{
    return inst.longitude/1000000.0;
}
int Coms::get_satellites_used()
{
    return systems.sats_used;
}
Firelink::AP_MODE Coms::get_ap_mode()
{
    return ap_status.mode;
}

Firelink::vs_raw_marg Coms::get_raw_marg()
{
    return marg;
}
Firelink::vs_raw_pwm Coms::get_raw_pwm()
{
    return pwm;
}
int Coms::get_ap_waypoint()
{
    return ap_status.wp_number;
}
float Coms::get_ground_speed()
{
    return systems.ground_speed/100.0;
}
float Coms::get_ground_track()
{
    return systems.ground_track/100.0;
}
Firelink::GPS_LOCK Coms::get_gps_mode()
{
    return systems.gps_lock;
}
uint32_t Coms::get_timer()
{
    return systems.timer;
}
float Coms::get_temperature()
{
    return systems.temperature/100.0;
}
float Coms::get_ap_waypoint_lat()
{
    return ap_status.wp_lat/1000000.0;
}
float Coms::get_ap_waypoint_lng()
{
    return ap_status.wp_lng/1000000.0;
}
int Coms::get_power_remaining()
{
    return systems.power_remaining;
}
float Coms::get_amps()
{
    return systems.milliamps/1000.0;
}
float Coms::get_volts()
{
    return systems.volts/100.0;
}

float Coms::get_main_avion_volts()
{
    return sys_health.main_avion_volts/100.0;
}
float Coms::get_stby_avion_volts()
{
    return sys_health.stby_avion_volts/100.0;
}

std::string Coms::get_vehicle_title()
{
    return vehicle_name;
}

bool Coms::have_new_instrument_settings()
{
    return new_inst_settings;
}
float Coms::get_altimeter_setting()
{
    return inst_setting_alt_setting;
}
float Coms::get_ground_altitude()
{
    return inst_setting_gnd_ref;
}
float Coms::get_mag_var_setting()
{
    return inst_setting_mag_var;
}
void Coms::reset_inst_settings_flag()
{
    new_inst_settings = false;
}

