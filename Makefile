CC=i686-w64-mingw32-g++
GCC=i686-w64-mingw32-gcc -fpermissive
CFLAGS= -I./threads/Windows -I./osmmaps -I./ -std=c++11 -c `pkg-config gtkmm-3.0 libsoup-2.4 --cflags`
LDFLAGS= -L../../../ -std=c++11 -I./threads/Windows -I./osmmaps `pkg-config gtkmm-3.0 libsoup-2.4 --cflags`
CPP_SOURCES=$(wildcard *.cpp) $(wildcard FireLink/*.cpp) $(wildcard threads/Windows/*.cpp) $(wildcard squirrel/*.cpp) $(wildcard squirrel/sqstdlib/*.cpp)
C_SOURCES=$(wildcard osmmaps/*.c) 
CC_SOURCES=./serial/src/serial.cc ./serial/src/impl/win.cc
OBJECTS=$(CPP_SOURCES:.cpp=.o) $(CC_SOURCES:.cc=.o) $(C_SOURCES:.c=.o)
LIBS= `pkg-config gtkmm-3.0 glibmm-2.4 libsoup-2.4 --libs` -lOpenAL32 -lsndfile -mwindows -lwsock32 -lpthread
EXECUTABLE=firetailgcs.exe


$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@ $(LIBS)
	cp firetailgcs.exe win32/bin

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
	
.c.o:
	$(GCC) $(CFLAGS) $< -o $@
	
.cc.o:
	$(CC) $(CFLAGS) $< -o $@
	
objects:
	echo $(OBJECTS)
	
clean:
	find . -name \*.o -execdir rm {} \;
	rm -f flstation.exe
