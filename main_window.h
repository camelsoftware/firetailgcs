/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAIN_WINDOW_H_INCLUDED
#define MAIN_WINDOW_H_INCLUDED

#include <gtkmm.h>
#include "coms.h"
#include "flight_display.h"
#include "planner.h"
#include "connect.h"
#include "configuration.h"
#include "logwindow.h"
#include "radio.h"
#include "term.h"
#include "about.h"



class mainWindow : public Gtk::Window
{
public:
    mainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~mainWindow();


private:
    void on_connect_button_clicked();
    void on_fly_button_clicked();
    void on_plan_button_clicked();
    void on_configure_button_clicked();
    void on_logs_button_clicked();
    void on_radio_button_clicked();
    void on_terminal_button_clicked();
    void on_about_button_clicked();

    bool timer(int tn);

    bool on_delete_event(GdkEventAny* ent);


    class ComsColumns : public Gtk::TreeModelColumnRecord
    {
    public:

        ComsColumns()
        {
            add(m_vehiclename);
            add(m_port);
            add(m_pps);
            add(m_active);
        }

        Gtk::TreeModelColumn<std::string> m_vehiclename;
        Gtk::TreeModelColumn<std::string> m_port; //port name
        Gtk::TreeModelColumn<int> m_pps; //packets per second
        Gtk::TreeModelColumn<bool> m_active;
    };

    Gtk::TreeView* coms_tree;
    ComsColumns coms_columns;
    Glib::RefPtr<Gtk::ListStore> refcomslist;

    Gtk::CellRendererToggle toggle_renderer;
    Gtk::TreeView::Column m_toggle_column;

    void on_active_toggled(Glib::ustring path);
    void on_treeview_row_activated(const Gtk::TreeModel::Path& path,Gtk::TreeViewColumn* column);
    void toggle_cell_data_func(Gtk::CellRenderer* ren, const Gtk::TreeModel::iterator& iter);


    Glib::RefPtr<Gtk::Builder> refBuilder;

    FlightDisplay* flightdisplay;
    Planner* planner;
    Config* config;
    RadioWindow* radio;
    Terminal* terminal;
    LogWindow* logwindow;

    Gtk::Button* connect;
    Gtk::Button* plan;
    Gtk::Button* fly;
    Gtk::Button* configure;
    Gtk::Button* logs;
    Gtk::Button* radio_btn;
    Gtk::Button* term;
    Gtk::Button* about;

    Gtk::Label* connected_lbl;
    Gtk::Label* con_strength_lbl;

    Gtk::Label* connect_lbl;

    int redraw_timeout = 0; //counter to constrain redraw rate so it doesnt' chew CPU
};




#endif // MAIN_WINDOW_H_INCLUDED
