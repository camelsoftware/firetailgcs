/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     Camel's Dead Simple OpenAL wrapper. Makes it dead simple to play wav files.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CAMWAV_H
#define CAMWAV_H

#include <AL/al.h>
#include <AL/alc.h>
#include <sndfile.h>
#include <string>

namespace camwav
{

class Buffer
{
public:
    Buffer() {}
	Buffer(std::string fname);
	~Buffer();

    bool create_from_file(std::string fname);

	ALuint get_al_buffer() { return buffer; }
	int get_error() { return error; }

private:
	ALvoid *data;
	ALuint buffer;
	int error;
};


class Sound
{
public:
    Sound() {}
	Sound(Buffer* buf);
	~Sound();

    void set_buffer(Buffer* buf);

	int get_error() { return error; }

	void play();
	void stop();
	void pause();

	bool is_playing();

	void set_loop(bool l) { alSourcei(source, AL_LOOPING, l); }

	void set_volume(float vol);

private:
	ALuint source;
	int error;
};



};


#endif

