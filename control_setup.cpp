#include "control_setup.h"

ControlSetup::ControlSetup(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("ctrl_lbl_chan1", channel_lbl[0]);
    refBuilder->get_widget("ctrl_lbl_chan2", channel_lbl[1]);
    refBuilder->get_widget("ctrl_lbl_chan3", channel_lbl[2]);
    refBuilder->get_widget("ctrl_lbl_chan4", channel_lbl[3]);
    refBuilder->get_widget("ctrl_lbl_chan5", channel_lbl[4]);
    refBuilder->get_widget("ctrl_lbl_chan6", channel_lbl[5]);

    refBuilder->get_widget("ctrl_back_btn", back_btn);
    refBuilder->get_widget("ctrl_next_btn", next_btn);
    refBuilder->get_widget("ctrl_close_btn", close_btn);
    refBuilder->get_widget("ctrl_ok_btn", ok_btn);

    back_btn->signal_clicked().connect(sigc::mem_fun(*this, &ControlSetup::on_back_clicked));
    next_btn->signal_clicked().connect(sigc::mem_fun(*this, &ControlSetup::on_next_clicked));
    ok_btn->signal_clicked().connect(sigc::mem_fun(*this, &ControlSetup::on_ok_clicked));
    close_btn->signal_clicked().connect(sigc::mem_fun(*this, &ControlSetup::on_close_clicked));

    refBuilder->get_widget("ctrl_notebook", notebook);
}

void ControlSetup::show()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    coms->set_vh_stream_prio(Firelink::VS_RAW_PWM, Firelink::PACK_PRIO_HIGH);
    coms->unlock();

    notebook->set_current_page(0);
    ok_btn->set_visible(false);
    back_btn->set_visible(false);

    Gtk::Window::show();
}

bool ControlSetup::on_delete_event(GdkEventAny* ent)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    coms->lock();
    coms->set_vh_stream_prio(Firelink::VS_RAW_PWM, Firelink::PACK_PRIO_OFF);
    coms->unlock();

    return false;
}

void ControlSetup::timeout()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    Firelink::vs_raw_pwm pwm_inputs = coms->get_raw_pwm();
    coms->unlock();

    for(int i = 0; i < 6; i++)
    {
        stringstream ss;
        ss << pwm_inputs.channels[i];
        channel_lbl[i]->set_text(ss.str());
    }
}


void ControlSetup::on_next_clicked()
{
    int current_page = notebook->get_current_page();
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    switch(current_page)
    {
    case 0:
    {
        for(int i = 0; i < 6; i++)
            neutrals[i] = coms->get_pwm_inputs(i);
    }
    break;
    case 1:
    {
        ele_up = coms->get_pwm_inputs(1);
    }
    break;
    case 2:
    {
        ele_down = coms->get_pwm_inputs(1);
    }
    break;
    case 3:
    {
        ail_left = coms->get_pwm_inputs(0);
    }
    break;
    case 4:
    {
        ail_right = coms->get_pwm_inputs(0);
    }
    break;
    case 5:
    {
        rud_left = coms->get_pwm_inputs(3);
    }
    break;
    case 6:
    {
        rud_right = coms->get_pwm_inputs(3);
    }
    break;
    case 7:
    {
        throttle_up = coms->get_pwm_inputs(2);
    }
    break;
    case 8:
    {
        throttle_down = coms->get_pwm_inputs(2);
    }
    break;
    }
    int n_pages = notebook->get_n_pages();
    notebook->set_current_page(current_page+1);
    current_page = notebook->get_current_page();

    if(current_page == n_pages-1)
    {
        ok_btn->set_visible(true);
        next_btn->set_visible(false);
    }
    else
    {
        ok_btn->set_visible(false);
        next_btn->set_visible(true);
    }

    if(current_page <= 0)
        back_btn->set_visible(false);
    else
        back_btn->set_visible(true);
}

void ControlSetup::on_back_clicked()
{
    int current_page = notebook->get_current_page();
    int n_pages = notebook->get_n_pages();
    notebook->set_current_page(current_page-1);
    current_page = notebook->get_current_page();

    if(current_page == n_pages-1)
    {
        ok_btn->set_visible(true);
        next_btn->set_visible(false);
    }
    else
    {
        ok_btn->set_visible(false);
        next_btn->set_visible(true);
    }

    if(current_page <= 0)
        back_btn->set_visible(false);
    else
        back_btn->set_visible(true);
}

void ControlSetup::on_close_clicked()
{
    Gtk::Window::hide();
}

void ControlSetup::on_ok_clicked()
{

}


