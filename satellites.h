/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SATELLITES_H_INCLUDED
#define SATELLITES_H_INCLUDED


#include <gtkmm.h>
#include <thread>
#include <mutex>
#include "coms.h"

class SatellitesWindow : public Gtk::Window
{
public:
    SatellitesWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~SatellitesWindow();

    void show();

    void timeout();

private:
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:

        ModelColumns()
        {
            add(m_col_id);
            add(m_col_prn);
            add(m_col_azimuth);
            add(m_col_elevation);
            add(m_col_snr);
        }

        Gtk::TreeModelColumn<unsigned int> m_col_id;
        Gtk::TreeModelColumn<int> m_col_prn;
        Gtk::TreeModelColumn<int> m_col_azimuth;
        Gtk::TreeModelColumn<int> m_col_elevation;
        Gtk::TreeModelColumn<int> m_col_snr;
    };

    ModelColumns m_Columns;
    Gtk::TreeView* treeview;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;

    Glib::RefPtr<Gtk::Builder> refBuilder;

    int timeout_ctr;
    int sat_ctr;

};


#endif // SATELLITES_H_INCLUDED
