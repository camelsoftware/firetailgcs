#include "satellites.h"

/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <iostream>
using namespace std;

SatellitesWindow::SatellitesWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("satellite_treeview", treeview);

    Gtk::Button* close_btn;
    refBuilder->get_widget("satellite_close_button", close_btn);
    close_btn->signal_clicked().connect(sigc::mem_fun(*this, &Gtk::Window::hide));

    m_refTreeModel = Gtk::ListStore::create(m_Columns);
    treeview->set_model(m_refTreeModel);

    treeview->append_column("#", m_Columns.m_col_id);
    treeview->append_column("PRN", m_Columns.m_col_prn);
    treeview->append_column("Azimuth", m_Columns.m_col_azimuth);
    treeview->append_column("Elevation", m_Columns.m_col_elevation);
    treeview->append_column("SNR", m_Columns.m_col_snr);
}

SatellitesWindow::~SatellitesWindow()
{

}

void SatellitesWindow::timeout()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    timeout_ctr++;
    if(timeout_ctr > 5)
    {
        timeout_ctr = 0;
        if(!coms->on_queue(Firelink::SM_GET_SATELLITES))
        {
            if(sat_ctr >= coms->get_satellites_used())
                sat_ctr = 0;

            coms->lock();
            Firelink::m_satellites ms;
            ms.sat_num = sat_ctr;
            coms->get_satellites(&ms, sizeof(ms));
            coms->unlock();
            sat_ctr++;
        }

        m_refTreeModel->clear();
        for(int i = 0; i < coms->get_satellites_used(); i++)
        {
            Gtk::TreeModel::Row row = *(m_refTreeModel->append());
            row[m_Columns.m_col_id] = i;
            row[m_Columns.m_col_prn] = coms->sat_reports[i].prn;
            row[m_Columns.m_col_azimuth] = coms->sat_reports[i].azimuth;
            row[m_Columns.m_col_elevation] = coms->sat_reports[i].elevation;
            row[m_Columns.m_col_snr] = coms->sat_reports[i].ss;
        }
    }
}
