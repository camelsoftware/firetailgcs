/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef UTILITY_H_INCLUDED
#define UTILITY_H_INCLUDED

#include <string>
#include <sstream>
#include <math.h>
#include <stdint.h>
#include <ctime>
#include "imumaths.h"
#include "FireLink/firelink.h"

#ifdef _WIN32
#include <winsock2.h>
#endif

#ifndef M_PI
#define M_PI 3.14159265359
#endif // M_PI

template <class T> class BrownLinearExpo
{
public:
    BrownLinearExpo()
    {
        a = 0.5;
    }
    BrownLinearExpo(float f, T init_est)
    {
        a = f;
        estimate = init_est;
    }

    void set_factor(float factor)
    {
        a = factor;
    }
    void step(T measurement)
    {
        single_smoothed = a * measurement + (1 - a) * single_smoothed;
        double_smoothed = a * single_smoothed + (1 - a) * double_smoothed;

        T est_a = (2*single_smoothed - double_smoothed);
        T est_b = (a / (1-a) )*(single_smoothed - double_smoothed);
        estimate = est_a + est_b;
    }
    T get()
    {
        return estimate;
    }

private:
    T estimate, double_smoothed, single_smoothed;
    float a;
};


//a pretty cool C string splitter
class Tokeniser
{
public:
    Tokeniser(const char* str, char token);

    bool next(char* out, int len);

private:
    const char* str;
    char token;
};



void sleep_ms(int ms);
int roundf(double r);
double to_degrees(double in);
double to_radians(double in);

int millis();


struct coord
{
	float lat, lng;
};

class waypoint
{
public:
	waypoint() { used = false; }

	coord pos;
	int alt;
	bool used;
};


coord make_coord(int32_t lat, int32_t lng); //converts lat and lng expressed in millions of degrees
                                            //to a coord struct that has floating point numbers
coord make_coordf(float lat, float lng);

float bearing_from_to(coord from, coord to);
float distance_between(coord a, coord b);
float cross_track_distance(coord from, coord to, coord here);


extern std::string share_uri;

const int fl_timeout = 60; //try this many times to send/recv a packet
const int fl_attempt_delay = 20; //delay between each attempt


std::string make_padded_string(std::string start, std::string finish, char padding, int length);

double CCW(imu::Vector<2> p1, imu::Vector<2> p2, imu::Vector<2> p3);
std::string hex_to_string(std::string in);

std::string alarm_action_to_string(Firelink::ALARM_ACTION ac);
Firelink::ALARM_ACTION string_to_alarm_action(std::string stac);

uint8_t read_bit(uint8_t bte, uint8_t n);
uint8_t set_bit(uint8_t bte, uint8_t n, bool on);

const std::string currentDateTime();

#endif // UTILITY_H_INCLUDED
