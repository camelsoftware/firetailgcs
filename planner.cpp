/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "planner.h"
#include <iomanip>

Planner* planner;
static OsmGpsMap* planner_map;
static OsmGpsMapLayer* osd;
static OsmGpsMapTrack* track;
static OsmGpsMapPolygon* fence;
static OsmGpsMapImage* cursor;

int click_down_x, click_down_y;

static gboolean on_track_changed(GtkWidget* widget);

static gboolean
on_map_changed(GtkWidget* widget, gboolean* ptr)
{
    planner->map_zoom_spin->set_value(osm_gps_map_get_zoom(planner_map));
    return true;
}


static gboolean
on_fence_point_added(GtkWidget* widget, gboolean* ptr)
{
    OsmGpsMapPoint* point = (OsmGpsMapPoint*)ptr;
    Gtk::TreeModel::Row row = *(planner->refgpListStore->append());
    row[planner->m_Columns.m_col_latitude] = to_degrees(point->rlat);
    row[planner->m_Columns.m_col_longitude] = to_degrees(point->rlon);
    row[planner->m_Columns.m_col_altitude] = 350;

    int nrows = planner->refgpListStore->children().size();
    if(nrows > 1)
    {
        Gtk::TreeModel::Row last_row = planner->refgpListStore->children()[nrows-2];
        float last_alt = last_row[planner->m_Columns.m_col_altitude];
        int last_n = last_row[planner->m_Columns.m_col_number];
        row[planner->m_Columns.m_col_altitude] = last_alt;
        row[planner->m_Columns.m_col_number] = last_n+1;
    }
    planner->update_fence_warning();
    return false;
}

static gboolean
on_fence_point_inserted(GtkWidget* widget, int pos)
{
    Gtk::TreeModel::iterator iter = planner->refgpListStore->insert(planner->refgpListStore->children()[pos]);
    Gtk::TreeModel::Row row = *iter;
    OsmGpsMapTrack* trk = osm_gps_map_polygon_get_track(fence);
    OsmGpsMapPoint* point = osm_gps_map_track_get_point(trk, pos);
    row[planner->m_Columns.m_col_latitude] = to_degrees(point->rlat);
    row[planner->m_Columns.m_col_longitude] = to_degrees(point->rlon);

    iter--;
    row = *iter;
    int counter = row[planner->m_Columns.m_col_number];
    while((iter != planner->refgpListStore->children().end())
            && (planner->refgpListStore->iter_is_valid(iter)))
    {
        row = *iter;
        row[planner->m_Columns.m_col_number] = counter;
        counter++;
        iter++;
    }

    planner->update_fence_warning();
    return false;
}

static gboolean
on_fence_point_removed(GtkWidget* widget, int pos)
{
    Gtk::TreeModel::iterator iter = planner->refgpListStore->children()[pos];
    planner->refgpListStore->erase(iter);

    iter = planner->refgpListStore->children().begin();
    Gtk::TreeModel::Row row = *iter;
    int counter = 0;
    while((iter != planner->refgpListStore->children().end())
            && (planner->refgpListStore->iter_is_valid(iter)))
    {
        row = *iter;
        row[planner->m_Columns.m_col_number] = counter;
        counter++;
        iter++;
    }
    planner->update_fence_warning();
    return false;
}


static gboolean
on_fence_changed(GtkWidget* widget)
{
    Gtk::TreeModel::Children rows = planner->refgpListStore->children();
    Gtk::TreeModel::iterator iter = rows.begin();
    OsmGpsMapTrack* trk = osm_gps_map_polygon_get_track(fence);
    GSList* waypoints = osm_gps_map_track_get_points(trk);
    while((waypoints) && (planner->refgpListStore->iter_is_valid(iter)))
    {
        OsmGpsMapPoint* point = (OsmGpsMapPoint*)waypoints->data;
        Gtk::TreeModel::Row row = *iter;
        row[planner->m_Columns.m_col_latitude] = to_degrees(point->rlat);
        row[planner->m_Columns.m_col_longitude] = to_degrees(point->rlon);
        iter++;
        waypoints = waypoints->next;
    }
    planner->update_fence_warning();
    return true;
}

static gboolean
on_track_point_added(GtkWidget* widget, gboolean* ptr)
{
    OsmGpsMapPoint* point = (OsmGpsMapPoint*)ptr;
    Gtk::TreeModel::Row row = *(planner->refwpListStore->append());
    row[planner->m_Columns.m_col_latitude] = to_degrees(point->rlat);
    row[planner->m_Columns.m_col_longitude] = to_degrees(point->rlon);
    row[planner->m_Columns.m_col_altitude] = 350;

    int nrows = planner->refwpListStore->children().size();
    if(nrows > 1)
    {
        Gtk::TreeModel::Row last_row = planner->refwpListStore->children()[nrows-2];
        float last_alt = last_row[planner->m_Columns.m_col_altitude];
        int last_n = last_row[planner->m_Columns.m_col_number];
        row[planner->m_Columns.m_col_altitude] = last_alt;
        row[planner->m_Columns.m_col_number] = last_n+1;
    }
    planner->update_path_distance();
    planner->update_fence_warning();
    return false;
}

static gboolean
on_track_point_inserted(GtkWidget* widget, int pos)
{
    Gtk::TreeModel::iterator iter = planner->refwpListStore->insert(planner->refwpListStore->children()[pos]);
    Gtk::TreeModel::Row row = *iter;
    OsmGpsMapPoint* point = osm_gps_map_track_get_point(track, pos);
    row[planner->m_Columns.m_col_latitude] = to_degrees(point->rlat);
    row[planner->m_Columns.m_col_longitude] = to_degrees(point->rlon);

    Gtk::TreeModel::Row prior_row = planner->refwpListStore->children()[pos-1];
    row[planner->m_Columns.m_col_altitude] = int(prior_row[planner->m_Columns.m_col_altitude]);

    iter--;
    row = *iter;
    int counter = row[planner->m_Columns.m_col_number];
    while((iter != planner->refwpListStore->children().end())
            && (planner->refwpListStore->iter_is_valid(iter)))
    {
        row = *iter;
        row[planner->m_Columns.m_col_number] = counter;
        counter++;
        iter++;
    }

    planner->update_path_distance();
    planner->update_fence_warning();
    return false;
}

static gboolean
on_track_point_removed(GtkWidget* widget, int pos)
{
    Gtk::TreeModel::iterator iter = planner->refwpListStore->children()[pos];
    planner->refwpListStore->erase(iter);

    iter = planner->refwpListStore->children().begin();
    Gtk::TreeModel::Row row = *iter;
    int counter = 0;
    while((iter != planner->refwpListStore->children().end())
            && (planner->refwpListStore->iter_is_valid(iter)))
    {
        row = *iter;
        row[planner->m_Columns.m_col_number] = counter;
        counter++;
        iter++;
    }
    planner->update_path_distance();
    planner->update_fence_warning();
    return false;
}


static gboolean
on_track_changed(GtkWidget* widget)
{
    Gtk::TreeModel::Children rows = planner->refwpListStore->children();
    Gtk::TreeModel::iterator iter = rows.begin();
    GSList* waypoints = osm_gps_map_track_get_points(track);
    while((waypoints) && (planner->refwpListStore->iter_is_valid(iter)))
    {
        OsmGpsMapPoint* point = (OsmGpsMapPoint*)waypoints->data;
        Gtk::TreeModel::Row row = *iter;
        row[planner->m_Columns.m_col_latitude] = to_degrees(point->rlat);
        row[planner->m_Columns.m_col_longitude] = to_degrees(point->rlon);
        iter++;
        waypoints = waypoints->next;
    }
    planner->update_path_distance();
    planner->update_fence_warning();
    return true;
}

static gboolean
on_map_button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    int left_button = (event->button == 1);
    if((left_button) && (event->type == GDK_2BUTTON_PRESS))
    {
        GSList* points = osm_gps_map_track_get_points(track);
        int ctr = 0;
        while(points)
        {
            int cx, cy;
            OsmGpsMapPoint* point = (OsmGpsMapPoint*)points->data;
            osm_gps_map_convert_geographic_to_screen(planner_map, point, &cx, &cy);

            float dist_sqrd = (event->x - cx) * (event->x-cx) + (event->y-cy) * (event->y-cy);
            if(dist_sqrd <= (5*5))
            {
                osm_gps_map_track_remove_point(track, ctr);
                osm_gps_map_map_redraw_idle(planner_map);
                return false;
            }

            points = points->next;
            ctr++;
        }

        points = osm_gps_map_track_get_points(osm_gps_map_polygon_get_track(fence));
        ctr = 0;
        while(points)
        {
            int cx, cy;
            OsmGpsMapPoint* point = (OsmGpsMapPoint*)points->data;
            osm_gps_map_convert_geographic_to_screen(planner_map, point, &cx, &cy);

            float dist_sqrd = (event->x - cx) * (event->x-cx) + (event->y-cy) * (event->y-cy);
            if(dist_sqrd <= (5*5))
            {
                osm_gps_map_track_remove_point(osm_gps_map_polygon_get_track(fence), ctr);
                osm_gps_map_map_redraw_idle(planner_map);
                return false;
            }

            points = points->next;
            ctr++;
        }

        click_down_x = event->x;
        click_down_y = event->y;
    }
    return false;
}


static gboolean
on_map_button_release_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    if((click_down_x == int(event->x)) && (click_down_y == int(event->y)))
    {

        OsmGpsMapPoint coord;
        float lat, lon;

        osm_gps_map_convert_screen_to_geographic(planner_map, event->x, event->y, &coord);
        osm_gps_map_point_get_degrees(&coord, &lat, &lon);
        osm_gps_map_track_add_point(track, &coord);
    }

    return false;
}

static void
clear_tracks()
{
    osm_gps_map_track_remove_all(planner_map);
    osm_gps_map_polygon_remove_all(planner_map);

    track = osm_gps_map_track_new();
    fence = osm_gps_map_polygon_new();

    osm_gps_map_track_add(OSM_GPS_MAP(planner_map), track);
    osm_gps_map_polygon_add(OSM_GPS_MAP(planner_map), fence);

    g_signal_connect (G_OBJECT (track), "changed",
                      G_CALLBACK (on_track_changed), NULL);
    g_signal_connect (G_OBJECT (track), "point-inserted",
                      G_CALLBACK (on_track_point_inserted), NULL);
    g_signal_connect (G_OBJECT (track), "point-removed",
                      G_CALLBACK (on_track_point_removed), NULL);
    g_signal_connect (G_OBJECT (track), "point-added",
                      G_CALLBACK (on_track_point_added), NULL);

    GdkRGBA col;
    col.red = 0.2;
    col.green = 0.2;
    col.blue = 1.0;
    osm_gps_map_track_set_color(track, &col);
    g_object_set((GObject*)track, "alpha", 0.85, NULL);
    g_object_set((GObject*)track, "editable", true, NULL);
    g_object_set((GObject*)fence, "editable", true, NULL);

    OsmGpsMapTrack* fence_track = osm_gps_map_polygon_get_track(fence);
    g_signal_connect (G_OBJECT (fence_track), "changed",
                      G_CALLBACK (on_fence_changed), NULL);
    g_signal_connect (G_OBJECT (fence_track), "point-inserted",
                      G_CALLBACK (on_fence_point_inserted), NULL);
    g_signal_connect (G_OBJECT (fence_track), "point-removed",
                      G_CALLBACK (on_fence_point_removed), NULL);
    g_signal_connect (G_OBJECT (fence_track), "point-added",
                      G_CALLBACK (on_fence_point_added), NULL);

    GdkRGBA black;
    black.red = 0.0;
    black.green = 0.0;
    black.blue = 0.0;
    osm_gps_map_track_set_color(fence_track, &black);

    float alpha = 0.8;
    g_object_set((GObject*)fence_track, "alpha", alpha, NULL);
    g_object_set((GObject*)fence, "shaded", FALSE, NULL);
}


static void
on_tiles_queued_changed (OsmGpsMap *image, GParamSpec *pspec, gpointer user_data)
{
    int tiles;
    g_object_get(image, "tiles-queued", &tiles, NULL);
    std::stringstream ss;
    ss << tiles << " tiles remaining";
    planner->tiles_label->set_text(ss.str());
}



Planner::Planner(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    planner = this;
    refBuilder = _refBuilder;
    thr_running = false;
    changed = false;

    upload_progress = 0;
    Gtk::Box* map_container;
    refBuilder->get_widget("map_container", map_container);

    std::string hdir = Glib::get_home_dir();
    hdir += "/.firetailgcs";

    planner_map = (OsmGpsMap*)g_object_new (OSM_TYPE_GPS_MAP,
                                            "map-source",OSM_GPS_MAP_SOURCE_GOOGLE_HYBRID,
                                            "tile-cache",g_strdup(OSM_GPS_MAP_CACHE_AUTO),
                                            "tile-cache-base", hdir.c_str(),
                                            "proxy-uri",g_getenv("http_proxy"),
                                            NULL);

    osd = (OsmGpsMapLayer*)g_object_new (OSM_TYPE_GPS_MAP_OSD,
                                         "show-scale",TRUE,
                                         "show-coordinates",TRUE,
                                         "show-crosshair",FALSE,
                                         "show-dpad",TRUE,
                                         "show-zoom",FALSE,
                                         "show-gps-in-dpad",TRUE,
                                         "show-gps-in-zoom",FALSE,
                                         "dpad-radius", 30,
                                         NULL);
    osm_gps_map_layer_add(OSM_GPS_MAP(planner_map), osd);
    g_object_unref(G_OBJECT(osd));

    gtk_box_pack_end(
        GTK_BOX(map_container->gobj()),
        GTK_WIDGET(planner_map), TRUE, TRUE, 0);

    gtk_widget_set_vexpand(GTK_WIDGET(planner_map), true);
    gtk_widget_set_hexpand(GTK_WIDGET(planner_map), true);
    gtk_widget_set_hexpand_set(GTK_WIDGET(planner_map), true);
    gtk_widget_set_vexpand_set(GTK_WIDGET(planner_map), true);

    gtk_widget_show(GTK_WIDGET(planner_map));

    g_signal_connect (G_OBJECT (planner_map), "button-press-event",
                      G_CALLBACK (on_map_button_press_event), NULL);
    g_signal_connect (G_OBJECT (planner_map), "button-release-event",
                      G_CALLBACK (on_map_button_release_event),
                      NULL);

    clear_tracks();

    std::string cursor_path = share_uri;
    cursor_path += "marker.png";
    gdk_cursor_image = gdk_pixbuf_new_from_file_at_size(cursor_path.c_str(), 64, 64, NULL);


    refBuilder->get_widget("waypoint_list_treeview", waypoint_list);
    refwpListStore = Gtk::ListStore::create(m_Columns);
    waypoint_list->set_model(refwpListStore);

    waypoint_list->append_column("ID", m_Columns.m_col_number);
    waypoint_list->append_column("Latitude", m_Columns.m_col_latitude);
    waypoint_list->append_column("Longitude", m_Columns.m_col_longitude);
    waypoint_list->append_column_editable("Altitude", m_Columns.m_col_altitude);

    waypoint_list->get_column(0)->set_min_width(100);
    waypoint_list->get_column(1)->set_min_width(200);
    waypoint_list->get_column(2)->set_min_width(200);
    waypoint_list->get_column(3)->set_min_width(200);

    waypoint_selection = waypoint_list->get_selection();
    waypoint_selection->signal_changed().connect(sigc::mem_fun(*this, &Planner::on_waypoint_selection_changed));

    refBuilder->get_widget("geopoint_list_treeview", geopoint_list);
    refgpListStore = Gtk::ListStore::create(m_Columns);
    geopoint_list->set_model(refgpListStore);

    geopoint_list->append_column("ID", m_Columns.m_col_number);
    geopoint_list->append_column("Latitude", m_Columns.m_col_latitude);
    geopoint_list->append_column("Longitude", m_Columns.m_col_longitude);

    geopoint_list->get_column(0)->set_min_width(100);
    geopoint_list->get_column(1)->set_min_width(200);
    geopoint_list->get_column(2)->set_min_width(200);

    refBuilder->get_widget("geofence_warning", fence_warning);

    refBuilder->get_widget("path_distance_label", path_distance_lbl);
    refBuilder->get_widget("time_estimate_label", time_estimate_lbl);
    refBuilder->get_widget("speed_spinbutton", speed_spin);

    speed_spin->set_range(1.0, 1000.0);
    speed_spin->set_digits(0);
    speed_spin->set_increments(1, 5);
    speed_spin->set_value(100.0);

    speed_spin->signal_value_changed().connect(sigc::mem_fun(*this, &Planner::speed_spin_changed));

    refBuilder->get_widget("planner_upload_button", upload_button);
    upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_upload_plan));

    refBuilder->get_widget("progress_lbl", progress_lbl);
    refBuilder->get_widget("sync_progress_bar", sync_progress);

    refBuilder->get_widget("planner_file_new", file_new);
    refBuilder->get_widget("planner_file_open", file_open);
    refBuilder->get_widget("planner_file_save", file_save);
    refBuilder->get_widget("planner_file_saveas", file_saveas);

    file_new->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_new));
    file_save->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_save));
    file_saveas->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_save_as));
    file_open->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_open));

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    refBuilder->get_widget("map_zoom_spin", map_zoom_spin);
    map_zoom_spin->set_digits(0);
    map_zoom_spin->set_range(2, 20);
    map_zoom_spin->set_increments(1, 1);
    map_zoom_spin->signal_changed().connect(sigc::mem_fun(*this, &Planner::on_zoom_changed));

    refBuilder->get_widget("map_type_combo", map_type_combo);
    map_type_combo->set_active(0);
    map_type_combo->signal_changed().connect(sigc::mem_fun(*this, &Planner::on_map_type_combo_changed));

    refBuilder->get_widget("create_geofence_button", create_geofence_button);
    create_geofence_button->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_geofence_create));

    refBuilder->get_widget("delete_geofence_button", delete_geofence_button);
    delete_geofence_button->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_geofence_delete));

    refBuilder->get_widget("planner_cache_button", cache_button);
    cache_button->signal_clicked().connect(sigc::mem_fun(*this, &Planner::on_cache_clicked));

    refBuilder->get_widget("tiles_queued_lbl", tiles_label);
    g_signal_connect (G_OBJECT(planner_map), "notify::tiles-queued", G_CALLBACK (on_tiles_queued_changed), NULL);

    g_signal_connect(G_OBJECT(planner_map), "changed",
                     G_CALLBACK(on_map_changed), NULL);

    Gtk::Window::maximize();
    first_show = true;
}


Planner::~Planner()
{

}

void Planner::update_fence_warning()
{
    //first check to see if all waypoints are within the fence

    //create a vector full of geopoints
    std::vector<imu::Vector<2>> points;
    Gtk::TreeModel::Children rows = refgpListStore->children();
    if(rows.size() < 1)
    {
        fence_warning->set_text("");
        return;
    }

    Gtk::TreeModel::iterator iter = rows.begin();

    while(refgpListStore->iter_is_valid(iter) && (iter != rows.end()))
    {
        Gtk::TreeModel::Row row = *iter;
        imu::Vector<2> d;
        d.y() = row[m_Columns.m_col_latitude];
        d.x() = row[m_Columns.m_col_longitude];
        points.push_back(d);
        iter++;
    }

    //iterate over the waypoints and check that they are within the geofence
    //using a ray casting algorithm
    rows = refwpListStore->children();
    iter = rows.begin();
    int counter = 0;
    while(refwpListStore->iter_is_valid(iter) && (iter != rows.end()))
    {
        counter++;
        bool oddNodes = false;
        Gtk::TreeModel::Row row = *iter;
        imu::Vector<2> here;
        here.y() = row[m_Columns.m_col_latitude];
        here.x() = row[m_Columns.m_col_longitude];

        for(unsigned int i = 0; i < points.size()-1; i++)
        {
            if(((points[i].y() > here.y()) != (points[i+1].y() > here.y())) &&
                    (here.x() < (points[i+1].x() - points[i].x()) * (here.y() - points[i].y()) / (points[i+1].y() - points[i].y()) + points[i].x()))
            {
                oddNodes = !oddNodes;
            }
        }

        int i = points.size()-1;
        if(((points[i].y() > here.y()) != (points[0].y() > here.y())) &&
                (here.x() < (points[0].x() - points[i].x()) * (here.y() - points[i].y()) / (points[0].y() - points[i].y()) + points[i].x()))
        {
            oddNodes = !oddNodes;
        }

        if(!oddNodes)
        {
            std::stringstream ss;
            ss << "Waypoint " << (counter-1) << " is outside the fence.";
            fence_warning->set_label(ss.str());
            return;
        }

        iter++;
    }

    //now check to see if any sectors intersect the boundary
    //only worth doing if there are two waypoints or more
    if(rows.size() > 1)
    {
        iter = rows.begin();
        iter++;
        counter = 0;
        while(iter != rows.end())
        {
            counter++;
            iter--;
            Gtk::TreeModel::Row prev_row = *iter;
            iter++;
            Gtk::TreeModel::Row row = *iter;
            iter++;

            imu::Vector<2> wp1, wp2;
            wp1.y() = prev_row[m_Columns.m_col_latitude];
            wp1.x() = prev_row[m_Columns.m_col_longitude];

            wp2.y() = row[m_Columns.m_col_latitude];
            wp2.x() = row[m_Columns.m_col_longitude];

            for(unsigned int i = 1; i < points.size(); i++)
            {
                imu::Vector<2> gp1 = points[i-1];
                imu::Vector<2> gp2 = points[i];

                if((CCW(wp1, gp1, gp2) != CCW(wp2, gp1, gp2)) && (CCW(wp1, wp2, gp1) != CCW(wp1, wp2, gp2)))
                {
                    std::stringstream ss;
                    ss << "Path between waypoints " << counter-1 << " and " << counter << " crosses the geofence!";
                    fence_warning->set_label(ss.str());
                    return;
                }
            }
        }
    }
    fence_warning->set_label("");
}

void Planner::show()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    if(coms->connection_strength() > 3) //if link quality is more than 3
    {
        //download location and center the map there
        float lat, lng;
        lat = coms->get_latitude();
        lng = coms->get_longitude();

        osm_gps_map_set_center_and_zoom(planner_map, lat, lng, 14);
    }

    Gtk::Window::show();
    Gtk::Window::maximize();
}

//needed to recalculate estimated time
void Planner::speed_spin_changed()
{
    update_path_distance();
}

//moves the selection marker on the map
void Planner::on_waypoint_selection_changed()
{
    Gtk::TreeModel::iterator iter = waypoint_selection->get_selected();
    if(iter)
    {
        Gtk::TreeModel::Row row = *iter;

        osm_gps_map_image_remove(planner_map, cursor);
        cursor = osm_gps_map_image_add(planner_map, 0, 0, gdk_cursor_image);
        OsmGpsMapPoint* cursor_point = osm_gps_map_point_new_degrees(row[m_Columns.m_col_latitude], row[m_Columns.m_col_longitude]);
        g_object_set((GObject*)cursor, "point", (GValue*)cursor_point, NULL);
    }
    else
        osm_gps_map_image_remove(planner_map, cursor);

    changed = true;
}

void Planner::on_upload_plan()
{
    if(thr_running)
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    int pid = coms->set_clear_points(Firelink::POINT_TYPE_WAYPOINT);
    coms->unlock();

    if(!coms->wait_for_dispatch(pid))
    {
        Gtk::MessageDialog msg(*this, "Failed to clear waypoints.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("Is the datalink OK? The vehicle didn't acknowledge the 'clear waypoints' message.");
        msg.run();
        return;
    }

    coms->lock();
    pid = coms->set_clear_points(Firelink::POINT_TYPE_GEOPOINT);
    coms->unlock();
    if(!coms->wait_for_dispatch(pid))
    {
        Gtk::MessageDialog msg(*this, "Failed to clear waypoints.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("Is the datalink OK? The vehicle didn't acknowledge the 'clear geopoints' message.");
        msg.run();
        return;
    }

    points.clear();
    geopoints.clear();

    Gtk::TreeModel::Children rows = refwpListStore->children();
    Gtk::TreeModel::iterator iter = rows.begin();

    while(iter != rows.end())
    {
        Gtk::TreeModel::Row row = *iter;
        PlanFile::wp p;
        p.lat = row[m_Columns.m_col_latitude];
        p.lng = row[m_Columns.m_col_longitude];
        p.alt = row[m_Columns.m_col_altitude];
        points.push_back(p);
        iter++;
    }

    rows = refgpListStore->children();
    iter = rows.begin();
    while(iter != rows.end())
    {
        Gtk::TreeModel::Row row = *iter;
        PlanFile::wp p;
        p.lat = row[m_Columns.m_col_latitude];
        p.lng = row[m_Columns.m_col_longitude];
        p.alt = row[m_Columns.m_col_altitude];
        geopoints.push_back(p);
        iter++;
    }

    try
    {
        if(thr.joinable())
            thr.join();
        thr = std::thread(&Planner::threadfunc, this);
    }
    catch(...)
    {
        cout << "Exception starting thread!" << endl;
    }
}

void Planner::upload_thread()
{

    thr_running = true;

    shared_ptr<Coms> coms = Coms::get_active_instance();

    mute.lock();
    progress_msg = "Uploading...";
    upload_progress = 0;
    mute.unlock();

    float sz = points.size() + geopoints.size();
    float incr = 1/sz;

    for(unsigned int i = 0; i < points.size(); i++)
    {
        coms->lock();
        uint16_t pid = coms->set_point(Firelink::POINT_TYPE_WAYPOINT, i, points[i].lat*1000000,
                                       points[i].lng*1000000, points[i].alt);
        coms->unlock();
        if(!coms->wait_for_dispatch(pid))
        {
            mute.lock();
            progress_msg = "Error uploading waypoints. Check datalink.";
            upload_progress = 0;
            mute.unlock();
            thr_running = false;
            return;
        }
        mute.lock();
        upload_progress += incr/2;
        mute.unlock();
    }

    for(unsigned int i = 0; i < geopoints.size(); i++)
    {
        coms->lock();
        uint16_t pid = coms->set_point(Firelink::POINT_TYPE_GEOPOINT, i, geopoints[i].lat*1000000,
                                       geopoints[i].lng*1000000, geopoints[i].alt);

        coms->unlock();
        if(!coms->wait_for_dispatch(pid))
        {
            mute.lock();
            progress_msg = "Error uploading geofence points. Check datalink.";
            upload_progress = 0;
            mute.unlock();
            thr_running = false;
            return;
        }
        mute.lock();
        upload_progress += incr/2;
        mute.unlock();
    }

    mute.lock();
    progress_msg = "Verifying...";
    mute.unlock();


    coms->lock();
    coms->plan_points.clear();
    coms->fence_points.clear();
    uint16_t pid = coms->get_n_points();
    coms->unlock();

    if(!coms->wait_for_dispatch(pid))
    {
        mute.lock();
        progress_msg = "Error uploading waypoints. \nCheck datalink.";
        upload_progress = 0;
        mute.unlock();
        thr_running = false;
        return;
    }

    if(!coms->wait_for_message(Firelink::VM_N_POINTS))
    {
        mute.lock();
        progress_msg = "Error uploading waypoints. \nVehicle didn't report number of waypoints.";
        upload_progress = 0;
        mute.unlock();
        thr_running = false;
        return;
    }

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_N_POINTS);
    coms->unlock();

    Firelink::m_n_points* pnw = (Firelink::m_n_points*)pack.data;
    if(pnw->wp != points.size())
    {
        stringstream ss;
        ss << "Error uploading waypoints. \nVehicle reports only " << pnw->wp << " waypoints uploaded.";
        mute.lock();
        progress_msg = ss.str();
        upload_progress = 0;
        mute.unlock();
        thr_running = false;
        return;
    }

    if(pnw->gp != geopoints.size())
    {
        stringstream ss;
        ss << "Error uploading geopoints. \nVehicle reports " << pnw->gp << " geopoints uploaded.";
        mute.lock();
        progress_msg = ss.str();
        upload_progress = 0;
        mute.unlock();
        thr_running = false;
        return;
    }


    coms->lock();
    coms->reset_last_points();
    coms->get_all_points();
    coms->unlock();

    sleep_ms(500);

    int n_points_checked = 0;
    while(n_points_checked < points.size())
    {
        int i = n_points_checked;
        coms->lock();
        int n_plan_points_received = coms->last_plan_point_recvd();
        coms->unlock();

        if(n_plan_points_received >= i)
        {
            coms->lock();
            waypoint wp = coms->plan_points[i];
            coms->unlock();

            if(fabs((float)wp.pos.lat - (float)(points[i].lat)) > 25)
            {
                mute.lock();
                progress_msg = "Error verifying waypoints. \nBad latitude found.";
                upload_progress = 0;
                mute.unlock();
                thr_running = false;
                return;
            }
            if(fabs(wp.pos.lng - int(points[i].lng)) > 25)
            {
                mute.lock();
                progress_msg = "Error verifying waypoints.\nBad longitude.";
                upload_progress = 0;
                mute.unlock();
                thr_running = false;
                return;
            }
            if(abs(wp.alt - int(points[i].alt)) > 5)
            {
                mute.lock();
                progress_msg = "Error verifying waypoints. \nBad altitude.";
                upload_progress = 0;
                mute.unlock();
                thr_running = false;
                return;
            }
            n_points_checked++;
            mute.lock();
            upload_progress += incr/2;
            mute.unlock();
        }
    }


    n_points_checked = 0;
    while(n_points_checked < geopoints.size())
    {
        int i = n_points_checked;
        coms->lock();
        int n_fence_points_received = coms->last_fence_point_recvd();
        coms->unlock();

        if(n_fence_points_received >= i)
        {
            coms->lock();
            waypoint wp = coms->fence_points[i];
            coms->unlock();

            if(fabs((float)wp.pos.lat - (float)(geopoints[i].lat)) > 25)
            {
                mute.lock();
                progress_msg = "Error verifying fence points. \nBad latitude found.";
                upload_progress = 0;
                mute.unlock();
                thr_running = false;
                return;
            }
            if(fabs(wp.pos.lng - int(geopoints[i].lng)) > 25)
            {
                mute.lock();
                progress_msg = "Error verifying fence points.\nBad longitude.";
                upload_progress = 0;
                mute.unlock();
                thr_running = false;
                return;
            }

            n_points_checked++;
            mute.lock();
            upload_progress += incr/2;
            mute.unlock();
        }
    }

    mute.lock();
    progress_msg = "Waypoints uploaded!";
    upload_progress = 100;
    mute.unlock();
    thr_running = false;
    return;
}

void Planner::threadfunc()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_skip_queue(true);
    coms->unlock();

    upload_thread();

    coms->lock();
    coms->set_skip_queue(false);
    coms->unlock();
}

void Planner::timeout()
{
    mute.lock();
    sync_progress->set_fraction(upload_progress);
    progress_lbl->set_text(progress_msg);
    mute.unlock();
}

void Planner::on_new()
{
    if(changed)
    {
        Gtk::MessageDialog msg("Changes to this plan will be lost.", false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
        msg.set_secondary_text("You have made changes to this plan. Are you sure you want to start a new plan? ");
        if(msg.run() == Gtk::RESPONSE_NO)
            return;
    }
    file_name = "";

    refwpListStore->clear();
    refgpListStore->clear();
    speed_spin_changed(); //to update time estimate

    clear_tracks();
    changed = false;
}

void Planner::on_save()
{
    if(file_name == "")
        on_save_as();

    PlanFile pf;

    pf.map_type = map_type_combo->get_active_row_number();
    pf.zoom_level = map_zoom_spin->get_value_as_int();

    OsmGpsMapPoint* center = osm_gps_map_get_center(planner_map);
    pf.origin_lat = to_degrees(center->rlat);
    pf.origin_lng = to_degrees(center->rlon);
    osm_gps_map_point_free(center);

    Gtk::TreeModel::Children rows = refwpListStore->children();
    Gtk::TreeModel::iterator iter = rows.begin();

    while(iter != rows.end())
    {
        Gtk::TreeModel::Row row = *iter;
        PlanFile::wp w;
        w.lat = row[m_Columns.m_col_latitude];
        w.lng = row[m_Columns.m_col_longitude];
        w.alt = row[m_Columns.m_col_altitude];
        pf.waypoints.push_back(w);
        iter++;
    }

    rows = refgpListStore->children();
    iter = rows.begin();

    while(iter != rows.end())
    {
        Gtk::TreeModel::Row row = *iter;
        PlanFile::wp w;
        w.lat = row[m_Columns.m_col_latitude];
        w.lng = row[m_Columns.m_col_longitude];
        w.alt = row[m_Columns.m_col_altitude];
        pf.geopoints.push_back(w);
        iter++;
    }

    pf.save(file_name);
    changed = false;
}

void Planner::on_save_as()
{
    Gtk::FileChooserDialog fcd(*this, "Save plan as", Gtk::FILE_CHOOSER_ACTION_SAVE, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    file_name = fcd.get_filename();
    std::string extension(file_name, file_name.length()-3, 3);
    if(extension != "xml")
        file_name += ".xml";

    on_save();
}

void Planner::on_open()
{
    if(changed)
    {
        Gtk::MessageDialog msg("Changes to this plan will be lost.", false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
        msg.set_secondary_text("You have made changes to this plan. You may wish to save them. Are you sure you want to close this plan now?");
        if(msg.run() == Gtk::RESPONSE_NO)
            return;
    }

    Gtk::FileChooserDialog fcd(*this, "Open plan", Gtk::FILE_CHOOSER_ACTION_OPEN, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    PlanFile pf;
    if(!pf.load(fcd.get_filename(), this))
        return;

    file_name = fcd.get_filename();

    refwpListStore->clear();
    refgpListStore->clear();
    clear_tracks();

    map_zoom_spin->set_value(pf.zoom_level);
    map_type_combo->set_active(pf.map_type);
    osm_gps_map_set_center(planner_map, pf.origin_lat, pf.origin_lng);

    for(unsigned int i = 0; i < pf.waypoints.size(); i++)
    {
        OsmGpsMapPoint* pt = osm_gps_map_point_new_degrees(pf.waypoints[i].lat, pf.waypoints[i].lng);
        osm_gps_map_track_add_point(track, pt);

        Gtk::TreeModel::Children rows = refwpListStore->children();
        Gtk::TreeModel::iterator iter = rows.end();
        iter--;
        if(refwpListStore->iter_is_valid(iter))
        {
            Gtk::TreeModel::Row row = *iter;
            row[m_Columns.m_col_altitude] = pf.waypoints[i].alt;
        }
    }

    for(unsigned int i = 0; i < pf.geopoints.size(); i++)
    {
        OsmGpsMapTrack* gtrack = osm_gps_map_polygon_get_track(fence);
        OsmGpsMapPoint* pt = osm_gps_map_point_new_degrees(pf.geopoints[i].lat, pf.geopoints[i].lng);
        osm_gps_map_track_add_point(gtrack, pt);
    }

    //on success
    changed = false;
}

void Planner::on_geofence_create()
{

    int npoints = osm_gps_map_track_n_points(
                      osm_gps_map_polygon_get_track(fence));

    if(npoints > 0)
        return;
    OsmGpsMapPoint *top_left, *bottom_right, *bottom_left, *top_right;

    top_left = osm_gps_map_point_new_degrees(0, 0);
    bottom_right = osm_gps_map_point_new_degrees(0, 0);

    osm_gps_map_get_bbox(planner_map, top_left, bottom_right);
    bottom_left = osm_gps_map_point_new_radians(bottom_right->rlat, top_left->rlon);
    top_right = osm_gps_map_point_new_radians(top_left->rlat, bottom_right->rlon);

    OsmGpsMapTrack* fence_track = osm_gps_map_polygon_get_track(fence);
    osm_gps_map_track_add_point(fence_track, top_left);
    osm_gps_map_track_add_point(fence_track, top_right);
    osm_gps_map_track_add_point(fence_track, bottom_right);
    osm_gps_map_track_add_point(fence_track, bottom_left);
}

void Planner::on_geofence_delete()
{
    osm_gps_map_polygon_remove_all(planner_map);
    fence = osm_gps_map_polygon_new();
    osm_gps_map_polygon_add(OSM_GPS_MAP(planner_map), fence);
    g_object_set((GObject*)fence, "editable", true, NULL);

    OsmGpsMapTrack* fence_track = osm_gps_map_polygon_get_track(fence);
    g_signal_connect (G_OBJECT (fence_track), "changed",
                      G_CALLBACK (on_fence_changed), NULL);
    g_signal_connect (G_OBJECT (fence_track), "point-inserted",
                      G_CALLBACK (on_fence_point_inserted), NULL);
    g_signal_connect (G_OBJECT (fence_track), "point-removed",
                      G_CALLBACK (on_fence_point_removed), NULL);
    g_signal_connect (G_OBJECT (fence_track), "point-added",
                      G_CALLBACK (on_fence_point_added), NULL);

    GdkRGBA black;
    black.red = 0.0;
    black.green = 0.0;
    black.blue = 0.0;
    osm_gps_map_track_set_color(fence_track, &black);

    float alpha = 0.8;
    g_object_set((GObject*)fence_track, "alpha", alpha, NULL);
    g_object_set((GObject*)fence, "shaded", FALSE, NULL);
    refgpListStore->clear();
    update_fence_warning();
}

void Planner::on_zoom_changed()
{
    int zoom = map_zoom_spin->get_value_as_int();
    osm_gps_map_set_zoom(planner_map, zoom);
}

void Planner::on_map_type_combo_changed()
{
    int mt = map_type_combo->get_active_row_number();
    switch(mt)
    {
    case 0:
    {
        g_object_set(planner_map, "map-source", OSM_GPS_MAP_SOURCE_GOOGLE_HYBRID, NULL);
    }
    break;
    case 1:
    {
        g_object_set(planner_map, "map-source", OSM_GPS_MAP_SOURCE_OPENSTREETMAP, NULL);
    }
    break;
    case 2:
    {
        g_object_set(planner_map, "map-source", OSM_GPS_MAP_SOURCE_VIRTUAL_EARTH_SATELLITE, NULL);
    }
    break;
    case 3:
    {
        g_object_set(planner_map, "map-source", OSM_GPS_MAP_SOURCE_MAPS_FOR_FREE, NULL);
    }
    break;
    }
}

void Planner::update_path_distance()
{
    double dist = osm_gps_map_track_get_length(track);
    std::stringstream ss;
    ss << "Distance: " << dist;
    path_distance_lbl->set_text(ss.str());

    float speed = speed_spin->get_value();
    speed *= 0.277777778;
    ss.str("");

    int input_seconds = dist/speed;
    float days = input_seconds / 60 / 60 / 24;
    float hours = (input_seconds / 60 / 60) % 24;
    float minutes = (input_seconds / 60) % 60;
    float seconds = input_seconds % 60;
    ss << "Estimated time: " << days << "d " << hours << "h " << minutes << "m " << seconds << "s";
    time_estimate_lbl->set_text(ss.str());
}

void Planner::on_cache_clicked()
{
    int zoom,max_zoom;
    OsmGpsMapPoint pt1, pt2;
    osm_gps_map_get_bbox(planner_map, &pt1, &pt2);
    g_object_get(planner_map, "zoom", &zoom, "max-zoom", &max_zoom, NULL);
    osm_gps_map_download_maps(planner_map, &pt1, &pt2, zoom, max_zoom);
}
