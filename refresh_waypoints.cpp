/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "refresh_waypoints.h"
#include "utility.h"

#include <iostream>
using namespace std;

RefreshWaypointsWindow::RefreshWaypointsWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("refresh_wp_lbl", lbl);
    lbl->set_text("");
    refBuilder->get_widget("refresh_wp_progress", prog);
    refBuilder->get_widget("cancel_wp_btn", cancel_btn);

    cancel_btn->signal_clicked().connect(sigc::mem_fun(*this, &RefreshWaypointsWindow::on_cancel_clicked));

    status = 0;
}

RefreshWaypointsWindow::~RefreshWaypointsWindow()
{

}

void RefreshWaypointsWindow::show()
{
        if(thr.joinable())
        {
            stop_thread = true;
            thr.join();
        }

        thr = std::thread(&RefreshWaypointsWindow::thr_func, this);
        sleep_ms(100);
        Gtk::Window::show();
}

void RefreshWaypointsWindow::timeout()
{
    if(!this->is_visible())
        return;

    switch(status)
    {
    case 0:
    {
        prog->set_fraction(1.0);
        lbl->set_text("Complete!");
    }
    break;
    case 1:
    {
        prog->set_fraction(0);
        lbl->set_text("Getting number of waypoints...");
    }
    break;
    case -1:
    {
        lbl->set_text("Error downloading waypoints.");
    }
    break;
    default:
    {
        prog->set_fraction(pc/100.0);
        std::stringstream ss;
        ss << "Getting waypoint " << status-1 << " of " << n_points;
        lbl->set_text(ss.str());
    }
    }
}

bool RefreshWaypointsWindow::on_delete_event(GdkEventAny* e)
{
    on_cancel_clicked();
    return true;
}

void RefreshWaypointsWindow::on_cancel_clicked()
{
    stop_thread = true;
    hide();
}


void RefreshWaypointsWindow::thr_func()
{
    stop_thread = false;
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    pc = 0;
    status = 1;

    while(!stop_thread)
    {
        coms->lock();
        coms->clear_in_queue();
        int pid = coms->get_n_points();
        coms->unlock();

        if(coms->wait_for_dispatch(pid))
        {
            if(coms->wait_for_message(Firelink::VM_N_POINTS))
            {
                coms->lock();
                Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_N_POINTS);
                coms->unlock();

                Firelink::m_n_points* np = (Firelink::m_n_points*)pack.data;
                n_points = np->wp;
                break;
            }
            else
                cout << "no n points received" << endl;
        }
        else
            cout << "get n points not acked" << endl;

    }
    if(stop_thread)
    {
        status = 0;
        return;
    }

    coms->lock();
    coms->get_all_points();
    coms->unlock();
    /*
    for(unsigned int i = 0; i < n_points; i++)
    {
        coms->lock();
        Firelink::m_point mp;
        mp.n = i;
        mp.type = Firelink::POINT_TYPE_WAYPOINT;
        coms->clear_in_queue();
        int pid = coms->get_point(&mp, sizeof(mp));
        coms->unlock();

        if(coms->wait_for_dispatch(pid))
        {
            if(coms->wait_for_message(Firelink::VM_POINT))
            {
                Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_POINT);
                Firelink::m_point* mp = (Firelink::m_point*)pack.data;

                if(mp->n == i)
                {
                    status++;
                    pc += (100.0/float(n_points));
                    continue;
                }
            }
        }

        if(stop_thread)
        {
            status = 0;
            return;
        }

        i--;

        /*
        status++;

        int tmpctr = 100;
        while(true)
        {
            tmpctr++;
            if(tmpctr > 50)
            {
                tmpctr = 0;
                coms->lock();
                Firelink::m_point mp;
                mp.n = i;
                mp.type = Firelink::POINT_TYPE_WAYPOINT;
                coms->clear_in_queue();
                cout << "getting point " << mp.n << endl;
                coms->get_point(&mp);
                coms->unlock();
            }
            if(coms->on_queue(Firelink::SM_GET_POINT))
                cout << "pack is on queue" << endl;

            sleep_ms(20);
            bool oq;
            coms->lock();
            oq = coms->on_in_queue(Firelink::VM_POINT);
            if(oq)
            {
                cout << "got reply" << endl;
                Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_POINT);
                Firelink::m_point* wp = (Firelink::m_point*)pack.data;
                cout << "reply was for point " << wp->n << endl;
                if(wp->n == i)
                {
                    coms->unlock();
                    break;
                }
            }
            coms->unlock();

            if(stop_thread)
                break;
        }



    }
    */
    status = 0;
    pc = 100.0;
}
