/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "logwindow.h"
#include <chrono>

LogWindow::LogWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    thr_running = false;
    entry_pos = 0;
    refBuilder = _refBuilder;

    refBuilder->get_widget("log_file_label", file_label);
    refBuilder->get_widget("log_open_button", open_button);
    open_button->signal_clicked().connect(sigc::mem_fun(*this, &LogWindow::open_log));

    refBuilder->get_widget("log_export_button", export_button);
    export_button->signal_clicked().connect(sigc::mem_fun(*this, &LogWindow::export_csv));

    refBuilder->get_widget("log_pause_toggle", pause_toggle);
    pause_toggle->signal_toggled().connect(sigc::mem_fun(*this, &LogWindow::pause_toggled));

    refBuilder->get_widget("log_play_toggle", play_toggle);
    play_toggle->signal_toggled().connect(sigc::mem_fun(*this, &LogWindow::play_toggled));

    refBuilder->get_widget("log_ffwd_toggle", ffwd_toggle);
    ffwd_toggle->signal_toggled().connect(sigc::mem_fun(*this, &LogWindow::ffwd_toggled));

    refBuilder->get_widget("log_rwd_toggle", rewind_toggle);
    rewind_toggle->signal_toggled().connect(sigc::mem_fun(*this, &LogWindow::rwd_toggled));

    refBuilder->get_widget("log_scale", scale);
    Glib::RefPtr<Gtk::Adjustment> adj = scale->get_adjustment();
    adj->set_lower(0);
    adj->set_upper(0);

    scale->signal_value_changed().connect(sigc::mem_fun(*this, &LogWindow::on_slider_value_changed));

    refBuilder->get_widget("log_time_now", current_time);
    refBuilder->get_widget("log_time_total", total_time);

    this->signal_delete_event().connect(sigc::mem_fun(*this, &LogWindow::on_delete_event));

    std::string iconpath = share_uri;
    iconpath += "firetail.png";
    set_icon_from_file(iconpath);
}

LogWindow::~LogWindow()
{

}

void LogWindow::show()
{
    mute.lock();
    pack_vec.clear();
    entry_pos = 0;
    mute.unlock();
    file_label->set_text("No log open.");
    Gtk::Window::show();
}

void LogWindow::timeout()
{
    Glib::RefPtr<Gtk::Adjustment> adj = scale->get_adjustment();
    int upper = adj->get_upper();
    mute.lock();
    if(upper != pack_vec.size())
        adj->set_upper(pack_vec.size()+0.1);

    ignore_slider_changed = true;
    adj->set_value(entry_pos);
    ignore_slider_changed = false;

    int input_seconds = pack_vec.size()/10;
    int minutes = (input_seconds / 60);
    int seconds = input_seconds % 60;

    stringstream ss;
    ss << std::setfill('0') << std::setw(2) << minutes << ":"
        << std::setfill('0') << std::setw(2) << seconds;

    total_time->set_text(ss.str());

    ss.clear();
    ss.str("");

    input_seconds = entry_pos/10;
    minutes = input_seconds / 60;
    seconds = input_seconds % 60;
    ss << std::setfill('0') << std::setw(2) << minutes << ":"
        << std::setfill('0') << std::setw(2) << seconds;
    current_time->set_text(ss.str());

    mute.unlock();
}

void LogWindow::pause_toggled()
{
    if(pause_toggle->get_active())
    {
        play_toggle->set_active(false);
        ffwd_toggle->set_active(false);
        rewind_toggle->set_active(false);
        mute.lock();
        PlayMode = PLAY_MODE_PAUSE;
        mute.unlock();
    }
}

void LogWindow::play_toggled()
{
    if(play_toggle->get_active())
    {
        pause_toggle->set_active(false);
        ffwd_toggle->set_active(false);
        rewind_toggle->set_active(false);
        mute.lock();
        PlayMode = PLAY_MODE_PLAY;
        mute.unlock();
    }
}

void LogWindow::ffwd_toggled()
{
    if(ffwd_toggle->get_active())
    {
        pause_toggle->set_active(false);
        play_toggle->set_active(false);
        rewind_toggle->set_active(false);
        mute.lock();
        PlayMode = PLAY_MODE_FFWD;
        mute.unlock();
    }
}

void LogWindow::rwd_toggled()
{
    if(rewind_toggle->get_active())
    {
        pause_toggle->set_active(false);
        ffwd_toggle->set_active(false);
        play_toggle->set_active(false);
        mute.lock();
        PlayMode = PLAY_MODE_REWIND;
        mute.unlock();
    }
}

bool LogWindow::on_delete_event(GdkEventAny* event)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    coms->stop();
    mute.lock();
    thr_run = false;
    mute.unlock();
    return false;
}

void LogWindow::open_log()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    if(coms->running())
    {
        Gtk::MessageDialog msg(*this, "Can't open log while connected!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("Please disconnect before trying to open a log. ");
        msg.run();
        return;
    }

    Gtk::FileChooserDialog fcd(*this, "Open log file", Gtk::FILE_CHOOSER_ACTION_OPEN, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    std::string fname = Glib::get_home_dir();
    fname += "/.firetailgcs/logs";
    fcd.set_current_folder(fname);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::fstream inf;
    inf.open(fcd.get_filename());
    if(!inf.is_open())
    {
        Gtk::MessageDialog msg(*this, "Error opening file!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be opened.");
        msg.run();
        return;
    }

    std::stringstream ss;
    ss << inf.rdbuf();
    std::string contents = ss.str();
    inf.close();

    tinyxml2::XMLDocument doc_front;
    int parseret = doc_front.Parse(contents.c_str());
    if(parseret != tinyxml2::XML_NO_ERROR)
    {
        Gtk::MessageDialog msg(*this, "Failed to open file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("There was an error parsing the log file. It does not appear to contain valid XML.");
        msg.run();
        return;
    }

    tinyxml2::XMLElement* header = doc_front.FirstChildElement("FlightLog");
    if(!header)
    {
        Gtk::MessageDialog msg(*this, "Failed to open file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file does not contain a FlightLog element. It probably isn't a FiretailGCS log file. ");
        msg.run();
        return;
    }

    PlayMode = PLAY_MODE_PAUSE;

    //kill the old thread if it is running
    if(thr_running)
    {
        thr_run = false;
        thr.join();
    }

    //swap in the new XML document
    mute.lock();

    pack_vec.clear();
    tinyxml2::XMLElement* entryElement = header->FirstChildElement("Entry");

    int entry_count = 0;
    while(entryElement)
    {
        std::vector<std::string> packs;
        int pack_count = 0;
        tinyxml2::XMLElement* pktElement = entryElement->FirstChildElement("Pkt");
        while(pktElement)
        {
            tinyxml2::XMLNode* pktnode = pktElement->FirstChild();
            if(pktnode)
            {
                tinyxml2::XMLText* text_t = pktnode->ToText();
                if(text_t)
                {
                    std::string rawpkt = text_t->Value();
                    packs.push_back(hex_to_string(rawpkt));
                }
            }
            pack_count++;
            pktElement = pktElement->NextSiblingElement("Pkt");
        }

        pack_vec.push_back(packs);
        entry_count++;
        entryElement = entryElement->NextSiblingElement("Entry");
    }

    mute.unlock();

    //restart the thread
    if(thr.joinable())
    {
        thr_run = false;
        thr.join();
    }
    thr = std::thread(&LogWindow::log_thread, this);
    file_label->set_text(fcd.get_filename());
}

void LogWindow::log_thread()
{
    thr_running = true;
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    bool was_encryption_on = coms->get_encryption_on();
    coms->set_encryption_on(false);

    thread_loop();

    coms->set_encryption_on(was_encryption_on);
    thr_running = false;
}

void LogWindow::thread_loop()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    if(coms->running())
        return;

    coms->m = COMS_LOG_PLAYBACK;

    thr_run = true;

    std::chrono::steady_clock::time_point log_timer = std::chrono::steady_clock::now();

    while(thr_run)
    {
        mute.lock();
        if((entry_pos < 0) || (entry_pos > pack_vec.size()-1))
        {
            PlayMode = PLAY_MODE_PAUSE;
        }

        if((PlayMode == PLAY_MODE_PLAY) || (PlayMode == PLAY_MODE_FFWD))
        {
            for(int i = 0; i < pack_vec[entry_pos].size(); i++)
            {
                coms->push_log_pack(pack_vec[entry_pos][i]);
            }
            entry_pos++;
            if(PlayMode == PLAY_MODE_FFWD)
                entry_pos += 4;
        }
        if(PlayMode == PLAY_MODE_REWIND)
        {
            for(int i = pack_vec[entry_pos].size()-1; i > 0; i--)
            {
                coms->push_log_pack(pack_vec[entry_pos][i]);
            }
            entry_pos-=5;
        }

        mute.unlock();

        typedef std::chrono::duration<int,std::milli> millisecs_t;
        millisecs_t dift = std::chrono::duration_cast<millisecs_t>(std::chrono::steady_clock::now()-log_timer);
        while(dift.count() < 100)
        {
            sleep_ms(1);
            dift = std::chrono::duration_cast<millisecs_t>(std::chrono::steady_clock::now()-log_timer);
        }
        log_timer = std::chrono::steady_clock::now();
    }
}


void LogWindow::on_slider_value_changed()
{
    if(ignore_slider_changed)
        return;

    mute.lock();
    int val = scale->get_adjustment()->get_value();
    entry_pos = val;
    mute.unlock();
}

void LogWindow::export_csv()
{
    Gtk::FileChooserDialog fcd(*this, "Save CSV as", Gtk::FILE_CHOOSER_ACTION_SAVE, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::string file_name = fcd.get_filename();

    ofstream ofile;
    ofile.open(file_name);
    if(!ofile.is_open())
    {
        cout << "Error saving to file" << endl;
        return;
    }

    ofile << "Volts,"
          << "Timer,"
          << "Amps,"
          << "Temp,"
          << "Power,"
          << "Motor armed,"
          << "Autothrottle,"
          << "Hil mode on,"
          << "Ground track,"
          << "Heading,"
          << "Pitch,"
          << "Roll,"
          << "Alt,"
          << "Vert speed,"
          << "Air speed,"
          << "Latitude,"
          << "Longitude,"
          << "Ground speed,"
          << "Satellites used,"
          << "GPS Mode,"
          << "AP Mode,"
          << "AP Pitch,"
          << "AP Roll,"
          << "AP Heading,"
          << "AP Alt,"
          << "Waypoint lat,"
          << "Waypoint lng,"
          << "Waypoint number" << endl;

    Coms lcoms;
    lcoms.m = COMS_LOG_PLAYBACK;

    try
    {
    for(int i = 0; i < pack_vec.size(); i++)
    {
        for(int j = 0; j < pack_vec[i].size(); j++)
        {
            lcoms.push_log_pack(pack_vec[i][j]);
        }

        ofile << lcoms.get_volts() << ", ";
        ofile << lcoms.get_timer() << ", ";
        ofile << lcoms.get_amps() << ", ";
        ofile << lcoms.get_temperature() << ", ";
        ofile << lcoms.get_power_remaining() << ", ";
        ofile << lcoms.get_motor_armed() << ", ";
        ofile << lcoms.get_autothrottle_on() << ", ";
        ofile << "0, "; //HIL mode on
        ofile << lcoms.get_ground_track() << ", ";
        ofile << lcoms.get_heading() << ", ";
        ofile << lcoms.get_pitch() << ", ";
        ofile << lcoms.get_roll() << ", ";
        ofile << lcoms.get_alt() << ", ";
        ofile << lcoms.get_vert_speed() << ", ";
        ofile << lcoms.get_air_speed() << ", ";
        ofile << lcoms.get_latitude() << ", ";
        cout << lcoms.get_latitude() << endl;
        ofile << lcoms.get_longitude() << ", ";
        ofile << lcoms.get_ground_speed() << ", ";
        ofile << lcoms.get_satellites_used() << ", ";
        ofile << (int)lcoms.get_gps_mode() << ", ";
        ofile << (int)lcoms.get_ap_mode() << ", ";
        ofile << lcoms.get_ap_pitch() << ", ";
        ofile << lcoms.get_ap_roll() << ", ";
        ofile << lcoms.get_ap_heading() << ", ";
        ofile << lcoms.get_ap_alt() << ", ";
        ofile << lcoms.get_ap_waypoint_lat() << ", ";
        ofile << lcoms.get_ap_waypoint_lng() << ", ";
        ofile << lcoms.get_ap_waypoint() << "\n";
    }
    }
    catch(std::exception& e)
    {
        Gtk::MessageDialog msg("std::exception", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text(e.what());
        msg.run();
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
    ofile.close();
    lcoms.stop();
}
