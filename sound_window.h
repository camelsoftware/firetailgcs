/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SOUND_WINDOW_H
#define SOUND_WINDOW_H

#include <gtkmm.h>
#include <gtkmm/volumebutton.h>
#include "camwav.h"

class SoundWindow : public Gtk::Window
{
public:
    SoundWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~SoundWindow();


    camwav::Buffer dtone_buffer;
    camwav::Sound disconnect_tone;

    camwav::Buffer alarm_buffer;
    camwav::Sound alarm_sound;

    camwav::Buffer buzzer_buffer;
    camwav::Sound buzzer_sound;

private:
    void on_mute();
    void on_vol_changed(double v);

    Glib::RefPtr<Gtk::Builder> refBuilder;

    Gtk::CheckButton* mute_check;
    Gtk::VolumeButton* disconnect_vol;
    Gtk::VolumeButton* alarm_vol;
    Gtk::VolumeButton* buzzer_vol;
};


#endif // SOUND_WINDOW_H
