/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "main_window.h"
#include "gcs_settings.h"
#include <exception>


void terminate_handler_foo()
{
    Gtk::MessageDialog msg("An error has occured and this program will now terminate. If you are able to reproduce this fault, please report it to samuel.cowen@camelsoftware.com");
    msg.run();
    abort();
}


int main(int argc, char *argv[])
{
    std::set_terminate(terminate_handler_foo);
    try
    {
        gcs_settings_init(); //load GCS settings

        Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "com.camelsoftware.FiretailGCS");
        std::string pths;
        pths = share_uri;
        pths += "main_window.glade";

        Glib::RefPtr<Gtk::Builder> builder;
        try
        {
            builder = Gtk::Builder::create_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading main_window.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "flight_display.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading flight_display.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }


        try
        {
            pths = share_uri;
            pths += "config.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading config.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "planner.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading planner.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "logs.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading logs.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "satellites.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading satellites.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "terminal.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading terminal.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "radio.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading radio.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "control_setup.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading control_setup.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }

        try
        {
            pths = share_uri;
            pths += "mixer_uploader.glade";
            builder->add_from_file(pths);
        }
        catch(Glib::MarkupError& e)
        {
            Gtk::MessageDialog msg("Error loading mixer_uploader.glade", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text(e.what());
            msg.run();
            return -1;
        }


        mainWindow* mw = 0;
        builder->get_widget_derived("window", mw);

        return app->run(*mw);
    }
    catch(Glib::MarkupError& e)
    {
        cout << e.what() << endl;
    }
    catch(Glib::FileError& e)
    {
        Gtk::MessageDialog msg("File error!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text(e.what());
        msg.run();
        return -1;
    }
    catch(Gtk::BuilderError& e)
    {
        Gtk::MessageDialog msg("Builder error!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text(e.what());
        msg.run();
        return -1;
    }
    catch(std::exception& e)
    {
        Gtk::MessageDialog msg("std::exception", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text(e.what());
        msg.run();
        return -1;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
}

