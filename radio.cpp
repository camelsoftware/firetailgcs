#include "radio.h"
#include "gcs_settings.h"

#include <iostream>
using namespace std;

RadioWindow::RadioWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("radio_serial_entry", port_entry);
    port_entry->set_text(gcs_set.radio_port);

    refBuilder->get_widget("radio_version_entry", version_entry);

    refBuilder->get_widget("radio_baud_rate_combo", baud_combo);
    refBuilder->get_widget("radio_air_speed_combo", air_speed_combo);
    refBuilder->get_widget("radio_net_id_spinbutton", net_id_spinbutton);
    refBuilder->get_widget("radio_tx_pwr_spinbutton", tx_pwr_spinbutton);
    refBuilder->get_widget("radio_node_dest_combo", node_dest_combo);
    refBuilder->get_widget("radio_min_freq_combo", min_freq_combo);
    refBuilder->get_widget("radio_max_freq_combo", max_freq_combo);
    refBuilder->get_widget("radio_num_channels_combo", num_chans_combo);
    refBuilder->get_widget("radio_duty_cycle_combo", duty_cycle_combo);
    refBuilder->get_widget("radio_node_count_combo", node_count_combo);
    refBuilder->get_widget("radio_node_id_combo", node_id_combo);

    refBuilder->get_widget("radio_sync_any_check", sync_any_check);
    refBuilder->get_widget("radio_ecc_check", ecc_check);
    refBuilder->get_widget("radio_op_resend_check", opp_resend_check);
    refBuilder->get_widget("radio_mavlink_check", mavlink_check);

    baud_combo->set_active(1);
    air_speed_combo->set_active(4);

    net_id_spinbutton->set_range(0, 499);
    net_id_spinbutton->set_digits(0);
    net_id_spinbutton->set_increments(1, 1);
    net_id_spinbutton->set_value(0);

    tx_pwr_spinbutton->set_range(0, 30);
    tx_pwr_spinbutton->set_value(25);
    tx_pwr_spinbutton->set_digits(0);
    tx_pwr_spinbutton->set_increments(1, 1);


    ecc_check->set_active(true);

    for(int i = 0; i <= 25; i++)
    {
        stringstream ss;
        ss << 902000 + (i*1000);
        min_freq_combo->append(ss.str());

        ss.clear();
        ss.str("");
        ss << 903000 + (i*1000);
        max_freq_combo->append(ss.str());
    }

    min_freq_combo->set_active(13);
    max_freq_combo->set_active(25);

    for(int i = 5; i <= 19; i++)
    {
        stringstream ss;
        ss << i;
        num_chans_combo->append(ss.str());
    }
    for(int i = 20; i <= 50; i += 10)
    {
        stringstream ss;
        ss << i;
        num_chans_combo->append(ss.str());
    }

    num_chans_combo->set_active(18);

    for(int i = 10; i <= 100; i+= 10)
    {
        stringstream ss;
        ss << i;
        duty_cycle_combo->append(ss.str());
    }

    for(int i = 0; i <= 29; i++)
    {
        stringstream ss;
        ss << i;
        node_id_combo->append(ss.str());
        node_dest_combo->append(ss.str());
    }

    for(int i = 2; i <= 30; i++)
    {
        stringstream ss;
        ss << i;
        node_count_combo->append(ss.str());
    }

    duty_cycle_combo->set_active(9);
    opp_resend_check->set_active(true);

    refBuilder->get_widget("radio_read_button", read_button);
    refBuilder->get_widget("radio_write_button", write_button);
    refBuilder->get_widget("radio_write_default_button", write_default_button);

    read_button->signal_clicked().connect(sigc::mem_fun(*this, &RadioWindow::read_settings));
    write_button->signal_clicked().connect(sigc::mem_fun(*this, &RadioWindow::write_settings));
    write_default_button->signal_clicked().connect(sigc::mem_fun(*this, &RadioWindow::write_default));

    rd_settings[1] = 57;
}

RadioWindow::~RadioWindow()
{

}

void RadioWindow::timeout()
{

}

bool RadioWindow::open_port()
{
    std::string address = port_entry->get_text();
    sp.setPort(address);

    int rdbd = rd_settings[1]; //radio baud
    int baud = 0;
    if(rdbd == 2)
        baud = 2400;
    if(rdbd == 4)
        baud = 4800;
    if(rdbd == 9)
        baud = 9600;
    if(rdbd == 19)
        baud = 19200;
    if(rdbd == 38)
        baud = 38400;
    if(rdbd == 57)
        baud = 57600;
    if(rdbd == 115)
        baud = 115200;

    sp.setBaudrate(baud);

    serial::Timeout st = serial::Timeout::simpleTimeout(25);
    st.write_timeout_constant = 10;

    sp.setTimeout(st);

    try
    {
        sp.open();
    }
    catch(...)
    {
        Gtk::MessageDialog dialog(*this, "Could not connect!",
                                  false, Gtk::MESSAGE_INFO,
                                  Gtk::BUTTONS_OK);
        dialog.set_secondary_text(
            "The serial port could not be opened.");
        dialog.run();
        return false;
    }

    if(!sp.isOpen())
    {
        Gtk::MessageDialog dialog(*this, "Could not connect!",
                                  false, Gtk::MESSAGE_INFO,
                                  Gtk::BUTTONS_OK);
        dialog.set_secondary_text(
            "The serial port could not be opened.");
        dialog.run();
        return false;
    }

    snprintf(gcs_set.radio_port, 120, "%s", address.c_str());
    gcs_settings_write();

    return true;
}

bool RadioWindow::enter_at_mode()
{
    if(!sp.isOpen())
        return false;

    bool in_at_mode = false;
    for(int i = 0; i <= 5; i++)
    {
        while(sp.available())
            sp.read();

        sleep_ms(1000);
        sp.write("+++");
        std::string ret = sp.readline();
        if((ret[0] == 'O') && (ret[1] == 'K'))
        {
            in_at_mode = true;
            break;
        }
    }

    if(!in_at_mode)
    {
        Gtk::MessageDialog dialog(*this, "Could not connect!",
                                  false, Gtk::MESSAGE_INFO,
                                  Gtk::BUTTONS_OK);
        dialog.set_secondary_text(
            "The modem did not enter AT command mode. Please check that you have the correct baud rate selected.");
        dialog.run();

        sp.close();
        return false;
    }

    sleep_ms(1050);
    return true;
}

void RadioWindow::write_settings()
{
    if(!open_port())
        return;

    if(!enter_at_mode())
        return;

    sp.write("\r\nATI\r\n");
    sleep_ms(100);

    float version = 0;
    while(sp.available())
    {
        std::string possible_version = sp.readline();
        if(possible_version.length() < 5)
            continue;
        if(possible_version.find("SiK") != -1)
        {
            version = atof(&possible_version[4]);

            int retpos = possible_version.find('\r');
            possible_version.resize(retpos);
            version_entry->set_text(possible_version);

            if(version < 2.0)
            {
                node_count_combo->set_sensitive(false);
                node_id_combo->set_sensitive(false);
                node_dest_combo->set_sensitive(false);
                sync_any_check->set_sensitive(false);
            }
            else
            {
                node_count_combo->set_sensitive(true);
                node_id_combo->set_sensitive(true);
                node_dest_combo->set_sensitive(true);
                sync_any_check->set_sensitive(true);
            }
        }
    }

    for(int i = 0; i <= 18; i++)
    {
        if((i > 11) && (version < 2.0))
            break;

        switch(i)
        {
        case 1: //serial speed
        {
            std::string output = "ATS1=";

            std::string bd = baud_combo->get_active_text();
            bd.resize(bd.length()-3);
            output += bd;
            output += "\r\n";
            sp.write(output);
        }
        break;
        case 2: //air speed
        {
            std::string output = "ATS2=";
            std::string as = air_speed_combo->get_active_text();
            output += as;
            output += "\r\n";
            sp.write(output);
        }
        break;
        case 3: //net id
        {
            stringstream ss;
            ss << "ATS3=" << net_id_spinbutton->get_value_as_int() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 4: //tx power
        {
            stringstream ss;
            ss << "ATS4=" << tx_pwr_spinbutton->get_value_as_int() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 5: //ECC
        {
            stringstream ss;
            ss << "ATS5=" << int(ecc_check->get_active()) << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 6: //mavlink
        {
            stringstream ss;
            ss << "ATS6=" << int(mavlink_check->get_active()) << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 7: //op resend
        {
            stringstream ss;
            ss << "ATS7=" << int(opp_resend_check->get_active()) << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 8: //min freq
        {
            stringstream ss;
            ss << "ATS8=" << min_freq_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 9: //max freq
        {
            stringstream ss;
            ss << "ATS9=" << max_freq_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 10://num channels
        {
            stringstream ss;
            ss << "ATS10=" << num_chans_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 11: //duty cycle
        {
            stringstream ss;
            ss << "ATS11=" << duty_cycle_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 15: //node id
        {
            stringstream ss;
            ss << "ATS15=" << node_id_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 16: //node destination
        {
            stringstream ss;
            ss << "ATS16=" << node_dest_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 17: //sync any
        {
            stringstream ss;
            ss << "ATS17=" << int(sync_any_check->get_active()) << "\r\n";
            sp.write(ss.str());
        }
        break;
        case 18: //node count
        {
            stringstream ss;
            ss << "ATS18=" << node_count_combo->get_active_text() << "\r\n";
            sp.write(ss.str());
        }
        break;
        }
    }

    sp.write("AT&W\r\nATO\r\n");
    sp.flush();
    sp.close();
}

void RadioWindow::read_settings()
{
    rd_settings[1] = atoi(baud_combo->get_active_text().c_str())/1000;
    if(!open_port())
        return;

    if(!enter_at_mode())
        return;

    sp.write("\r\nATI\r\n");
    sleep_ms(100);

    float version = 0;
    while(sp.available())
    {
        std::string possible_version = sp.readline();
        if(possible_version.length() < 5)
            continue;
        if(possible_version.find("SiK") != -1)
        {
            version = atof(&possible_version[4]);

            int retpos = possible_version.find('\r');
            possible_version.resize(retpos);
            version_entry->set_text(possible_version);

            if(version < 2.0)
            {
                node_count_combo->set_sensitive(false);
                node_id_combo->set_sensitive(false);
                node_dest_combo->set_sensitive(false);
                sync_any_check->set_sensitive(false);
            }
            else
            {
                node_count_combo->set_sensitive(true);
                node_id_combo->set_sensitive(true);
                node_dest_combo->set_sensitive(true);
                sync_any_check->set_sensitive(true);
            }
        }
    }

    sp.write("\r\nATI5\r\n");
    sleep_ms(100);

    while(sp.available())
    {
        char strbuf[120];
        std::string ln = sp.readline();

        int id = 0;
        int setting = 0;

        id = atoi(ln.c_str()+1);

        for(int i = 0; i <= ln.length(); i++)
        {
            if(ln[i] == '=')
            {
                setting = atoi(ln.c_str()+i+1);
            }
        }

        if((id == 0) && (setting == 0)) //probs not a setting
            continue;

        if((id > 11) && (version < 2.0)) //early versions don't have these higher settings
            continue;

        rd_settings[id] = setting;

        switch(id)
        {
        case 0: //format, ignore
        {

        }
        break;
        case 1: //serial speed
        {
            if(setting == 2)
                baud_combo->set_active_text("2400");
            if(setting == 4)
                baud_combo->set_active_text("4800");
            if(setting == 9)
                baud_combo->set_active_text("9600");
            if(setting == 19)
                baud_combo->set_active_text("19200");
            if(setting == 38)
                baud_combo->set_active_text("38400");
            if(setting == 57)
                baud_combo->set_active_text("57600");
            if(setting == 115)
                baud_combo->set_active_text("115200");
        }
        break;
        case 2: //air speed
        {
            stringstream ss;
            ss << setting;
            air_speed_combo->set_active_text(ss.str());
        }
        break;
        case 3: //net ID
        {
            net_id_spinbutton->set_value(setting);
        }
        break;
        case 4: //Tx power
        {
            tx_pwr_spinbutton->set_value(setting);
        }
        break;
        case 5: //ECC
        {
            ecc_check->set_active(setting);
        }
        break;
        case 6: //mavlink
        {
            mavlink_check->set_active(setting);
        }
        break;
        case 7: //op resend
        {
            opp_resend_check->set_active(setting);
        }
        break;
        case 8: //min freq
        {
            stringstream ss;
            ss << setting;
            min_freq_combo->set_active_text(ss.str());
        }
        break;
        case 9: //max freq
        {
            stringstream ss;
            ss << setting;
            max_freq_combo->set_active_text(ss.str());
        }
        break;
        case 10: //num channels
        {
            stringstream ss;
            ss << setting;
            num_chans_combo->set_active_text(ss.str());
        }
        break;
        case 11: //duty cycle
        {
            stringstream ss;
            ss << setting;
            duty_cycle_combo->set_active_text(ss.str());
        }
        break;
        case 15: //node id
        {
            node_id_combo->set_active(setting);
        }
        break;
        case 16: //node destination
        {
            node_dest_combo->set_active(setting);
        }
        break;
        case 17: //sync any
        {
            sync_any_check->set_active(setting);
        }
        break;
        case 18: //node count
        {
            node_count_combo->set_active(setting);
        }
        break;
        }
        sleep_ms(10);
    }

    sp.write("\r\nATO\r\n");
    sp.flush();
    sp.close();
}

void RadioWindow::write_default()
{
    baud_combo->set_active_text("57600");
    air_speed_combo->set_active_text("64");
    net_id_spinbutton->set_value(0);
    tx_pwr_spinbutton->set_value(25);
    min_freq_combo->set_active_text("915000");
    max_freq_combo->set_active_text("928000");
    num_chans_combo->set_active_text("50");
    duty_cycle_combo->set_active_text("100");
    ecc_check->set_active();
    opp_resend_check->set_active();
    mavlink_check->set_active(false);

    write_settings();
}


