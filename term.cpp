#include "term.h"
#include "utility.h"
#include "gcs_settings.h"

#include <iostream>

using namespace std;

TermConnectDlg::TermConnectDlg()
{
    Gtk::VBox* vbox = Gtk::manage(new Gtk::VBox());
    Gtk::HBox* box = Gtk::manage(new Gtk::HBox());
    Gtk::Label* lbl = Gtk::manage(new Gtk::Label());
    lbl->set_text("Serial port");
    addr_entry = Gtk::manage(new Gtk::Entry());
    addr_entry->set_text(gcs_set.terminal_port);

    box->pack_start(*lbl);
    box->pack_start(*addr_entry);

    vbox->pack_start(*box);

    box = Gtk::manage(new Gtk::HBox());
    lbl = Gtk::manage(new Gtk::Label());
    lbl->set_text("Baud");
    baud_entry = Gtk::manage(new Gtk::Entry());
    char bd[120];
    memset(bd, '\0', 120);
    snprintf(bd, 120, "%i", gcs_set.terminal_baud);
    baud_entry->set_text(bd);

    box->pack_start(*lbl);
    box->pack_start(*baud_entry);

    vbox->pack_start(*box);

    get_vbox()->pack_start(*vbox, true, true, 0);
    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);

    std::string iconpath = share_uri;
    iconpath += "firetail.png";
    set_icon_from_file(iconpath);
    show_all();

}

TermConnectDlg::~TermConnectDlg() { }



Terminal::Terminal(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("term_send_button", send_btn);
    refBuilder->get_widget("term_entry", term_entry);
    refBuilder->get_widget("term_text_view", text_view);

    refBuilder->get_widget("append_newline_check", newline_check);

    text_buffer = Gtk::TextBuffer::create();
    text_view->set_buffer(text_buffer);

    Glib::RefPtr<Gtk::TextBuffer::TagTable> table = text_buffer->get_tag_table();
    green_tag = Gtk::TextTag::create("green_tag");
    green_tag->property_foreground() = "green";

    blue_tag = Gtk::TextTag::create("blue_tag");
    blue_tag->property_foreground() = "blue";

    table->add(green_tag);
    table->add(blue_tag);

    send_btn->signal_clicked().connect(sigc::mem_fun(*this, &Terminal::send));
    term_entry->signal_activate().connect(sigc::mem_fun(*this, &Terminal::send));
}


bool Terminal::on_delete_event(GdkEventAny* event)
{
    if(sp.isOpen())
        sp.close();

    return false;
}

void Terminal::show()
{
    TermConnectDlg cd;
    while(cd.run() == Gtk::RESPONSE_OK)
    {
        std::string address = cd.get_address();
        sp.setPort(address);
        sp.setBaudrate(cd.get_baud());

        snprintf(gcs_set.terminal_port, 120, "%s", address.c_str());
        gcs_set.terminal_baud = cd.get_baud();
        gcs_settings_write();

        serial::Timeout st = serial::Timeout::simpleTimeout(25);
        st.write_timeout_constant = 10;

        sp.setTimeout(st);
        try
        {
            sp.open();
        }
        catch(...)
        {
            Gtk::MessageDialog dialog(*this, "Exception!",
                                    false, Gtk::MESSAGE_INFO,
                                    Gtk::BUTTONS_OK);
            dialog.set_secondary_text(
                        "");
            dialog.run();
        }
        if(!sp.isOpen())
        {
            Gtk::MessageDialog dialog(*this, "Could not connect!",
                                    false, Gtk::MESSAGE_INFO,
                                    Gtk::BUTTONS_OK);
            dialog.set_secondary_text(
                        "");
            dialog.run();
        }
        else
        {
            sp.write("term\n");
            while(sp.available())
                sp.read();

            text_buffer->set_text("");
            Gtk::Window::show();
            return;
        }
    }
}


void Terminal::timeout()
{
    try
    {
        std::string buf;
        while(sp.available())
            buf += sp.read();

        text_buffer->insert_with_tag(text_buffer->get_iter_at_offset(text_buffer->get_text().length()), buf, blue_tag);

        if(buf.length() > 0)
        {
            Gtk::TextIter ti = text_buffer->get_iter_at_offset(text_buffer->get_text().length());
            text_view->scroll_to(ti);
        }
    }
    catch(...)
    {
        cout << "Exception in terminal::timeout()" << endl;
    }
}

void Terminal::send()
{
    try
    {
        Glib::ustring snd = term_entry->get_text();
        if(newline_check->get_active())
            snd += "\n";
        sp.write(snd);
        sp.flush();

        if(snd.validate())
        {
            Gtk::TextIter end_iter = text_buffer->get_iter_at_offset(text_buffer->get_text().length());
            text_buffer->insert_with_tag(end_iter, snd, green_tag);

            term_entry->set_text("");

            end_iter = text_buffer->get_iter_at_offset(text_buffer->get_text().length());
            text_view->scroll_to(end_iter);
        }
        else
        {
            cout << "invalid text" << endl;
        }
    }
    catch(...)
    {
        cout << "Exception in Terminal::send()" << endl;
    }
}
