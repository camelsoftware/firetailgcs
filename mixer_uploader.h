/*
 *     Copyright (C) 2015  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MIXER_UPLOADER_H_INCLUDED
#define MIXER_UPLOADER_H_INCLUDED

#include <iostream>
#include <gtkmm.h>
#include <fstream>
#include "coms.h"
#include "utility.h"

using namespace std;

class MixerUploader : public Gtk::Window
{
public:
    MixerUploader(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    virtual ~MixerUploader()
    {
        return;
    }

    void show();
    void timeout();

private:

    std::string mix_file;
    int part;
    int n_parts;

    void on_close();

    Gtk::Button* btn;
    Gtk::ProgressBar* pbar;
    Glib::RefPtr<Gtk::Builder> refBuilder;
};


#endif // CONNECT_H_INCLUDED

