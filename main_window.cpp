/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "main_window.h"
#include "gcs_settings.h"



mainWindow::mainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;


    refBuilder->get_widget("connections_treeview", coms_tree);
    refcomslist = Gtk::ListStore::create(coms_columns);
    coms_tree->set_model(refcomslist);

    coms_tree->append_column("Port", coms_columns.m_port);
    coms_tree->append_column("Name", coms_columns.m_vehiclename);
    coms_tree->append_column("PPS", coms_columns.m_pps);

    m_toggle_column.set_title("Active");
    m_toggle_column.pack_start(toggle_renderer);
    coms_tree->append_column(m_toggle_column);

    m_toggle_column.set_cell_data_func(toggle_renderer, sigc::mem_fun(*this,
                                       &mainWindow::toggle_cell_data_func));

    toggle_renderer.set_activatable();

    toggle_renderer.signal_toggled().connect(sigc::mem_fun(*this, &mainWindow::on_active_toggled));


    coms_tree->get_column(0)->set_min_width(200);
    coms_tree->get_column(1)->set_min_width(200);
    coms_tree->get_column(2)->set_min_width(100);
    coms_tree->get_column(3)->set_max_width(100);

    coms_tree->signal_row_activated().connect(sigc::mem_fun(*this, &mainWindow::on_treeview_row_activated));


    refBuilder->get_widget("connect_button", connect);
    refBuilder->get_widget("fly_button", fly);
    refBuilder->get_widget("plan_button", plan);
    refBuilder->get_widget("configure_button", configure);
    refBuilder->get_widget("logs_button", logs);
    refBuilder->get_widget("radios_button", radio_btn);
    refBuilder->get_widget("terminal_button", term);
    refBuilder->get_widget("about_button", about);

    refBuilder->get_widget("connected_lbl", connected_lbl);
    refBuilder->get_widget("connection_strength_lbl", con_strength_lbl);

    refBuilder->get_widget("connect_label", connect_lbl);

    connect->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_connect_button_clicked));
    fly->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_fly_button_clicked));
    plan->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_plan_button_clicked));
    configure->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_configure_button_clicked));
    logs->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_logs_button_clicked));
    radio_btn->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_radio_button_clicked));
    term->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_terminal_button_clicked));
    about->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_about_button_clicked));

    refBuilder->get_widget_derived("flight_window", flightdisplay);
    refBuilder->get_widget_derived("planner_window", planner);
    refBuilder->get_widget_derived("config_window", config);
    refBuilder->get_widget_derived("terminal_window", terminal);
    refBuilder->get_widget_derived("log_window", logwindow);
    refBuilder->get_widget_derived("radio_window", radio);

    sigc::slot<bool> slot = sigc::bind(sigc::mem_fun(*this, &mainWindow::timer), 0);
    sigc::connection conn = Glib::signal_timeout().connect(slot, 50);

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    set_title("Firetail GCS");

    maximize();
}


mainWindow::~mainWindow()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->stop();
}


void mainWindow::on_active_toggled(Glib::ustring path)
{
    Coms::set_active_connection(atoi(path.c_str()));
}

void mainWindow::on_treeview_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column)
{
    int row_number = atoi(path.to_string().c_str());


    Gtk::TreeModel::Row row = refcomslist->children()[row_number];

    stringstream ss;
    ss << "Do you really want to disconnect from "
       << Coms::get_instance(row_number)->get_vehicle_title()
       << " on " << Coms::get_instance(row_number)->address << "?";

    Gtk::MessageDialog dialog(*this, ss.str(),
                              false, Gtk::MESSAGE_INFO,
                              Gtk::BUTTONS_YES_NO);
    if(dialog.run() == Gtk::RESPONSE_YES)
    {
        Coms::remove_connection(row_number);

        refcomslist->erase(refcomslist->children()[row_number]);
    }
}


bool mainWindow::on_delete_event(GdkEventAny* event)
{
    while(Coms::get_n_connections())
    {
        Coms::remove_connection(0);
    }
}

void mainWindow::toggle_cell_data_func(Gtk::CellRenderer* ren, const Gtk::TreeModel::iterator& iter)
{
    if(iter)
    {
        Gtk::CellRendererToggle* treg = (Gtk::CellRendererToggle*)ren;
        Gtk::TreeModel::Row row = *iter;
        treg->set_active(row[coms_columns.m_active]);
    }
}

void mainWindow::on_connect_button_clicked()
{
    connectDlg connect_dlg;
    if(connect_dlg.run() == Gtk::RESPONSE_OK)
    {
        shared_ptr<Coms> coms = Coms::add_new_connection();
        cout << "new connection created" << endl;
        if(connect_dlg.page == 0)
        {
            coms->address = connect_dlg.coms_port;
            coms->baudport = connect_dlg.baud;
            coms->m = COMS_SERIAL;
        }
        if(connect_dlg.page == 1)
        {
            coms->address = connect_dlg.udp_addr;
            coms->baudport = connect_dlg.udp_port;
            coms->m = COMS_UDP;
        }

        cout << "starting coms" << endl;
        coms->start();

        //pause while thread begins to run
        if(coms->running())
        {

            Gtk::TreeModel::Row row = *(refcomslist->append());
            row[coms_columns.m_port] = coms->address;
            row[coms_columns.m_vehiclename] = coms->get_vehicle_title();
            row[coms_columns.m_pps] = coms->connection_strength();

            coms->lock();
            coms->get_vehicle_name();
            coms->get_instrument_settings();
            coms->unlock();
        }
        else
        {
            Coms::remove_connection(Coms::get_n_connections()-1);
            Gtk::MessageDialog dialog(*this, "Couldn't open coms port!",
                                      false, Gtk::MESSAGE_INFO,
                                      Gtk::BUTTONS_OK);
            dialog.set_secondary_text(
                "Please check that your radio is connected.");
            dialog.run();
        }
    }

}

void mainWindow::on_fly_button_clicked()
{
    flightdisplay->show();
}


void mainWindow::on_plan_button_clicked()
{
    planner->show();
}

void mainWindow::on_configure_button_clicked()
{
    config->show();
}

void mainWindow::on_radio_button_clicked()
{
    radio->show();
}

void mainWindow::on_terminal_button_clicked()
{
    terminal->show();
}

void mainWindow::on_about_button_clicked()
{
    AboutDlg aboutdlg;
    aboutdlg.run();
}

void mainWindow::on_logs_button_clicked()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    if(coms->running())
    {
        Gtk::MessageDialog dialog(*this, "Disconnect first!",
                                  false, Gtk::MESSAGE_INFO,
                                  Gtk::BUTTONS_OK);
        dialog.set_secondary_text(
            "You can't replay a log while the Coms system is running. Please disconnect and try again.");
        dialog.run();
        return;
    }
    logwindow->show();
}

bool mainWindow::timer(int n)
{
    if(flightdisplay->is_visible())
        flightdisplay->redraw();
    if(config->is_visible())
        config->timeout();
    if(planner->is_visible())
        planner->timeout();
    if(terminal->is_visible())
        terminal->timeout();
    if(logwindow->is_visible())
        logwindow->timeout();


    redraw_timeout++;
    if(redraw_timeout < 10)
        return true;
    redraw_timeout = 0;

    char txt[100];
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    coms->lock();
    bool cr = coms->running();
    int n_cons = Coms::get_n_connections();
    sprintf(txt, "Link Quality: %i pps", coms->connection_strength());
    coms->unlock();

    con_strength_lbl->set_text(txt);

    if((coms->m == COMS_LOG_PLAYBACK) && (coms->running()))
    {
        connect->set_sensitive(false);
        connect_lbl->set_text("Replaying log");
    }
    else
    {
        connect->set_sensitive(true);
        connect_lbl->set_text("Connect");
    }

    Gtk::TreeModel::iterator iter = refcomslist->children().begin();
    for(int i = 0; i < n_cons; i++)
    {
        shared_ptr<Coms> tmpcom = Coms::get_instance(i);
        if(refcomslist->children().end() == iter)
            break;

        Gtk::TreeModel::Row row = *iter;
        iter++;


        tmpcom->lock();
        row[coms_columns.m_port] = tmpcom->address;
        row[coms_columns.m_vehiclename] = tmpcom->get_vehicle_title();
        row[coms_columns.m_pps] = tmpcom->connection_strength();
        tmpcom->unlock();

        if(i == Coms::get_active_connection())
            row[coms_columns.m_active] = true;
        else
            row[coms_columns.m_active] = false;
    }

    return true;
}



