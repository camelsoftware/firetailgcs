/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "about.h"

#include "version.h"
#include "utility.h"
#include "FireLink/firelink.h"

int AboutDlg::run()
{
    set_comments("The Firelink Ground Control Station");
    set_copyright("© 2013-2015 Samuel Cowen");

    std::string version = "Version ";
    version += AutoVersion::FULLVERSION_STRING;
    version += " ";
    version += AutoVersion::STATUS;

    version += "\nCompiled with Firelink version 2.0";
    set_version(version);

    std::string iconfile;
    iconfile = share_uri;
    iconfile += "firetail.png";

    set_icon_from_file(iconfile);

    Glib::RefPtr<Gdk::Pixbuf> ppbuf = Gdk::Pixbuf::create_from_file(iconfile);
    set_logo(ppbuf);

    return AboutDialog::run();
}
