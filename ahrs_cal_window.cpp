/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "ahrs_cal_window.h"


static std::vector<std::array<double, 3> > mag_samples; //a vector of vectors!
static std::vector<std::array<double, 3> > culled_samples;

imu::Vector<3> smooth_accel; //low pass filtered acceleration


AccDa::AccDa(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::DrawingArea(cobject) { }

AccDa::~AccDa() { }

void AccDa::redraw(double x, double y, double z)
{
    acc_x = x;
    acc_y = y;
    acc_z = z;

    Glib::RefPtr<Gdk::Window> win = get_window();
    if(win)
    {
        win->invalidate(false);
    }
}

bool AccDa::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
    cr->rectangle(0, 0, width, height);
    cr->fill();

    cr->set_source_rgba(0.8, 0.1, 0.1, 1.0);
    cr->move_to(10, 10);
    cr->show_text("Acceleration");
    cr->fill();

    std::valarray<double> dsh = { 2.0 };
    cr->set_dash(dsh, 1);
    cr->set_source_rgba(0.1, 0.1, 0.8, 1.0);

    cr->move_to(width/2.0, 0);
    cr->line_to(width/2.0, height);
    cr->stroke();

    cr->move_to(0, height/2.0);
    cr->line_to(width, height/2.0);
    cr->stroke();

    cr->move_to(0, height);
    cr->line_to(width, 0);
    cr->stroke();


    cr->set_source_rgba(0.8, 0.1, 0.1, 1.0);
    cr->unset_dash();
    cr->move_to(width/2.0, height/2.0);
    cr->rel_line_to(0, -(acc_z*80));
    cr->stroke();

    cr->move_to(width/2.0, height/2.0);
    cr->rel_line_to(-(acc_x*40.0), (acc_x*40.0));
    cr->stroke();

    cr->move_to(width/2.0, height/2.0);
    cr->rel_line_to((acc_y*80.0), 0);
    cr->stroke();

    return true;
}



MagDa::MagDa(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::DrawingArea(cobject)
{

}

MagDa::~MagDa()
{

}

void MagDa::redraw(AXIS_TYPE ast)
{
    axt = ast;

    Glib::RefPtr<Gdk::Window> win = get_window();
    if(win)
    {
        win->invalidate(false);
    }
}

bool MagDa::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
    cr->rectangle(0, 0, width, height);
    cr->fill();

    cr->set_source_rgba(0.8, 0.1, 0.1, 1.0);

    int r1 = 0, r2 = 0; //rows to use
    switch(axt)
    {
    case AXIS_XY:
    {
        r1 = 0; r2 = 1;
        cr->move_to(10, 10);
        cr->show_text("X-Y");
    }
    break;
    case AXIS_XZ:
    {
        r1 = 0; r2 = 2;
        cr->move_to(10, 10);
        cr->show_text("X-Z");
    }
    break;
    case AXIS_YZ:
    {
        r1 = 1; r2 = 2;
        cr->move_to(10, 10);
        cr->show_text("Y-Z");
    }
    break;
    }

    cr->fill();

    for(unsigned int i = 0; i < culled_samples.size(); i++)
    {
        std::array<double, 3> smpl = culled_samples[i];
        cr->rectangle((smpl[r1]/70.0)+(width/2.0), (smpl[r2]/70.0)+(height/2.0), 1, 1);
        cr->fill();
    }

    cr->set_source_rgba(0.1, 0.1, 0.8, 1.0);
    cr->set_line_width(1.0);

    cr->move_to(width/2.0, (height/2.0)+10);
    cr->line_to(width/2.0, (height/2.0)-10);
    cr->stroke();

    cr->move_to((width/2.0)+10, (height/2.0));
    cr->line_to((width/2.0)-10, (height/2.0));
    cr->stroke();

    return true;
}


AhrsCal::AhrsCal(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    new_cal_done = false;
    refBuilder = _refBuilder;

    refBuilder->get_widget("cal_sample_mag_toggle", sample_mag_toggle);

    refBuilder->get_widget("cal_cull_spin", cull_spin);
    cull_spin->set_range(0, 100);
    cull_spin->set_increments(0.1, 0.1);
    cull_spin->set_digits(1);
    cull_spin->set_value(0);

    cull_spin->signal_changed().connect(sigc::mem_fun(*this, &AhrsCal::on_cull_spin_changed));

    refBuilder->get_widget("cal_total_samp_lbl", total_samp_lbl);
    refBuilder->get_widget("cal_samp_used_lbl", used_samp_lbl);

    refBuilder->get_widget("cal_calibrate_button", calibrate_button);
    calibrate_button->signal_clicked().connect(sigc::mem_fun(*this, &AhrsCal::on_calibrate));

    refBuilder->get_widget_derived("cal_xy_da", graph_xy);
    refBuilder->get_widget_derived("cal_xz_da", graph_xz);
    refBuilder->get_widget_derived("cal_yz_da", graph_yz);

    refBuilder->get_widget_derived("cal_accel_da", accel_da);

    refBuilder->get_widget("accel_bias_entry_x", accel_bias_x_entry);
    refBuilder->get_widget("accel_bias_entry_y", accel_bias_y_entry);
    refBuilder->get_widget("accel_bias_entry_z", accel_bias_z_entry);

    refBuilder->get_widget("mag_gain_entry_x", mag_gain_x_entry);
    refBuilder->get_widget("mag_gain_entry_y", mag_gain_y_entry);
    refBuilder->get_widget("mag_gain_entry_z", mag_gain_z_entry);

    refBuilder->get_widget("mag_bias_entry_x", mag_bias_x_entry);
    refBuilder->get_widget("mag_bias_entry_y", mag_bias_y_entry);
    refBuilder->get_widget("mag_bias_entry_z", mag_bias_z_entry);

    refBuilder->get_widget("cal_cancel_button", cancel_button);
    cancel_button->signal_clicked().connect(sigc::mem_fun(*this, &Gtk::Window::hide));

    refBuilder->get_widget("cal_ok_button", ok_button);
    ok_button->signal_clicked().connect(sigc::mem_fun(*this, &AhrsCal::on_ok_clicked));

    refBuilder->get_widget("cal_reset_button", reset_button);
    reset_button->signal_clicked().connect(sigc::mem_fun(*this, &AhrsCal::on_reset_clicked));
}


void AhrsCal::show()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    coms->set_vh_stream_prio(Firelink::VS_RAW_MARG, Firelink::PACK_PRIO_HIGH);
    coms->unlock();

    graph_xy->redraw(MagDa::AXIS_TYPE::AXIS_XY);
    graph_xz->redraw(MagDa::AXIS_TYPE::AXIS_XZ);
    graph_yz->redraw(MagDa::AXIS_TYPE::AXIS_YZ);

    accel_da->redraw(0, 0, 0);

    Gtk::Window::show();
}

bool AhrsCal::on_delete_event(GdkEventAny* ent)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    coms->lock();
    coms->set_vh_stream_prio(Firelink::VS_RAW_MARG, Firelink::PACK_PRIO_OFF);
    coms->unlock();
    return false;
}

void AhrsCal::timeout()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    Firelink::vs_raw_marg am = coms->get_raw_marg();
    coms->unlock();

    if(sample_mag_toggle->get_active())
    {
        std::array<double, 3> smpl;
        smpl[0] = am.mag_x;
        smpl[1] = am.mag_y;
        smpl[2] = am.mag_z;
        mag_samples.push_back(smpl);

        culled_samples = mag_samples;

        graph_xy->redraw(MagDa::AXIS_TYPE::AXIS_XY);
        graph_xz->redraw(MagDa::AXIS_TYPE::AXIS_XZ);
        graph_yz->redraw(MagDa::AXIS_TYPE::AXIS_YZ);
    }

    imu::Vector<3> acc;
    acc.x() = am.acc_x/1000.0;
    acc.y() = am.acc_y/1000.0;
    acc.z() = am.acc_z/1000.0;

    smooth_accel = (smooth_accel*0.6) + (acc*0.4);

    accel_da->redraw(smooth_accel.x(), smooth_accel.y(), smooth_accel.z());

    stringstream ss;
    ss << "Total Samples: " << mag_samples.size();
    total_samp_lbl->set_text(ss.str());

    int sused = mag_samples.size() * (1.0 - (cull_spin->get_value()/100.0));
    ss.str("");
    ss.clear();
    ss << "Used Samples: " << sused;
    used_samp_lbl->set_text(ss.str());
}

void AhrsCal::on_cull_spin_changed()
{
    //culls the samples to remove any abnormally lengthy vectors
    //unusually long samples are probably caused by interferrence of some description and will distort the results
    sample_mag_toggle->set_active(false);

    culled_samples = mag_samples;

    float pc = (cull_spin->get_value()/100.0);
    int n_removals = mag_samples.size() * pc;

    double biggest_mag = 0;
    int pos = 0;
    for(int i = 0; i < n_removals; i++)
    {
        biggest_mag = 0;
        pos = 0;

        for(unsigned int j = 0; j < culled_samples.size(); j++)
        {
            imu::Vector<3> smpl;
            smpl.x() = culled_samples[j][0];
            smpl.y() = culled_samples[j][1];
            smpl.z() = culled_samples[j][2];

            if(smpl.magnitude() > biggest_mag)
            {
                biggest_mag = smpl.magnitude();
                pos = j;
            }
        }

        culled_samples.erase(culled_samples.begin()+(pos));
    }

    graph_xy->redraw(MagDa::AXIS_TYPE::AXIS_XY);
    graph_xz->redraw(MagDa::AXIS_TYPE::AXIS_XZ);
    graph_yz->redraw(MagDa::AXIS_TYPE::AXIS_YZ);
}

void AhrsCal::on_calibrate()
{
    //first calculate bias (hard iron) corrections
    double x_min = 0, x_max = 0;
    double y_min = 0, y_max = 0;
    double z_min = 0, z_max = 0;

    double x_bias, y_bias, z_bias;

    for(unsigned int i = 0; i < culled_samples.size(); i++)
    {
        if(culled_samples[i][0] < x_min)
            x_min = culled_samples[i][0];
        if(culled_samples[i][0] > x_max)
            x_max = culled_samples[i][0];

        if(culled_samples[i][1] < y_min)
            y_min = culled_samples[i][1];
        if(culled_samples[i][1] > y_max)
            y_max = culled_samples[i][1];

        if(culled_samples[i][2] < z_min)
            z_min = culled_samples[i][2];
        if(culled_samples[i][2] > z_max)
            z_max = culled_samples[i][2];
    }

    x_bias = (x_min + x_max)/2.0;
    y_bias = (y_min + y_max)/2.0;
    z_bias = (z_min + z_max)/2.0;

    //bias has been calculated, now to correct for soft iron errors (gain)
    imu::Vector<3> vmax;
    vmax.x() = x_max - x_bias;
    vmax.y() = y_max - y_bias;
    vmax.z() = z_max - z_bias;

    imu::Vector<3> vmin;
    vmin.x() = x_min - x_bias;
    vmin.y() = y_min - y_bias;
    vmin.z() = z_min - z_bias;

    imu::Vector<3> avgs;
    avgs = vmax + (vmin*-1); //multiply by -1 to make negative values positive
    avgs = avgs / 2.0;

    double avg_rad = avgs.x() + avgs.y() + avgs.z();
    avg_rad /= 3.0;

    double x_scale = (avg_rad/avgs.x());
    double y_scale = (avg_rad/avgs.y());
    double z_scale = (avg_rad/avgs.z());


    for(int i = 0; i < culled_samples.size(); i++)
    {
        culled_samples[i][0] -= x_bias;
        culled_samples[i][1] -= y_bias;
        culled_samples[i][2] -= z_bias;

        culled_samples[i][0] *= x_scale;
        culled_samples[i][1] *= y_scale;
        culled_samples[i][2] *= z_scale;
    }

    stringstream ss;
    ss << x_bias;
    mag_bias_x_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << y_bias;
    mag_bias_y_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << z_bias;
    mag_bias_z_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << x_scale;
    mag_gain_x_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << y_scale;
    mag_gain_y_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << z_scale;
    mag_gain_z_entry->set_text(ss.str());

    graph_xy->redraw(MagDa::AXIS_TYPE::AXIS_XY);
    graph_xz->redraw(MagDa::AXIS_TYPE::AXIS_XZ);
    graph_yz->redraw(MagDa::AXIS_TYPE::AXIS_YZ);

    //accelerometer bias calibration now
    double acc_x, acc_y, acc_z;
    acc_z = 1.0 - smooth_accel.z();
    acc_x = smooth_accel.x();
    acc_y = smooth_accel.y();

    ss.str(""); ss.clear();
    ss << acc_x;
    accel_bias_x_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << acc_y;
    accel_bias_y_entry->set_text(ss.str());

    ss.str(""); ss.clear();
    ss << acc_z;
    accel_bias_z_entry->set_text(ss.str());
}

void AhrsCal::on_ok_clicked()
{
    new_cal_done = true;
    Gtk::Window::hide();
}

bool AhrsCal::got_new_cal_data()
{
    if(new_cal_done)
    {
        new_cal_done = false;
        return true;
    }
    return false;
}

void AhrsCal::on_reset_clicked()
{
    mag_samples.clear();
    culled_samples.clear();

    graph_xy->redraw(MagDa::AXIS_TYPE::AXIS_XY);
    graph_xz->redraw(MagDa::AXIS_TYPE::AXIS_XZ);
    graph_yz->redraw(MagDa::AXIS_TYPE::AXIS_YZ);
}
