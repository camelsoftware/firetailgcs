#ifndef TERM_H_INCLUDED
#define TERM_H_INCLUDED

#include <gtkmm.h>
#include <sstream>
#include <fstream>
#include "serial/serial.h"

class TermConnectDlg : public Gtk::Dialog
{
public:
    TermConnectDlg();
	~TermConnectDlg();

    std::string get_address() { return addr_entry->get_text(); }
    int get_baud() { return atoi(baud_entry->get_text().c_str()); }
private:
    Gtk::Entry* addr_entry;
    Gtk::Entry* baud_entry;
};


class Terminal : public Gtk::Window
{
public:
    Terminal(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~Terminal() {}

    void show();

    void timeout();

    bool on_delete_event(GdkEventAny* event);

private:

    void send();

    Glib::RefPtr<Gtk::Builder> refBuilder;
    Gtk::Button* send_btn;
    Gtk::Entry* term_entry;

    Gtk::CheckButton* newline_check;

    Gtk::TextView* text_view;
    Glib::RefPtr<Gtk::TextBuffer> text_buffer;
    Glib::RefPtr<Gtk::TextTag> green_tag;
    Glib::RefPtr<Gtk::TextTag> blue_tag;

    serial::Serial sp;

};
#endif // TERM_H_INCLUDED
