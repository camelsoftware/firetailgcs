/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GCS_SETTINGS_H_INCLUDED
#define GCS_SETTINGS_H_INCLUDED

#include <gtkmm.h>

//this file contains functions to read and write settings and session stuff to a binary file
//eg. the last entered coms port for the terminal and things like that
//so the user doesn't have to enter the same port every time
//an tings like dis n dat


struct gcs_settings
{
    int coms_baud;
    char coms_port[120];
    int coms_udp_port;
    char coms_addr[120];

    char radio_port[120];
    int terminal_baud;
    char terminal_port[120];

    bool snd_mute;
    int snd_disconnect_vol;
    int snd_alarm_vol;
    int snd_buzzer_vol;
};

extern gcs_settings gcs_set;

void gcs_settings_init();
void gcs_settings_write();

#endif // GLOBALS_H_INCLUDED
