/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED


#include <gtkmm.h>
#include <memory>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <iostream>

#include "coms.h"
#include "tinyxml2.h"
#include "utility.h"


class LogWindow : public Gtk::Window
{
public:
    LogWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
	~LogWindow();

    void timeout();

    void show();

private:

    enum {
        PLAY_MODE_PLAY,
        PLAY_MODE_PAUSE,
        PLAY_MODE_FFWD,
        PLAY_MODE_REWIND
    } PlayMode;

    void open_log();
    void export_csv();

    void pause_toggled();
    void play_toggled();
    void ffwd_toggled();
    void rwd_toggled();

    void log_thread();
    void thread_loop();

    void on_slider_value_changed();

    bool on_delete_event(GdkEventAny* event);

    Glib::RefPtr<Gtk::Builder> refBuilder;

    Gtk::Label* file_label;
    Gtk::Button* open_button;
    Gtk::Button* export_button;

    Gtk::ToggleButton* pause_toggle;
    Gtk::ToggleButton* play_toggle;
    Gtk::ToggleButton* rewind_toggle;
    Gtk::ToggleButton* ffwd_toggle;

    Gtk::Scale* scale;
    Gtk::Label* total_time;
    Gtk::Label* current_time;

    std::vector< std::vector<std::string> > pack_vec;

    std::thread thr;
    std::mutex mute;

    bool thr_running; //true if thread is running
    bool thr_run; //set to false to kill the thread

    int entry_pos;

    bool ignore_slider_changed = false;
    bool mouse_down = false;
};


#endif // LOG_H_INCLUDED
