/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "sound_window.h"
#include "utility.h"
#include "gcs_settings.h"

SoundWindow::SoundWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("snd_mute_check", mute_check);

    refBuilder->get_widget("snd_disconnect_volume", disconnect_vol);
    disconnect_vol->set_adjustment(Gtk::Adjustment::create(gcs_set.snd_disconnect_vol, 1, 100, 1, 5, 0));

    refBuilder->get_widget("snd_alarm_volume", alarm_vol);
    alarm_vol->set_adjustment(Gtk::Adjustment::create(gcs_set.snd_alarm_vol, 1, 100, 1, 5, 0));

    refBuilder->get_widget("snd_buzzer_volume", buzzer_vol);
    buzzer_vol->set_adjustment(Gtk::Adjustment::create(gcs_set.snd_buzzer_vol, 1, 100, 1, 5, 0));

    mute_check->signal_clicked().connect(sigc::mem_fun(*this, &SoundWindow::on_mute));
    disconnect_vol->signal_value_changed().connect(sigc::mem_fun(*this, &SoundWindow::on_vol_changed));
    alarm_vol->signal_value_changed().connect(sigc::mem_fun(*this, &SoundWindow::on_vol_changed));
    buzzer_vol->signal_value_changed().connect(sigc::mem_fun(*this, &SoundWindow::on_vol_changed));

    Gtk::Button* ok_button;
    refBuilder->get_widget("snd_ok_button", ok_button);
    ok_button->signal_clicked().connect(sigc::mem_fun(*this, &Gtk::Window::hide));

    std::string dtone_file;

#ifdef _WIN32
    dtone_file = Glib::get_current_dir();
    dtone_file += "\\share\\nolink.wav";
#else
    dtone_file = share_uri;
    dtone_file += "nolink.ogg";
#endif // _WIN32


    if(!dtone_buffer.create_from_file(dtone_file))
    {
        Gtk::MessageDialog msg(*this, "Couldn't load audio!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        std::string wmsg;
        wmsg = "An audio file could not be loaded. The path should be ";
        wmsg += dtone_file;
        msg.set_secondary_text(wmsg);
        msg.run();
    }

    disconnect_tone.set_buffer(&dtone_buffer);
    disconnect_tone.set_loop(true);


#ifdef _WIN32
    dtone_file = Glib::get_current_dir();
    dtone_file += "\\share\\alarm.wav";
#else
    dtone_file = share_uri;
    dtone_file += "alarm.ogg";
#endif // _WIN32
    if(!alarm_buffer.create_from_file(dtone_file))
    {
        Gtk::MessageDialog msg(*this, "Couldn't load audio!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        std::string wmsg;
        wmsg = "An audio file could not be loaded. The path should be ";
        wmsg += dtone_file;
        msg.set_secondary_text(wmsg);
        msg.run();
    }

    alarm_sound.set_buffer(&alarm_buffer);
    alarm_sound.set_loop(false);

#ifdef _WIN32
    dtone_file = Glib::get_current_dir();
    dtone_file += "\\share\\buzzer.wav";
#else
    dtone_file = share_uri;
    dtone_file += "buzzer.ogg";
#endif // _WIN32

    if(!buzzer_buffer.create_from_file(dtone_file))
    {
        Gtk::MessageDialog msg(*this, "Couldn't load audio!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        std::string wmsg;
        wmsg = "An audio file could not be loaded. The path should be ";
        wmsg += dtone_file;
        msg.set_secondary_text(wmsg);
        msg.run();
    }

    buzzer_sound.set_buffer(&buzzer_buffer);
    buzzer_sound.set_loop(false);

    disconnect_tone.set_volume(gcs_set.snd_disconnect_vol/100.0);
    alarm_sound.set_volume(gcs_set.snd_alarm_vol/100.0);
    buzzer_sound.set_volume(gcs_set.snd_buzzer_vol/100.0);

    if(gcs_set.snd_mute)
    {
        mute_check->set_active(true);
    }
}

SoundWindow::~SoundWindow()
{

}

void SoundWindow::on_mute()
{
    if(mute_check->get_active())
    {
        disconnect_tone.set_volume(0);
        alarm_sound.set_volume(0);
        buzzer_sound.set_volume(0);

        gcs_set.snd_mute = true;
    }
    else
    {
        disconnect_tone.set_volume(disconnect_vol->get_value()/100.0);
        alarm_sound.set_volume(alarm_vol->get_value()/100.0);
        buzzer_sound.set_volume(buzzer_vol->get_value()/100.0);
        gcs_set.snd_mute = false;
    }
    gcs_settings_write();
}

void SoundWindow::on_vol_changed(double v)
{
    if(mute_check->get_active())
    {
        disconnect_tone.set_volume(0);
        alarm_sound.set_volume(0);
        buzzer_sound.set_volume(0);
    }
    else
    {
        disconnect_tone.set_volume(disconnect_vol->get_value()/100.0);
        alarm_sound.set_volume(alarm_vol->get_value()/100.0);
        buzzer_sound.set_volume(buzzer_vol->get_value()/100.0);
    }

    gcs_set.snd_disconnect_vol = disconnect_vol->get_value();
    gcs_set.snd_alarm_vol = alarm_vol->get_value();
    gcs_set.snd_buzzer_vol = buzzer_vol->get_value();
    gcs_settings_write();
}


