/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "utility.h"


#ifdef _WIN32
std::string share_uri = "share\\";
#else
#include <unistd.h>
std::string share_uri = "/usr/share/firetailgcs/";
#endif

//cross platform sleep function
void sleep_ms(int ms)
{
#ifndef _WIN32
    usleep(ms*1000);
#else
    Sleep(ms);
#endif
}

int roundf(double r)
{
    return (r > 0.0) ? (r + 0.5) : (r - 0.5);
}

double to_degrees(double in)
{
    return in*57.2957795131; //180/pi
}

double to_radians(double in)
{
    return in*0.01745329251;  //pi/180
}

Tokeniser::Tokeniser(const char* _str, char _token)
{
    str = _str;
    token = _token;
}


bool Tokeniser::next(char* out, int len)
{
    int count = 0;

    if(str[0] == 0)
        return false;

    while(true)
    {
        if(str[count] == '\0')
        {
            out[count] = '\0';
            str = &str[count];
            return true;
        }

        if(str[count] == token)
        {
            out[count] = '\0';
            count++;
            str = &str[count];
            return true;
        }

        if(count < len)
            out[count] = str[count];

        count++;
    }
    return false;
}



coord make_coord(int32_t lat, int32_t lng)
{
    coord ret;
    ret.lat = lat/1000000.0;
    ret.lng = lng/1000000.0;
    return ret;
}

coord make_coordf(float lat, float lng)
{
    coord ret;
    ret.lat = lat;
    ret.lng = lng;
    return ret;
}

float bearing_from_to(coord from, coord to)
{
    //1 = from, 2 = to
    //Unnamed but amazing mathematics from
    //http://www.movable-type.co.uk/scripts/latlong.html

    //convert to radians firstly
    from.lat = to_radians(from.lat);
    from.lng = to_radians(from.lng);

    to.lat = to_radians(to.lat);
    to.lng = to_radians(to.lng);

    float dLon = to.lng - from.lng;
    float y = sin(dLon) * cos(to.lat);
    float x = cos(from.lat)*sin(to.lat) -
        sin(from.lat)*cos(to.lat)*cos(dLon);
    return to_degrees(atan2(y, x));
}

float distance_between(coord a, coord b)
{
    //The Spherical Law Of Cosines
    //ref: http://www.movable-type.co.uk/scripts/latlong.html

    a.lat = to_radians(a.lat); //convert to radians
    a.lng = to_radians(a.lng);

    b.lat = to_radians(b.lat);
    b.lng = to_radians(b.lng);

    //this functions multiplies the result by 1000 to get the answer in meters, rather than kilometers
    return acos(sin(a.lat)*sin(b.lat) + cos(a.lat)*cos(b.lat)*cos(b.lng-a.lng)) * 6371 * 1000;
}

float cross_track_distance(coord from, coord to, coord here)
{
    //Cross track distance function, also from
    //http://www.moveable-type.co.uk/scripts/latlong.html

    float d13, brng13, brng12;
    d13 = distance_between(from, here);
    brng13 = to_radians(bearing_from_to(from, here));
    brng12 = to_radians(bearing_from_to(from, to));
    int R = 6371 * 1000; //radius of the world in meters
    return asin(sin(d13/R)*sin(brng13-brng12)) * R;
}

std::string make_padded_string(std::string start, std::string finish, char padding, int length)
{
    std::string ret = start;
    ret.resize((length-finish.length()), padding);
    ret += finish;
    return ret;
}

double CCW(imu::Vector<2> p1, imu::Vector<2> p2, imu::Vector<2> p3)
{
  double a = p1.y(); double b = p1.x();
  double c = p2.y(); double d = p2.x();
  double e = p3.y(); double f = p3.x();
  return (f - b) * (c - a) > (d - b) * (e - a);
}

//reads a string of hex characters like this
// "FD 05 8D"
//and packs a c string full of their actual values
//so out[0] would be 0xFD, out[1] == 0x05 and so on
std::string hex_to_string(std::string in)
{
	std::string out;
	int count = 0;
	unsigned short c;
	std::istringstream ins(in);
	ins >> std::hex;

	while((ins >> c) && (count < 38))
	{
	    out.append((char*)&c, 1);
		count++;
	}
	return out;
}

std::string alarm_action_to_string(Firelink::ALARM_ACTION ac)
{
    switch(ac)
    {
    case Firelink::ALARM_ACTION_NONE:
        return "None";
    break;
    case Firelink::ALARM_ACTION_LOCKUP:
        return "Lock flight controls";
    break;
    case Firelink::ALARM_ACTION_NO_PWR_GLIDE:
        return "Motor off glide";
    break;
    case Firelink::ALARM_ACTION_FIXED_BANK_LOITER:
        return "Fixed bank loiter";
    break;
    case Firelink::ALARM_ACTION_RTB:
        return "Return to base";
    break;
    case Firelink::ALARM_ACTION_LOITER:
        return "Loiter";
    break;
    case Firelink::ALARM_ACTION_MANUAL_CTRL:
        return "Manual control";
    break;
    }
    return "";
}

Firelink::ALARM_ACTION string_to_alarm_action(std::string stac)
{
    if(stac == "None")
        return Firelink::ALARM_ACTION_NONE;
    if(stac == "Lock flight controls")
        return Firelink::ALARM_ACTION_LOCKUP;
    if(stac == "Motor off glide")
        return Firelink::ALARM_ACTION_NO_PWR_GLIDE;
    if(stac == "Fixed bank loiter")
        return Firelink::ALARM_ACTION_FIXED_BANK_LOITER;
    if(stac == "Return to base")
        return Firelink::ALARM_ACTION_RTB;
    if(stac == "Loiter")
        return Firelink::ALARM_ACTION_LOITER;
    if(stac == "Manual control")
        return Firelink::ALARM_ACTION_MANUAL_CTRL;

    return Firelink::ALARM_ACTION_NONE;
}


uint8_t read_bit(uint8_t bte, uint8_t n)
{
    bool on = bte & (1 << (n-1));

    return on;
}

uint8_t set_bit(uint8_t bte, uint8_t n, bool on)
{
    uint8_t ret = bte;
    if(on)
        ret |= 1 << (n-1);
    else
        ret &= ~(1 << (n-1));

    return ret;
}


const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}
