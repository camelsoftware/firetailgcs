/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COMS_H_INCLUDED
#define COMS_H_INCLUDED

#include <fstream>
#include <mutex>
#include <string>
#include <ctime>
#include <memory>
#include <vector>

#include "logger.h"
#include "serial/serial.h"
#include "udp.h"
#include "FireLink/firelink.h"
#include "utility.h"

#include <iostream>

using namespace std;


enum Coms_Method
{
    COMS_SERIAL,
    COMS_UDP,
    COMS_LOG_PLAYBACK,
    COMS_IDLE
};


class Coms : public Firelink::Station
{
    public:
        Coms();
        ~Coms();

        static shared_ptr<Coms> add_new_connection();
        static void remove_connection(int n);
        static shared_ptr<Coms> get_instance(int n);
        static shared_ptr<Coms> get_active_instance();
        static int get_n_connections();
        static void set_active_connection(int n);
        static int get_active_connection();

        void lock();
        void unlock();

        bool start();
        void stop(); //function to stop thread. blocks until thread exits

        bool running();

        int connection_strength();

        bool wait_for_dispatch(uint16_t pid);
        bool wait_for_message(uint16_t pid);

        void push_log_pack(std::string pk);

        float get_heading();
        float get_pitch();
        float get_roll();
        float get_ap_pitch();
        float get_ap_roll();
        float get_ap_heading();
        float get_alt();
        float get_vert_speed();
        float get_ap_alt();
        float get_air_speed();
        float get_latitude();
        float get_longitude();
        int get_satellites_used();
        Firelink::AP_MODE get_ap_mode();
        bool get_autothrottle_on();
        bool get_autolaunch_on();
        bool get_motor_armed();
        bool get_has_rc_signal();
        bool get_is_inside_geofence();
        Firelink::vs_raw_marg get_raw_marg();
        Firelink::vs_raw_pwm get_raw_pwm();
        int get_ap_waypoint();
        float get_ground_speed();
        float get_ground_track();
        Firelink::GPS_LOCK get_gps_mode();
        uint32_t get_timer();
        float get_temperature();
        int get_pwm_inputs(int c);
        float get_ap_waypoint_lat();
        float get_ap_waypoint_lng();
        int get_power_remaining();
        float get_amps();
        float get_volts();

        float get_main_avion_volts();
        float get_stby_avion_volts();

        std::string get_vehicle_title();

        bool have_new_instrument_settings();
        float get_altimeter_setting();
        float get_ground_altitude();
        float get_mag_var_setting();
        void reset_inst_settings_flag();

        void send_script_message(char* msg);

        //a place to store received satellite reports
        std::array<Firelink::m_satellites, 12> sat_reports;

        float home_lat, home_lng, home_alt;

        std::string address;
        int baudport;
        Coms_Method m;

        //returns number of alerts that have been received
        int get_n_alarms();
        //returns the next alert
        std::string get_next_alarm();

        std::vector<waypoint> plan_points;
        std::vector<waypoint> fence_points;

        Firelink::m_sensing_status sense_status;

        int get_n_sensors();

        int last_plan_point_recvd() { return last_plan_point; }
        int last_fence_point_recvd() { return last_fence_point; }
        void reset_last_points() { last_fence_point = last_plan_point = 0; }


        int id;

    private:
        Coms( const Coms &obj) {}
        Logger logger;

        void ComsThread();
        std::thread thr;
        std::mutex mute;

        std::string vehicle_name;
        Firelink::vs_inst inst;
        Firelink::vs_systems systems;
        Firelink::vs_systems_health sys_health;
        Firelink::vs_ap_status ap_status;
        Firelink::vs_raw_marg marg;
        Firelink::vs_raw_pwm pwm;

        int n_sensors;
        int last_plan_point;
        int last_fence_point;

        bool on_recv_satellites(uint8_t* data);

        bool on_recv_home_coordinates(uint8_t* data);

        bool on_recv_alarms(uint8_t* data);

        bool on_recv_n_points(uint8_t* data);
        bool on_recv_point(uint8_t* data);

        bool on_recv_debug_msg(uint8_t* data);

        bool on_recv_sensing_status(uint8_t* data);

        bool on_recv_vehicle_name(uint8_t* data);

        bool on_recv_instrument_settings(uint8_t* data);

        bool on_recv_script_msg(uint8_t* data);

        bool serial();
        bool udp();

        serial::Serial sp;
        shared_ptr<UDPSocket> sock;

        const int keep_alive_seconds = 3;
        time_t keep_alive_time; //time of last keep-alive
        int pack_count = 0;
        int last_pack_count;

        bool keep_going;
        bool no_send;

        bool new_inst_settings;
        float inst_setting_alt_setting;
        float inst_setting_gnd_ref;
        float inst_setting_mag_var;

        std::vector<std::string> log_packs;
        std::vector<std::string> alarm_msgs;
        std::vector<std::string> script_msgs;
};


#endif // COMS_H_INCLUDED
