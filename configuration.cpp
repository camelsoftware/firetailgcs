/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "configuration.h"

#include <bitset>



SettingDialog::SettingDialog(std::string setting)
{
    Gtk::Label* lbl = Gtk::manage(new Gtk::Label());
    val_entry = Gtk::manage(new Gtk::Entry());

    std::string lbltxt = "New value for: ";
    lbltxt += setting;
    lbl->set_text(lbltxt);
    Gtk::HBox* box = Gtk::manage(new Gtk::HBox());

    box->pack_start(*lbl);
    box->pack_start(*val_entry);

    this->get_vbox()->pack_start(*box);
    this->show_all();
    this->set_modal(true);

    std::string iconpath;
    iconpath = share_uri;
#ifdef _WIN32
    iconpath += "firetail.ico";
#else
    iconpath += "firetail.png";
#endif // _WIN32
    this->set_icon_from_file(iconpath);

    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    Gtk::Button* okbtn = add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);
    okbtn->set_can_default();
    okbtn->grab_focus();
    set_default(*okbtn);
}


Config::Config(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    //ahrs cal stuff
    refBuilder->get_widget_derived("ahrs_cal_window", ahrs_cal);

    //control setup
    refBuilder->get_widget_derived("control_setup", control_setup);
    control_setup->set_transient_for(*this);

    refBuilder->get_widget_derived("mixer_uploader_window", mixer_uploader);
    mixer_uploader->set_transient_for(*this);

    refBuilder->get_widget("accel_bias_x", accel_bias_x);
    refBuilder->get_widget("accel_bias_y", accel_bias_y);
    refBuilder->get_widget("accel_bias_z", accel_bias_z);

    refBuilder->get_widget("mag_bias_x", mag_bias_x);
    refBuilder->get_widget("mag_bias_y", mag_bias_y);
    refBuilder->get_widget("mag_bias_z", mag_bias_z);

    refBuilder->get_widget("mag_gain_x", mag_gain_x);
    refBuilder->get_widget("mag_gain_y", mag_gain_y);
    refBuilder->get_widget("mag_gain_z", mag_gain_z);

    refBuilder->get_widget("begin_cal_button", begin_cal_button);
    refBuilder->get_widget("download_ahrs_cal_button", download_ahrs_cal_btn);
    refBuilder->get_widget("upload_ahrs_cal_button", upload_ahrs_cal_btn);

    begin_cal_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_begin_ahrs_cal_clicked));
    download_ahrs_cal_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_ahrs_cal_clicked));
    upload_ahrs_cal_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_ahrs_cal_clicked));


    //Control config page
    refBuilder->get_widget("servo_rc_signal_scale_spinbutton", rc_signal_scale);

    rc_signal_scale->set_digits(2);
    rc_signal_scale->set_range(0, 3.0);
    rc_signal_scale->set_value(1.0);
    rc_signal_scale->set_increments(0.05, 1.0);


    refBuilder->get_widget("aileron_min_scale", aileron_min_scale);
    aileron_min_scale->set_digits(1);
    aileron_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("aileron_max_scale", aileron_max_scale);
    aileron_max_scale->set_digits(1);
    aileron_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));
    refBuilder->get_widget("elevator_min_scale", elevator_min_scale);
    elevator_min_scale->set_digits(1);
    elevator_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("elevator_max_scale", elevator_max_scale);
    elevator_max_scale->set_digits(1);
    elevator_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));
    refBuilder->get_widget("rudder_min_scale", rudder_min_scale);
    rudder_min_scale->set_digits(1);
    rudder_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("rudder_max_scale", rudder_max_scale);
    rudder_max_scale->set_digits(1);
    rudder_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));
    refBuilder->get_widget("throttle_min_scale", throttle_min_scale);
    throttle_min_scale->set_digits(1);
    throttle_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("throttle_max_scale", throttle_max_scale);
    throttle_max_scale->set_digits(1);
    throttle_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));

    refBuilder->get_widget("ail_pwm_rev", ail_pwm_rev);
    refBuilder->get_widget("ail_ap_rev", ail_ap_rev);
    refBuilder->get_widget("ail_servo_rev", ail_servo_rev);
    refBuilder->get_widget("ele_pwm_rev", ele_pwm_rev);
    refBuilder->get_widget("ele_ap_rev", ele_ap_rev);
    refBuilder->get_widget("ele_servo_rev", ele_servo_rev);
    refBuilder->get_widget("rud_pwm_rev", rud_pwm_rev);
    refBuilder->get_widget("rud_ap_rev", rud_ap_rev);
    refBuilder->get_widget("rud_servo_rev", rud_servo_rev);
    refBuilder->get_widget("thr_pwm_rev", thr_pwm_rev);
    refBuilder->get_widget("thr_ap_rev", thr_ap_rev);
    refBuilder->get_widget("thr_servo_rev", thr_servo_rev);


    refBuilder->get_widget("configure_servo_button", config_servo_button);
    refBuilder->get_widget("download_servo_config_button", download_servo_config_button);
    refBuilder->get_widget("upload_servo_config_button", upload_servo_config_button);

    config_servo_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_setup_servo_clicked));
    download_servo_config_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_servo_config));
    upload_servo_config_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_servo_config));

    refBuilder->get_widget("mixer_config_combotext", mixer_config_combotext);
    refBuilder->get_widget("mixer_ratio_spinbutton", mixer_ratio_spinbutton);
    //mixer_config_combotext->set_active(0);
    mixer_ratio_spinbutton->set_range(0, 100);
    mixer_ratio_spinbutton->set_value(50);
    mixer_ratio_spinbutton->set_increments(1, 5);
    mixer_ratio_spinbutton->set_digits(0);

    refBuilder->get_widget("mixer_upload_file", mixer_upload_button);
    refBuilder->get_widget("mixer_config_download_button", mixer_config_download_button);
    refBuilder->get_widget("mixer_config_upload_button", mixer_config_upload_button);
    mixer_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_mixer_file));
    mixer_config_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_mixer_config));
    mixer_config_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_mixer_config));


    refBuilder->get_widget("mode_select_channel", mode_select_channel);
    refBuilder->get_widget("mode_select_mode", mode_select_mode);
    mode_select_channel->set_active(0);
    mode_select_mode->set_active(0);

    refBuilder->get_widget("mode_select_download_button", mode_select_download_button);
    refBuilder->get_widget("mode_select_upload_button", mode_select_upload_button);

    mode_select_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_mode_select));
    mode_select_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_mode_select));


    //autopilot stuff
    refBuilder->get_widget("ap_settings_treeview", ap_treeview);

    m_refTreeModel = Gtk::TreeStore::create(ap_columns);
    ap_treeview->set_model(m_refTreeModel);

    Gtk::TreeModel::Row row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "PNR";

    Gtk::TreeModel::Row childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Pitch Rate P";
    childrow[ap_columns.col_description] = "pitch rate P gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Pitch Rate I";
    childrow[ap_columns.col_description] = "pitch rate I gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Pitch Rate D";
    childrow[ap_columns.col_description] = "pitch rate D gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;


    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Roll Rate P";
    childrow[ap_columns.col_description] = "roll rate P gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Roll Rate I";
    childrow[ap_columns.col_description] = "roll rate I gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Roll Rate D";
    childrow[ap_columns.col_description] = "roll rate D gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Pitch Rate";
    childrow[ap_columns.col_description] = "degrees per second (1 - 180)";
    childrow[ap_columns.col_value] = 15;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Roll Rate";
    childrow[ap_columns.col_description] = "degrees per second (1 - 180)";
    childrow[ap_columns.col_value] = 60;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Pitch P";
    childrow[ap_columns.col_description] = "pitch proportional gain (1 - 10000)";
    childrow[ap_columns.col_value] = 20;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Pitch I";
    childrow[ap_columns.col_description] = "pitch integral gain (1 - 10000)";
    childrow[ap_columns.col_value] = 5;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Roll P";
    childrow[ap_columns.col_description] = "roll proportional gain (1 - 10000)";
    childrow[ap_columns.col_value] = 20;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Roll I";
    childrow[ap_columns.col_description] = "roll integral gain (1 - 10000)";
    childrow[ap_columns.col_value] = 5;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Pitch Up";
    childrow[ap_columns.col_description] = "degrees (5 - 85)";
    childrow[ap_columns.col_value] = 15;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Pitch Down";
    childrow[ap_columns.col_description] = "degrees (5 - 85)";
    childrow[ap_columns.col_value] = 25;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Bank Angle";
    childrow[ap_columns.col_description] = "degrees (15 - 85)";
    childrow[ap_columns.col_value] = 55;


    row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "YD & Coordination";

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "YD P Gain";
    childrow[ap_columns.col_description] = "yaw damper P gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "YD I Gain";
    childrow[ap_columns.col_description] = "yaw damper I gain (1 - 10000)";
    childrow[ap_columns.col_value] = 1;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "YD D Gain";
    childrow[ap_columns.col_description] = "yaw damper D gain (1 - 10000)";
    childrow[ap_columns.col_value] = 1;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Yaw Turn Gain";
    childrow[ap_columns.col_description] = "P gain for yaw channel turn coordination";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Pitch Bank Feedforward";
    childrow[ap_columns.col_description] = "P gain for pitch channel turn coordination";
    childrow[ap_columns.col_value] = 100;



    row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "HNA";

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Heading P Gain";
    childrow[ap_columns.col_description] = "heading P gain (1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Heading I Gain";
    childrow[ap_columns.col_description] = "heading I gain (1 - 1000)";
    childrow[ap_columns.col_value] = 55;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Heading D Gain";
    childrow[ap_columns.col_description] = "heading D gain (1 - 1000)";
    childrow[ap_columns.col_value] = 55;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "VS P Gain";
    childrow[ap_columns.col_description] = "Vertical speed P gain (1 - 1000)";
    childrow[ap_columns.col_value] = 55;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "VS I Gain";
    childrow[ap_columns.col_description] = "Vertical speed I gain (1 - 1000)";
    childrow[ap_columns.col_value] = 55;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "VS D Gain";
    childrow[ap_columns.col_description] = "Vertical speed D gain (1 - 1000)";
    childrow[ap_columns.col_value] = 55;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Alt Gain";
    childrow[ap_columns.col_description] = "Altitude gain (1 - 1000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Descent Rate";
    childrow[ap_columns.col_description] = "feet per second (1 - 100)";
    childrow[ap_columns.col_value] = 8;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max Climb Rate";
    childrow[ap_columns.col_description] = "feet per second (1 - 100)";
    childrow[ap_columns.col_value] = 3;


    row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "Path following";

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "XTrack Gain";
    childrow[ap_columns.col_description] = "(100 - 5000)";
    childrow[ap_columns.col_value] = 1000;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Loiter Radius";
    childrow[ap_columns.col_description] = "meters (10 - 1000)";
    childrow[ap_columns.col_value] = 120;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Loiter Gain";
    childrow[ap_columns.col_description] = "meters (1 - 100)";
    childrow[ap_columns.col_value] = 10;


    row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "Autothrottle";

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "AT P Gain";
    childrow[ap_columns.col_description] = "(1 - 10000)";
    childrow[ap_columns.col_value] = 1000;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "AT I Gain";
    childrow[ap_columns.col_description] = "(1 - 10000)";
    childrow[ap_columns.col_value] = 100;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "AT D Gain";
    childrow[ap_columns.col_description] = "(1 - 10000)";
    childrow[ap_columns.col_value] = 50;


    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "VS Feedforward";
    childrow[ap_columns.col_description] = "Vertical speed feedforward. \nThis should be inversely proportional to thrust/weight ratio.\n"
                                           "Slow & heavy planes would benefit from lots of feedforward.";
    childrow[ap_columns.col_value] = 50;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Cruise Throttle Setting";
    childrow[ap_columns.col_description] = "Throttle setting normally required for this aircraft \nduring straight and level flight. (0 - 100)";
    childrow[ap_columns.col_value] = 75;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Cruise Ground Speed";
    childrow[ap_columns.col_description] = "Default ground speed in km/h. (10 - 1000)";
    childrow[ap_columns.col_value] = 65;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Min Throttle";
    childrow[ap_columns.col_description] = "Lowest allowable throttle setting for autothrottle (0 - 100)";
    childrow[ap_columns.col_value] = 35;


    row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "Steering";

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Steering Outer Gain";
    childrow[ap_columns.col_description] = "";
    childrow[ap_columns.col_value] = 0;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Steering Inner Gain";
    childrow[ap_columns.col_description] = "";
    childrow[ap_columns.col_value] = 0;


    row = *(m_refTreeModel->append());
    row[ap_columns.col_column] = "Auto Takeoff";

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Launch type";
    childrow[ap_columns.col_description] = "0 - runway, 1 - bungee, 2 - handlaunch";
    childrow[ap_columns.col_value] = 0;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Max roll distance";
    childrow[ap_columns.col_description] = "meters";
    childrow[ap_columns.col_value] = 50;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Runway width";
    childrow[ap_columns.col_description] = "meters";
    childrow[ap_columns.col_value] = 10;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Takeoff throttle";
    childrow[ap_columns.col_description] = "%";
    childrow[ap_columns.col_value] = 95;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Rotate speed";
    childrow[ap_columns.col_description] = "km/h";
    childrow[ap_columns.col_value] = 45;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Climb out vert speed";
    childrow[ap_columns.col_description] = "feet per second";
    childrow[ap_columns.col_value] = 6;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Takeoff disconnect alt";
    childrow[ap_columns.col_description] = "feet above ground";
    childrow[ap_columns.col_value] = 150;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Bungee detect accel";
    childrow[ap_columns.col_description] = "milli-g forces required to activate launch";
    childrow[ap_columns.col_value] = 1800;

    childrow = *(m_refTreeModel->append(row.children()));
    childrow[ap_columns.col_setting] = "Bungee launch delay";
    childrow[ap_columns.col_description] = "after bungee launch detection\nthe AP will wait this number of milliseconds\nbefore starting motor";
    childrow[ap_columns.col_value] = 750;

    ap_treeview->append_column("Category", ap_columns.col_column);
    ap_treeview->append_column("Setting", ap_columns.col_setting);
    ap_treeview->append_column("Description", ap_columns.col_description);
    ap_treeview->append_column("Value", ap_columns.col_value);


    ap_treeview->signal_row_activated().connect(sigc::mem_fun(*this,
            &Config::on_treeview_row_activated) );


    refBuilder->get_widget("geofence_alarm_combo", geofence_alarm_combo);
    refBuilder->get_widget("low_bat_alarm_combo", low_bat_alarm_combo);
    refBuilder->get_widget("no_gps_alarm_combo", no_gps_alarm_combo);
    refBuilder->get_widget("no_control_alarm_combo", no_ctrl_alarm_combo);
    refBuilder->get_widget("no_datalink_alarm_combo", no_datalink_alarm_combo);


    geofence_alarm_combo->set_active(0);
    low_bat_alarm_combo->set_active(0);
    no_gps_alarm_combo->set_active(0);
    no_ctrl_alarm_combo->set_active(0);
    no_datalink_alarm_combo->set_active(0);


    refBuilder->get_widget("download_alarms_button", alarm_download_btn);
    refBuilder->get_widget("upload_alarms_button", alarm_upload_btn);
    alarm_download_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_alarm_download));
    alarm_upload_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_alarm_upload));

//loading and saving to files
    refBuilder->get_widget("open_settings_button", load_config_btn);
    refBuilder->get_widget("save_settings_button", save_config_btn);
    refBuilder->get_widget("edit_setting_button", edit_setting_btn);
    refBuilder->get_widget("refresh_settings_button", refresh_settings_btn);
    refBuilder->get_widget("upload_settings_button", upload_all_settings_btn);
    load_config_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::load_config));
    save_config_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::save_config));
    edit_setting_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_edit_setting));
    refresh_settings_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_refresh_settings));
    upload_all_settings_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_all_settings));

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);



}

Config::~Config()
{

}


void Config::show()
{
    if(!download_at_settings())
    {
        cout << "couldn't download at settings" << endl;
        goto failed;
    }
    if(!download_hna())
    {
        cout << "couldn't download hna settings" << endl;
        goto failed;
    }
    if(!download_max_angles())
    {
        cout << "couldn't download max angle settings" << endl;
        goto failed;
    }
    if(!download_max_vs())
    {
        cout << "couldn't download vs settings" << endl;
        goto failed;
    }
    if(!download_path_pid())
    {
        cout << "couldn't download path settings" << endl;
        goto failed;
    }
    if(!download_pitch_roll_rates())
    {
        cout << "couldn't download pitch roll rates settings" << endl;
        goto failed;
    }
    if(!download_pnr())
    {
        cout << "couldn't download pnr settings" << endl;
        goto failed;
    }
    if(!download_yd_tc_settings())
    {
        cout << "couldn't download yd tc setting" << endl;
        goto failed;
    }
    if(!download_takeoff_settings())
    {
        cout << "couldn't download takeoff settings" << endl;
        goto failed;
    }
    if(!download_steering_gains())
    {
        cout << "couldn't download steering gains" << endl;
        goto failed;
    }

    Gtk::Window::show();
    return;

    failed:
        Gtk::MessageDialog mb("Failed to synchronise with vehicle! Be very careful when changing autopilot settings.");
        mb.run();
        Gtk::Window::show();
}


void Config::upload_setting_from_iter(Gtk::TreeModel::iterator iter)
{
    if(iter)
    {
        Gtk::TreeModel::Row row = *iter;
        Glib::ustring val = row[ap_columns.col_setting];

        if(val == "")
            return;

        title_to_val(val);

        SettingDialog sd(val);
        if(sd.run() != Gtk::RESPONSE_OK)
            return;

        std::string new_val = sd.val_entry->get_text();
        for(int i = 0; i < new_val.length(); i++)
        {
            if((new_val[i] < '0') || (new_val[i] > '9'))
            {
                Gtk::MessageDialog msg("Bad value!");
                msg.run();
                return;
            }
        }

        int ival = atoi(new_val.c_str());

        if(val == "Pitch Rate P")
        {
            upload_pitch_roll_rates(ival, title_to_val("Pitch Rate I"), title_to_val("Pitch Rate D"),
                                    title_to_val("Roll Rate P"), title_to_val("Roll Rate I"), title_to_val("Roll Rate D"));
        }
        if(val == "Pitch Rate I")
        {
            upload_pitch_roll_rates(title_to_val("Pitch Rate P"), ival, title_to_val("Pitch Rate D"),
                                    title_to_val("Roll Rate P"), title_to_val("Roll Rate I"), title_to_val("Roll Rate D"));
        }
        if(val == "Pitch Rate D")
        {
            upload_pitch_roll_rates(title_to_val("Pitch Rate P"), title_to_val("Pitch Rate I"),  ival,
                                    title_to_val("Roll Rate P"), title_to_val("Roll Rate I"), title_to_val("Roll Rate D"));
        }
        if(val == "Roll Rate P")
        {
            upload_pitch_roll_rates(title_to_val("Pitch Rate P"), title_to_val("Pitch Rate I"), title_to_val("Pitch Rate D"),
                                    ival, title_to_val("Roll Rate I"), title_to_val("Roll Rate D"));
        }
        if(val == "Roll Rate I")
        {
            upload_pitch_roll_rates(title_to_val("Pitch Rate P"), title_to_val("Pitch Rate I"), title_to_val("Pitch Rate D"),
                                    title_to_val("Roll Rate P"), ival, title_to_val("Roll Rate D"));
        }
        if(val == "Roll Rate D")
        {
            upload_pitch_roll_rates(title_to_val("Pitch Rate P"), title_to_val("Pitch Rate I"), title_to_val("Pitch Rate D"),
                                    title_to_val("Roll Rate P"), title_to_val("Roll Rate I"), ival);
        }

        if(val == "Max Pitch Rate")
        {
            upload_max_angles(title_to_val("Max Pitch Up"), title_to_val("Max Pitch Down"), title_to_val("Max Bank Angle"),
                              ival, title_to_val("Max Roll Rate"));
        }
        if(val == "Max Roll Rate")
        {
            upload_max_angles(title_to_val("Max Pitch Up"), title_to_val("Max Pitch Down"), title_to_val("Max Bank Angle"),
                              title_to_val("Max Pitch Rate"), ival);
        }
        if(val == "Max Pitch Up")
        {
            upload_max_angles(ival, title_to_val("Max Pitch Down"), title_to_val("Max Bank Angle"),
                              title_to_val("Max Pitch Rate"), title_to_val("Max Roll Rate"));
        }
        if(val == "Max Pitch Down")
        {
            upload_max_angles(title_to_val("Max Pitch Up"), ival, title_to_val("Max Bank Angle"),
                              title_to_val("Max Pitch Rate"), title_to_val("Max Roll Rate"));
        }
        if(val == "Max Bank Angle")
        {
            upload_max_angles(title_to_val("Max Pitch Up"), title_to_val("Max Pitch Down"), ival,
                              title_to_val("Max Pitch Rate"), title_to_val("Max Roll Rate"));
        }

        if(val == "Pitch P")
        {
            upload_pnr(ival, title_to_val("Pitch I"), 0, title_to_val("Roll P"), title_to_val("Roll I"), 0);
        }
        if(val == "Pitch I")
        {
            upload_pnr(title_to_val("Pitch P"), ival, 0, title_to_val("Roll P"), title_to_val("Roll I"), 0);
        }
        if(val == "Roll P")
        {
            upload_pnr(title_to_val("Pitch P"), title_to_val("Pitch I"), 0, ival, title_to_val("Roll I"), 0);
        }
        if(val == "Roll I")
        {
            upload_pnr(title_to_val("Pitch P"), title_to_val("Pitch I"), 0, title_to_val("Roll P"), ival, 0);
        }


        if(val == "Heading P Gain")
        {
            upload_hna(ival, title_to_val("Heading I Gain"), title_to_val("Heading D Gain"),
                       title_to_val("VS P Gain"), title_to_val("VS I Gain"), title_to_val("VS D Gain"),
                       title_to_val("Alt Gain"));
        }
        if(val == "Heading I Gain")
        {
            upload_hna(title_to_val("Heading P Gain"), ival, title_to_val("Heading D Gain"),
                       title_to_val("VS P Gain"), title_to_val("VS I Gain"), title_to_val("VS D Gain"),
                       title_to_val("Alt Gain"));
        }
        if(val == "Heading D Gain")
        {
            upload_hna(title_to_val("Heading P Gain"), title_to_val("Heading I Gain"), ival,
                       title_to_val("VS P Gain"), title_to_val("VS I Gain"), title_to_val("VS D Gain"),
                       title_to_val("Alt Gain"));
        }
        if(val == "VS P Gain")
        {
            upload_hna(title_to_val("Heading P Gain"), title_to_val("Heading I Gain"), title_to_val("Heading D Gain"),
                       ival, title_to_val("VS I Gain"), title_to_val("VS D Gain"),
                       title_to_val("Alt Gain"));
        }
        if(val == "VS I Gain")
        {
            upload_hna(title_to_val("Heading P Gain"), title_to_val("Heading I Gain"), title_to_val("Heading D Gain"),
                       title_to_val("VS P Gain"), ival, title_to_val("VS D Gain"),
                       title_to_val("Alt Gain"));
        }
        if(val == "VS D Gain")
        {
            upload_hna(title_to_val("Heading P Gain"), title_to_val("Heading I Gain"), title_to_val("Heading D Gain"),
                       title_to_val("VS P Gain"), title_to_val("VS I Gain"), ival,
                       title_to_val("Alt Gain"));
        }
        if(val == "Alt Gain")
        {
            upload_hna(title_to_val("Heading P Gain"), title_to_val("Heading I Gain"), title_to_val("Heading D Gain"),
                       title_to_val("VS P Gain"), title_to_val("VS I Gain"), title_to_val("VS D Gain"),
                       ival);
        }


        if(val == "Max Climb Rate")
        {
            upload_max_vs(ival, title_to_val("Max Descent Rate"));
        }
        if(val == "Max Descent Rate")
        {
            upload_max_vs(title_to_val("Max Climb Rate"), ival);
        }


        if(val == "XTrack Gain")
            upload_path_pid(ival, title_to_val("Loiter Radius"), title_to_val("Loiter Gain"));
        if(val == "Loiter Radius")
            upload_path_pid(title_to_val("XTrack Gain"), ival, title_to_val("Loiter Gain"));
        if(val == "Loiter Gain")
            upload_path_pid(title_to_val("XTrack Gain"), title_to_val("Loiter Radius"), ival);


        if(val == "AT P Gain")
            upload_at_settings(ival, title_to_val("AT I Gain"), title_to_val("AT D Gain"), title_to_val("VS Feedforward"),
                               title_to_val("Cruise Throttle Setting"), title_to_val("Cruise Ground Speed"), title_to_val("Min Throttle"));
        if(val == "AT I Gain")
            upload_at_settings(title_to_val("AT P Gain"), ival, title_to_val("AT D Gain"), title_to_val("VS Feedforward"),
                               title_to_val("Cruise Throttle Setting"), title_to_val("Cruise Ground Speed"), title_to_val("Min Throttle"));
        if(val == "AT D Gain")
            upload_at_settings(title_to_val("AT P Gain"), title_to_val("AT I Gain"), ival, title_to_val("VS Feedforward"),
                               title_to_val("Cruise Throttle Setting"), title_to_val("Cruise Ground Speed"), title_to_val("Min Throttle"));
        if(val == "VS Feedforward")
            upload_at_settings(title_to_val("AT P Gain"), title_to_val("AT I Gain"), title_to_val("AT D Gain"), ival,
                               title_to_val("Cruise Throttle Setting"), title_to_val("Cruise Ground Speed"), title_to_val("Min Throttle"));
        if(val == "Cruise Throttle Setting")
            upload_at_settings(title_to_val("AT P Gain"), title_to_val("AT I Gain"), title_to_val("AT D Gain"), title_to_val("VS Feedforward"),
                               ival, title_to_val("Cruise Ground Speed"), title_to_val("Min Throttle"));
        if(val == "Cruise Ground Speed")
        {

            upload_at_settings(title_to_val("AT P Gain"), title_to_val("AT I Gain"), title_to_val("AT D Gain"), title_to_val("VS Feedforward"),
                               title_to_val("Cruise Throttle Setting"), ival, title_to_val("Min Throttle"));
        }

        if(val == "Min Throttle")
            upload_at_settings(title_to_val("AT P Gain"), title_to_val("AT I Gain"), title_to_val("AT D Gain"), title_to_val("VS Feedforward"),
                               title_to_val("Cruise Throttle Setting"), title_to_val("Cruise Ground Speed"), ival);


        if(val == "YD P Gain")
            upload_yd_tc_settings(ival, title_to_val("YD I Gain"), title_to_val("YD D Gain"),
                                    title_to_val("Yaw Turn Gain"), title_to_val("Pitch Bank Feedforward"));
        if(val == "YD I Gain")
            upload_yd_tc_settings(title_to_val("YD P Gain"), ival, title_to_val("YD D Gain"),
                                    title_to_val("Yaw Turn Gain"), title_to_val("Pitch Bank Feedforward"));
        if(val == "YD D Gain")
            upload_yd_tc_settings(title_to_val("YD P Gain"), title_to_val("YD I Gain"), ival,
                                    title_to_val("Yaw Turn Gain"), title_to_val("Pitch Bank Feedforward"));
        if(val == "Yaw Turn Gain")
            upload_yd_tc_settings(title_to_val("YD P Gain"), title_to_val("YD I Gain"), title_to_val("YD D Gain"),
                                    ival, title_to_val("Pitch Bank Feedforward"));
        if(val == "Pitch Bank Feedforward")
            upload_yd_tc_settings(title_to_val("YD P Gain"), title_to_val("YD I Gain"), title_to_val("YD D Gain"),
                                    title_to_val("Yaw Turn Gain"), ival);

        if(val == "Launch type")
            upload_takeoff_settings(ival, title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
        if(val == "Max roll distance")
            upload_takeoff_settings(title_to_val("Launch type"), ival, title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));

        if(val == "Runway width")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), ival, title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
        if(val == "Takeoff throttle")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), ival,
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
        if(val == "Rotate speed")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    ival, title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
        if(val == "Climb out vert speed")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), ival, title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
        if(val == "Takeoff disconnect alt")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), ival, title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
        if(val == "Bungee detect accel")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), ival, title_to_val("Bungee launch delay"));
        if(val == "Bungee launch delay")
            upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                                    title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"), title_to_val("Bungee detect accel"), ival);

        if(val == "Steering Outer Gain")
            upload_steering_gains(ival, title_to_val("Steering Inner Gain"));
        if(val == "Steering Inner Gain")
            upload_steering_gains(title_to_val("Steering Outer Gain"), ival);
    }
}

void Config::on_edit_setting()
{
    Glib::RefPtr<Gtk::TreeSelection> refTreeSelection = ap_treeview->get_selection();
    Gtk::TreeModel::iterator iter = refTreeSelection->get_selected();
    if(iter)
    {
        upload_setting_from_iter(iter);
    }
}

void Config::on_refresh_settings()
{
    if(!download_at_settings())
    {
        cout << "couldn't download at settings" << endl;
        goto failed;
    }
    if(!download_hna())
    {
        cout << "couldn't download hna settings" << endl;
        goto failed;
    }
    if(!download_max_angles())
    {
        cout << "couldn't download max angle settings" << endl;
        goto failed;
    }
    if(!download_max_vs())
    {
        cout << "couldn't download vs settings" << endl;
        goto failed;
    }
    if(!download_path_pid())
    {
        cout << "couldn't download path settings" << endl;
        goto failed;
    }
    if(!download_pitch_roll_rates())
    {
        cout << "couldn't download pitch roll rates settings" << endl;
        goto failed;
    }
    if(!download_pnr())
    {
        cout << "couldn't download pnr settings" << endl;
        goto failed;
    }
    if(!download_yd_tc_settings())
    {
        cout << "couldn't download yd tc setting" << endl;
        goto failed;
    }
    if(!download_takeoff_settings())
    {
        cout << "couldn't download takeoff settings" << endl;
        goto failed;
    }
    if(!download_steering_gains())
    {
        cout << "couldn't download steering gains" << endl;
        goto failed;
    }

    return;

    failed:
        Gtk::MessageDialog mb("Failed to synchronise with vehicle! Be very careful when changing autopilot settings.");
        mb.run();
}

void Config::on_upload_all_settings()
{
    Glib::ustring msg =  "Are you sure you want to upload all settings?";
    Gtk::MessageDialog msgd(*this,msg, false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
    if(msgd.run() == Gtk::RESPONSE_NO)
        return;

    upload_pitch_roll_rates(title_to_val("Pitch Rate P"), title_to_val("Pitch Rate I"), title_to_val("Pitch Rate D"),
                            title_to_val("Roll Rate P"), title_to_val("Roll Rate I"), title_to_val("Roll Rate D"));
    upload_max_angles(title_to_val("Max Pitch Up"), title_to_val("Max Pitch Down"), title_to_val("Max Bank Angle"),
                      title_to_val("Max Pitch Rate"), title_to_val("Max Roll Rate"));
    upload_pnr(title_to_val("Pitch P"), title_to_val("Pitch I"), 0, title_to_val("Roll P"), title_to_val("Roll I"), 0);
    upload_hna(title_to_val("Heading P Gain"), title_to_val("Heading I Gain"), title_to_val("Heading D Gain"),
               title_to_val("VS P Gain"), title_to_val("VS I Gain"), title_to_val("VS D Gain"), title_to_val("Alt Gain"));
    upload_max_vs(title_to_val("Max Climb Rate"), title_to_val("Max Descent Rate"));
    upload_yd_tc_settings(title_to_val("YD P Gain"), title_to_val("YD I Gain"), title_to_val("YD D Gain"),
                          title_to_val("Yaw Turn Gain"), title_to_val("Pitch Bank Feedforward"));
    upload_path_pid(title_to_val("XTrack Gain"), title_to_val("Loiter Radius"), title_to_val("Loiter Gain"));
    upload_at_settings(title_to_val("AT P Gain"), title_to_val("AT I Gain"), title_to_val("AT D Gain"),
                       title_to_val("VS Feedforward"), title_to_val("Cruise Ground Speed"), title_to_val("Cruise Throttle Setting"), title_to_val("Min Throttle"));
    upload_takeoff_settings(title_to_val("Launch type"), title_to_val("Max roll distance"), title_to_val("Runway width"), title_to_val("Takeoff throttle"),
                        title_to_val("Rotate speed"), title_to_val("Climb out vert speed"), title_to_val("Takeoff disconnect alt"),
                        title_to_val("Bungee detect accel"), title_to_val("Bungee launch delay"));
    upload_steering_gains(title_to_val("Steering Outer Gain"), title_to_val("Steering Inner Gain"));
}


void Config::on_treeview_row_activated(const Gtk::TreeModel::Path& path,
                                       Gtk::TreeViewColumn* col)
{
    Gtk::TreeModel::iterator iter = m_refTreeModel->get_iter(path);
    if(iter)
    {
        upload_setting_from_iter(iter);
    }
}

void Config::upload_pitch_roll_rates(int pp, int pi, int pd, int rp, int ri, int rd)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_rate_pids(pp, pi, pd, rp, ri, rd);
    coms->unlock();

    download_pitch_roll_rates();
}

void Config::upload_max_angles(int pitch_up, int pitch_down, int roll, int pitch_rate, int roll_rate)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_max_angles(pitch_up, pitch_down, roll, pitch_rate, roll_rate);
    coms->unlock();

    download_max_angles();
}

void Config::upload_pnr(int pp, int pi, int pd, int rp, int ri, int rd)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_pnr_pid(pp, pi, pd, rp, ri, rd);
    coms->unlock();

    download_pnr();
}

void Config::upload_hna(int hp, int hi, int hd, int vsp, int vsi, int vsd, int altp)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_hna_pid(hp, hi, hd, vsp, vsi, vsd, altp);
    coms->unlock();

    download_hna();
}

void Config::upload_max_vs(int climb, int descend)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_max_vs(climb, descend);
    coms->unlock();

    download_max_vs();
}

void Config::upload_path_pid(int pathp, int radius, int loiter_gain)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_path_pid(pathp, 0, 0);
    coms->set_loiter_settings(radius, loiter_gain);
    coms->unlock();

    download_path_pid();
}

void Config::upload_at_settings(int p, int i, int d, int vsff, int cruise_power, int cruise_speed, int min_thr)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_at_settings(p, i, d, vsff, cruise_speed, cruise_power, min_thr);
    coms->unlock();

    download_at_settings();
}

void Config::upload_yd_tc_settings(int p, int i, int d, int yg, int pf)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_yaw_damper_settings(p, i, d, yg, pf);
    coms->unlock();

    download_yd_tc_settings();
}

void Config::upload_takeoff_settings(int launch_type, int max_roll, int runwaywidth, int takeoff_throttle, int rotate_speed, int climb_vs, int disconnect_alt, int bungee_accel, int time_delay)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_takeoff_settings((Firelink::LAUNCH_TYPE)launch_type, max_roll, runwaywidth, takeoff_throttle, rotate_speed, climb_vs, disconnect_alt, bungee_accel, time_delay);
    coms->unlock();

    download_takeoff_settings();
}

void Config::upload_steering_gains(int outer, int inner)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->set_steering_gains(outer, inner);
    coms->unlock();

    download_steering_gains();
}


bool Config::download_pitch_roll_rates()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_rate_pids();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_RATE_PIDS))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_RATE_PIDS);
    coms->unlock();
    Firelink::m_rate_pids* rp = (Firelink::m_rate_pids*)pack.data;
    set_title_val("Pitch Rate P", rp->pitch_p);
    set_title_val("Pitch Rate I", rp->pitch_i);
    set_title_val("Pitch Rate D", rp->pitch_d);

    set_title_val("Roll Rate P", rp->roll_p);
    set_title_val("Roll Rate I", rp->roll_i);
    set_title_val("Roll Rate D", rp->roll_d);
    return true;
}

bool Config::download_max_angles()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_max_angles();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_MAX_ANGLES))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MAX_ANGLES);
    coms->unlock();
    Firelink::m_max_angles* rp = (Firelink::m_max_angles*)pack.data;
    set_title_val("Max Pitch Up", (int)rp->pitch_up);
    set_title_val("Max Pitch Down", rp->pitch_down);
    set_title_val("Max Bank Angle", rp->roll);
    set_title_val("Max Pitch Rate", rp->pitch_rate);
    set_title_val("Max Roll Rate", rp->roll_rate);
    return true;
}

bool Config::download_pnr()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_pnr_pid();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_PNR_PID))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_PNR_PID);
    coms->unlock();
    Firelink::m_pnr_pid* rp = (Firelink::m_pnr_pid*)pack.data;
    set_title_val("Pitch P", rp->pitch_p);
    set_title_val("Pitch I", rp->pitch_i);
    set_title_val("Roll P", rp->roll_p);
    set_title_val("Roll I", rp->roll_i);
    return true;
}

bool Config::download_hna()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_hna_pid();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_HNA_PID))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_HNA_PID);
    coms->unlock();
    Firelink::m_hna_pid* as = (Firelink::m_hna_pid*)pack.data;
    set_title_val("Heading P Gain", as->heading_p);
    set_title_val("Heading I Gain", as->heading_i);
    set_title_val("Heading D Gain", as->heading_d);
    set_title_val("VS P Gain", as->vs_p);
    set_title_val("VS I Gain", as->vs_i);
    set_title_val("VS D Gain", as->vs_d);
    set_title_val("Alt Gain", as->alt_p);
    return true;
}

bool Config::download_max_vs()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_max_vs();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_MAX_VS))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MAX_VS);
    coms->unlock();
    Firelink::m_max_vs* as = (Firelink::m_max_vs*)pack.data;
    set_title_val("Max Descent Rate", as->descend);
    set_title_val("Max Climb Rate", as->climb);
    return true;
}

bool Config::download_path_pid()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_path_pid();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_PATH_PID))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_PATH_PID);
    coms->unlock();
    Firelink::m_path_pid* as = (Firelink::m_path_pid*)pack.data;
    set_title_val("XTrack Gain", as->p);


    coms->lock();
    coms->get_loiter_settings();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_LOITER_SETTINGS))
        return false;

    coms->lock();
    pack = coms->retrieve_from_in_queue(Firelink::VM_LOITER_SETTINGS);
    coms->unlock();
    Firelink::m_loiter_settings* ls = (Firelink::m_loiter_settings*)pack.data;
    set_title_val("Loiter Radius", ls->radius);
    set_title_val("Loiter Gain", ls->p);
    return true;
}

bool Config::download_at_settings()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_at_settings();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_AT_SETTINGS))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_AT_SETTINGS);
    coms->unlock();
    Firelink::m_at_settings* as = (Firelink::m_at_settings*)pack.data;
    set_title_val("AT P Gain", as->p);
    set_title_val("AT I Gain", as->i);
    set_title_val("AT D Gain", as->d);
    set_title_val("VS Feedforward", as->vs_throttle);
    set_title_val("Cruise Throttle Setting", as->cruise_power);
    set_title_val("Cruise Ground Speed", as->cruise_speed);
    set_title_val("Min Throttle", as->min_throttle);
    return true;
}

bool Config::download_yd_tc_settings()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_yaw_damper_settings();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_YAW_DAMPER_SETTINGS))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_YAW_DAMPER_SETTINGS);
    coms->unlock();
    Firelink::m_yaw_damper_settings* as = (Firelink::m_yaw_damper_settings*)pack.data;
    set_title_val("YD P Gain", as->p);
    set_title_val("YD I Gain", as->i);
    set_title_val("YD D Gain", as->d);
    set_title_val("Yaw Turn Gain", as->slip_p);
    set_title_val("Pitch Bank Feedforward", as->pitch_ff);
    return true;
}

bool Config::download_takeoff_settings()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_takeoff_settings();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_TAKEOFF_SETTINGS))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_TAKEOFF_SETTINGS);
    coms->unlock();
    Firelink::m_takeoff_settings* mt = (Firelink::m_takeoff_settings*)pack.data;
    set_title_val("Launch type", mt->algorithm);
    set_title_val("Max roll distance", mt->max_roll);
    set_title_val("Runway width", mt->runway_width);
    set_title_val("Takeoff throttle", mt->takeoff_throttle);
    set_title_val("Rotate speed", mt->rotate_speed);
    set_title_val("Climb out vert speed", mt->climb_vs);
    set_title_val("Takeoff disconnect alt", mt->disconnect_alt);
    set_title_val("Bungee detect accel", mt->bungee_accel);
    set_title_val("Bungee launch delay", mt->time_delay);

    return true;
}

bool Config::download_steering_gains()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();

    coms->lock();
    coms->get_steering_gains();
    coms->unlock();

    if(!wait_for_message(Firelink::VM_STEERING_GAINS))
        return false;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_STEERING_GAINS);
    coms->unlock();
    Firelink::m_steering_gains* sg = (Firelink::m_steering_gains*)pack.data;
    set_title_val("Steering Outer Gain", sg->outer_gain);
    set_title_val("Steering Inner Gain", sg->inner_gain);

    return true;
}

int Config::title_to_val(std::string st)
{
    Gtk::TreeModel::iterator iter = m_refTreeModel->children().begin();
    while(iter != m_refTreeModel->children().end())
    {
        Gtk::TreeModel::iterator child_iter = (*iter)->children().begin();

        while(child_iter != (*iter)->children().end())
        {
            Gtk::TreeModel::Row row = *child_iter;
            if(row[ap_columns.col_setting] == st)
            {
                return row[ap_columns.col_value];
            }
            child_iter++;
        }

        iter++;
    }
    std::string msgtxt = "Error finding title: " + st;
    Gtk::MessageDialog msg(msgtxt);
    msg.run();
    return -1;
}

void Config::set_title_val(std::string st, int val)
{
    Gtk::TreeModel::iterator iter = m_refTreeModel->children().begin();
    while(iter != m_refTreeModel->children().end())
    {
        Gtk::TreeModel::iterator child_iter = (*iter)->children().begin();

        while(child_iter != (*iter)->children().end())
        {
            Gtk::TreeModel::Row row = *child_iter;
            if(row[ap_columns.col_setting] == st)
            {
                (*child_iter)[ap_columns.col_value] = val;
                return;
            }
            child_iter++;
        }

        iter++;
    }
    std::string msgtxt = "Error finding title: " + st;
    Gtk::MessageDialog msg(msgtxt);
    msg.run();
}

void Config::timeout()
{
    if(control_setup->is_visible())
        control_setup->timeout();

    if(ahrs_cal->is_visible())
        ahrs_cal->timeout();

    if(mixer_uploader->is_visible())
        mixer_uploader->timeout();

    if(ahrs_cal->got_new_cal_data())
    {
        mag_bias_x->set_text(ahrs_cal->mag_bias_x_entry->get_text());
        mag_bias_y->set_text(ahrs_cal->mag_bias_y_entry->get_text());
        mag_bias_z->set_text(ahrs_cal->mag_bias_z_entry->get_text());

        mag_gain_x->set_text(ahrs_cal->mag_gain_x_entry->get_text());
        mag_gain_y->set_text(ahrs_cal->mag_gain_y_entry->get_text());
        mag_gain_z->set_text(ahrs_cal->mag_gain_z_entry->get_text());

        accel_bias_x->set_text(ahrs_cal->accel_bias_x_entry->get_text());
        accel_bias_y->set_text(ahrs_cal->accel_bias_y_entry->get_text());
        accel_bias_z->set_text(ahrs_cal->accel_bias_z_entry->get_text());
    }
}

//generic function to check for a working data link
bool Config::check_datalink()
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    if(coms->connection_strength() < 3) //if link quality is less than 3
    {
        //show an error message and return false
        Gtk::MessageDialog msg(*this, "No datalink!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The datalink is either disconnected or the link quality is too low.");
        msg.run();
        return false;
    }
    //otherwise return true
    return true;
}

bool Config::dispatch_message(uint16_t pid)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    if(!coms->wait_for_dispatch(pid))
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknowledged the request.");
        msg.run();
        return false;
    }
    return true;
}

bool Config::wait_for_message(uint16_t pid)
{
    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return false;

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(50);
        coms->lock();
        oq = coms->on_in_queue(pid);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving data!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not send the requested data.");
        msg.run();
        return false;
    }
    return true;
}



void Config::on_begin_ahrs_cal_clicked()
{
    ahrs_cal->show();
}

void Config::on_download_ahrs_cal_clicked()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    coms->clear_in_queue();
    uint16_t pid = coms->get_ahrs_cal();
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_AHRS_CAL))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_AHRS_CAL);
    coms->unlock();

    Firelink::m_ahrs_cal* acp = (Firelink::m_ahrs_cal*)pack.data;
    stringstream ss;
    ss << acp->acc_bias_x/10000.0;

    accel_bias_x->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->acc_bias_y/10000.0;
    accel_bias_y->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->acc_bias_z/10000.0;
    accel_bias_z->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_bias_x;
    mag_bias_x->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_bias_y;
    mag_bias_y->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_bias_z;
    mag_bias_z->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_gain_x/10000.0;
    mag_gain_x->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_gain_y/10000.0;
    mag_gain_y->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_gain_z/10000.0;
    mag_gain_z->set_text(ss.str());
}

void Config::on_upload_ahrs_cal_clicked()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->set_ahrs_cal(atof(mag_gain_x->get_text().c_str())*10000,
                                      atof(mag_gain_y->get_text().c_str())*10000,
                                      atof(mag_gain_z->get_text().c_str())*10000,
                                      atof(mag_bias_x->get_text().c_str()),
                                      atof(mag_bias_y->get_text().c_str()),
                                      atof(mag_bias_z->get_text().c_str()),
                                      atof(accel_bias_x->get_text().c_str())*10000,
                                      atof(accel_bias_y->get_text().c_str())*10000,
                                      atof(accel_bias_z->get_text().c_str())*10000
                                     );
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Error uploading data.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge the accelerometer bias data.");
        msg.run();
        return;
    }

    Gtk::MessageDialog msg(*this, "Upload successful.", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The AHRS cal data was uploaded successfully.");
    msg.run();
}

void Config::on_setup_servo_clicked()
{
    if(!check_datalink())
        return;

    control_setup->show();
}

void Config::on_download_servo_config()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    coms->clear_in_queue();
    uint16_t pid = coms->get_servo_config();
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }
    if(oq)
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request for servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknlowdged the request for servo configuration data.");
        msg.run();
        return;
    }

    tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(Firelink::VM_SERVO_CONFIG);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not reply with a servo configuration packet.");
        msg.run();
        return;
    }

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_SERVO_CONFIG);
    coms->unlock();

    Firelink::m_servo_config* scr = (Firelink::m_servo_config*)pack.data;
    cout << int(scr->ele_min) << endl;
    aileron_min_scale->set_value(scr->ail_min);
    aileron_max_scale->set_value(scr->ail_max);
    elevator_min_scale->set_value(scr->ele_min);
    elevator_max_scale->set_value(scr->ele_max);
    rudder_min_scale->set_value(scr->rud_min);
    rudder_max_scale->set_value(scr->rud_max);
    throttle_min_scale->set_value(scr->thr_min);
    throttle_max_scale->set_value(scr->thr_max);

    rc_signal_scale->set_value(scr->scale/100.0);

    coms->lock();
    pid = coms->get_channel_inversions();
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Error sending channel reversal settings!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not receive request for channel reversal settings.");
        msg.run();
        return;
    }
    if(!wait_for_message(Firelink::VM_CHANNEL_INVERSIONS))
    {
        Gtk::MessageDialog msg(*this, "Error sending channel reversal settings!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not send channel reversal settings.");
        msg.run();
        return;
    }

    uint8_t in, ap, out;
    in = ap = out = 0;
    coms->lock();
    if(coms->on_in_queue(Firelink::VM_CHANNEL_INVERSIONS))
        cout << "is on queue" << endl;
    Firelink::flp apack = coms->retrieve_from_in_queue(Firelink::VM_CHANNEL_INVERSIONS);
    coms->unlock();

    in = apack.data[0];
    ap = apack.data[1];
    out = apack.data[2];

    for(int i = 0; i < 3; i++)
    {
        bitset<8> bs = apack.data[i];
        cout << bs << endl;
    }
    cout << endl;

    ail_pwm_rev->set_active(read_bit(in, 1));
    ele_pwm_rev->set_active(read_bit(in, 2));
    thr_pwm_rev->set_active(read_bit(in, 3));
    rud_pwm_rev->set_active(read_bit(in, 4));

    ail_ap_rev->set_active(read_bit(ap, 1));
    ele_ap_rev->set_active(read_bit(ap, 2));
    thr_ap_rev->set_active(read_bit(ap, 3));
    rud_ap_rev->set_active(read_bit(ap, 4));

    ail_servo_rev->set_active(read_bit(out, 1));
    ele_servo_rev->set_active(read_bit(out, 2));
    thr_servo_rev->set_active(read_bit(out, 3));
    rud_servo_rev->set_active(read_bit(out, 4));

}

void Config::on_upload_servo_config()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->set_servo_config(rc_signal_scale->get_value()*100,
                                          elevator_min_scale->get_value(), elevator_max_scale->get_value(),
                                          aileron_min_scale->get_value(), aileron_max_scale->get_value(),
                                          rudder_min_scale->get_value(), rudder_max_scale->get_value(),
                                          throttle_min_scale->get_value(), throttle_max_scale->get_value());
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < 100)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }

    if(oq) //if packet is still on dispatch queue
    {
        coms->lock();
        coms->remove_pack(pid); //remove it
        coms->unlock();

        //and show an error message
        Gtk::MessageDialog msg(*this, "Error uploading servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge receiving the servo configuration data.");
        msg.run();
    }


    uint8_t pwm_reverse, ap_reverse, servo_reverse;
    pwm_reverse = ap_reverse = servo_reverse = 0;
    pwm_reverse = set_bit(pwm_reverse, 1, ail_pwm_rev->get_active());
    pwm_reverse = set_bit(pwm_reverse, 2, ele_pwm_rev->get_active());
    pwm_reverse = set_bit(pwm_reverse, 3, thr_pwm_rev->get_active());
    pwm_reverse = set_bit(pwm_reverse, 4, rud_pwm_rev->get_active());

    ap_reverse = set_bit(ap_reverse, 1, ail_ap_rev->get_active());
    ap_reverse = set_bit(ap_reverse, 2, ele_ap_rev->get_active());
    ap_reverse = set_bit(ap_reverse, 3, thr_ap_rev->get_active());
    ap_reverse = set_bit(ap_reverse, 4, rud_ap_rev->get_active());

    servo_reverse = set_bit(servo_reverse, 1, ail_servo_rev->get_active());
    servo_reverse = set_bit(servo_reverse, 2, ele_servo_rev->get_active());
    servo_reverse = set_bit(servo_reverse, 3, thr_servo_rev->get_active());
    servo_reverse = set_bit(servo_reverse, 4, rud_servo_rev->get_active());

    bitset<8> bs = pwm_reverse;
    cout << "sent: " << bs << endl;

    coms->lock();
    pid = coms->set_channel_inversions(pwm_reverse, ap_reverse, servo_reverse);
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Failed to upload servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not receive channel reversal settings.");
        msg.run();
        return;
    }

    Gtk::MessageDialog msg(*this, "Servo configuration uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The new servo configuration data has been uploaded successfully.");
    msg.run();
}

void Config::on_download_mixer_config()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->get_mixer_config();
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }
    if(oq)
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request for mixer configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknlowdged the request for mixer configuration data.");
        msg.run();
        return;
    }

    tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(Firelink::VM_MIXER_CONFIG);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving mixer configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not reply with mixer configuration data.");
        msg.run();
        return;
    }

    Firelink::m_mixer_config* mcr;
    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MIXER_CONFIG);
    coms->unlock();

    mcr = (Firelink::m_mixer_config*)pack.data;

    mixer_config_combotext->set_active(mcr->config);
    mixer_ratio_spinbutton->set_value(mcr->mix_ratio);
}

void Config::on_upload_mixer_file()
{
    mixer_uploader->show();
}

void Config::on_upload_mixer_config()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->set_mixer_config(
                       Firelink::MIXER_CONFIGURATION(mixer_config_combotext->get_active_row_number()),
                       mixer_ratio_spinbutton->get_value_as_int());
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < 100)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }

    if(oq) //if packet is still on dispatch queue
    {
        coms->lock();
        coms->remove_pack(pid); //remove it
        coms->unlock();

        //and show an error message
        Gtk::MessageDialog msg(*this, "Error uploading mixer configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge receiving the mixer configuration data.");
        msg.run();
    }
    else //otherwise it was sent OK
    {
        Gtk::MessageDialog msg(*this, "Mixer configuration uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The new mixer configuration data has been uploaded successfully.");
        msg.run();
    }
}


void Config::on_download_mode_select()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->get_mode_select_settings();
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_MODE_SELECT_SETTINGS))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MODE_SELECT_SETTINGS);
    coms->unlock();

    Firelink::m_mode_select_settings* fms = (Firelink::m_mode_select_settings*)pack.data;
    mode_select_channel->set_active(fms->chan);
    mode_select_mode->set_active(fms->mode);
}

void Config::on_upload_mode_select()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->set_mode_select_settings(
                       (Firelink::AP_MODE)mode_select_mode->get_active_row_number(),
                       mode_select_channel->get_active_row_number());
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Mode select settings uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The mode select settings have been uploaded successfully.");
        msg.run();
    }
}

void Config::on_alarm_upload()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();

    uint16_t pid = coms->set_alarm_actions(
                       string_to_alarm_action(geofence_alarm_combo->get_active_text()),
                       string_to_alarm_action(low_bat_alarm_combo->get_active_text()),
                       string_to_alarm_action(no_gps_alarm_combo->get_active_text()),
                       string_to_alarm_action(no_ctrl_alarm_combo->get_active_text()),
                       string_to_alarm_action(no_datalink_alarm_combo->get_active_text()));

    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Alarm actions uploaded", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The alarm actions were uploaded OK.");
        msg.run();
    }
}

void Config::on_alarm_download()
{
    if(!check_datalink())
        return;

    shared_ptr<Coms> coms = Coms::get_active_instance();
    if(coms == nullptr)
        return;

    coms->lock();
    uint16_t pid = coms->get_alarm_actions();
    coms->unlock();

    if(!dispatch_message(pid))
        return;
    if(!wait_for_message(Firelink::VM_ALARM_ACTIONS))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_ALARM_ACTIONS);
    coms->unlock();

    Firelink::m_alarm_actions* ala = (Firelink::m_alarm_actions*)pack.data;

    geofence_alarm_combo->set_active_text(alarm_action_to_string(ala->boundary));
    low_bat_alarm_combo->set_active_text(alarm_action_to_string(ala->low_bat));
    no_gps_alarm_combo->set_active_text(alarm_action_to_string(ala->no_gps));
    no_ctrl_alarm_combo->set_active_text(alarm_action_to_string(ala->lost_control));
    no_datalink_alarm_combo->set_active_text(alarm_action_to_string(ala->no_telem));
}

void Config::load_config()
{
    Gtk::FileChooserDialog fcd(*this, "Open configuration file", Gtk::FILE_CHOOSER_ACTION_OPEN, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::ifstream infile;
    infile.open(fcd.get_filename());//open the input file
    if(!infile.is_open())
    {
        Gtk::MessageDialog msg(*this, "Failed to open file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be opened.");
        msg.run();
        return;
    }

    stringstream ss;
    ss << infile.rdbuf();
    std::string contents = ss.str();

    Tokeniser tok(contents.c_str(), '\n');
    char nxt[255];
    while(tok.next(nxt, 255))
    {
        if(nxt[0] == '#')
            continue;

        std::vector<std::string> items;
        Tokeniser line(nxt, ':');

        char c_item[255];
        while(line.next(c_item, 255))
        {
            std::string item(c_item);
            items.push_back(item);
        }

        if(items.size() < 2)
            continue;

        set_title_val(items[0], atoi(items[1].c_str()));
    }
}

void Config::save_config()
{
    Gtk::FileChooserDialog fcd(*this, "Save configuration file", Gtk::FILE_CHOOSER_ACTION_SAVE, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::stringstream sout;
    sout << "#Firetail UAV System Configuration file \n\n";

    Gtk::TreeModel::iterator iter = m_refTreeModel->children().begin();
    while(iter)
    {
        Gtk::TreeModel::Row row = *iter;

        Gtk::TreeModel::iterator childiter = row->children().begin();
        while(childiter)
        {
            Gtk::TreeModel::Row childrow = *childiter;

            Glib::ustring val = childrow[ap_columns.col_setting];
            sout << val << ":" << title_to_val(val) << "\n";
            childiter++;
        }
        iter++;
    }

    std::string filename = fcd.get_filename();
    std::string extension(filename, filename.length()-3, 3);
    if(extension != "cnf")
        filename += ".cnf";

    std::ofstream outfile;
    outfile.open(filename);
    if(!outfile.is_open())
    {
        Gtk::MessageDialog msg(*this, "Failed to save file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be saved.");
        msg.run();
        return;
    }

    outfile << sout.str();
    outfile.close();

    Gtk::MessageDialog msg(*this, "File saved!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The configuration file has been saved successfully.");
    msg.run();
}

