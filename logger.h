#ifndef LOGGER_H_INCLUDED
/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#define LOGGER_H_INCLUDED

#include <vector>
#include <string>
#include <fstream>
#include <thread>
#include <mutex>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <sstream>

class Logger
{
public:
    Logger();
    bool begin(std::string path);
    void end();

    //logs a received message
    void log_message(std::string msg);

private:
    void logger_thread();

    std::vector<std::string> msg_vec;
    std::ofstream of;
    bool running;

    std::thread thr;
    std::mutex mute;
};
#endif // LOGGER_H_INCLUDED
